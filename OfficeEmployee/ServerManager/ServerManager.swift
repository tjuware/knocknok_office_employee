//
//  ServerManager.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 02/08/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import Foundation


import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

let requestTimeOut: TimeInterval = 60*2

enum Result<ValueType, ErrorType> {
    case success(ValueType)
    case failure(ErrorType)
}

class ServerManager {
    
    
    let networkReachabilityManager = NetworkReachabilityManager(host: "www.apple.com")
//    let baseUrl = "https://www.digitalgorkha.com/office_employee_service2/"
    let baseUrl = "http://knocknok.co/office_employee_service2/"
    
    let defaultManager: Alamofire.SessionManager = {
        
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "localhost:3443": .disableEvaluation
        ]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = requestTimeOut
        configuration.httpShouldSetCookies = true
        return Alamofire.SessionManager(
            configuration: configuration,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
    }()
    
    
    
    class func sharedInstance() -> ServerManager {
        struct Static {
            static let sharedInstance = ServerManager()
        }
        return Static.sharedInstance
    }
    
    var headers: HTTPHeaders {
        get {
            let headers: HTTPHeaders = [
                "Content-type": "text/html; charset=UTF-8",
                "Accept": "text/html"
            ]
            return headers
        }
    }
    
    func getRequest(apiName: String, extraHeader: JSON?, closure: @escaping (Result<JSON, Error>) -> Void) {
        let localHeaders = headers
        let  urlString = "\(baseUrl)\(apiName)"
        if (Alamofire.NetworkReachabilityManager.init(host: "www.google.com")?.isReachable)! {
            
            defaultManager.request(urlString, method: .get, parameters: nil, encoding: URLEncoding.default, headers: localHeaders).validate().responseSwiftyJSON{
                response in
                switch response.result {
                case .success(let data):
                    if let result = response.result.value {
                        closure(.success(result))
                    }
                    print(data)
                    
                case .failure(let error):
                    print(error)
                    closure(.failure(error))
                }
            }
        } else {
            NotificationCenter.default.post(name: Notification.Name("NetworkNotification"), object: nil)
            
        }
    }
    
    func postRequest(postString: String?, apiName: String, extraHeader: JSON?, closure: @escaping (Result<JSON, Error>) -> Void) {
        
        
        if (Alamofire.NetworkReachabilityManager.init(host: "www.google.com")?.isReachable)! {
            let urlString = baseUrl + apiName
            var request = URLRequest(url: URL(string: urlString)!)
            request.httpMethod = "POST"
//            let postString = "username=abhi.dubey20@gmail.com&password=1234567890&device_id=3e7b9141b5c5088d&gcm_id=asdasffafaffa&method=authenticate"
            if let postString1 = postString {
                request.httpBody = postString1.data(using: .utf8)
            }

            defaultManager.request(request).responseSwiftyJSON {
                response in
                print(response)
                switch response.result {
                case .success(let data):
                    closure(.success(data))
                case .failure(let error):
                    print(error)
                    closure(.failure(error))
                }
            }
        }else{
            NotificationCenter.default.post(name: Notification.Name("NetworkNotification"), object: nil)
            
        }
    }
    
}


