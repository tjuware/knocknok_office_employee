//
//  ExpectedVisitersTableViewCell.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 10/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class ExpectedVisitersTableViewCell: UITableViewCell {

//    @IBOutlet weak var checkOutLabel: UILabel!
//    @IBOutlet weak var companyArrivalLabel: UILabel!
//
//    @IBOutlet weak var checkoutContainerViewHeightConstraint: NSLayoutConstraint!
//    @IBOutlet weak var checkoutContainerView: UIView!
    
    @IBOutlet weak var expectedView: UIView!
    @IBOutlet weak var checkInLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileNoLabel: UILabel!
    
    var expectedVisiter = ExpectedVisitor() {
        didSet{
            
            bindData()
        }
    }
    
    func bindData()  {
        self.nameLabel.text = expectedVisiter.name
        self.mobileNoLabel.text = expectedVisiter.contactNo
        self.checkInLabel.text = expectedVisiter.checkInDate
        self.mobileNoLabel.textColor = UIColor .blue

//        self.checkOutLabel.text = expectedVisiter.checkOutDate
        self.expectedView.layer.borderWidth = 1
        self.expectedView.layer.borderColor = UIColor(red:74/255, green:150/255, blue:132/255, alpha: 1).cgColor
        self.expectedView.layer.cornerRadius = 8;
    }
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
