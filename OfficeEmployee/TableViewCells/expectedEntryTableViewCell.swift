//
//  expectedEntryTableViewCell.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 11/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class expectedEntryTableViewCell: UITableViewCell {

    @IBOutlet weak var expectedView: UIView!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var submitButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        nameTextField.layer.masksToBounds = true
        nameTextField.layer.cornerRadius = 5
        nameTextField.layer.borderWidth = 1
        nameTextField.layer.borderColor = UIColor.textFieldBorderColor.cgColor
        mobileNumberTextField.layer.masksToBounds = true
        mobileNumberTextField.layer.cornerRadius = 5
        mobileNumberTextField.layer.borderWidth = 1
        mobileNumberTextField.layer.borderColor = UIColor.textFieldBorderColor.cgColor
        self.expectedView.layer.borderWidth = 1        
        self.expectedView.layer.borderColor = UIColor.textFieldBorderColor.cgColor
        self.expectedView.clipsToBounds = true
        self.expectedView.layer.cornerRadius = 5
        mobileNumberTextField.keyboardType = UIKeyboardType.phonePad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
