//
//  VisitorsTableViewCell.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 07/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit
import SDWebImage

class VisitorsTableViewCell: UITableViewCell {

    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var allow: UIButton!
    @IBOutlet weak var denay: UIButton!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var alowDenayView: UIView!
    @IBOutlet weak var visiterNameLabel: UILabel!
    @IBOutlet weak var visiterMobileNo: UILabel!
    @IBOutlet weak var checkoutLabel: UILabel!
    //    @IBOutlet weak var purposeLabel: UILabel!
//    @IBOutlet weak var checkInLabel: UILabel!
//    @IBOutlet weak var checkOutLabel: UILabel!
//
//    @IBOutlet weak var checkInOutLabel: UILabel!
//    @IBOutlet weak var allowLabel: UILabel!
//    @IBOutlet weak var allowButton: UIButton!
    @IBOutlet weak var checkInOutButton: UIButton!
    @IBOutlet weak var addImpButton: UIButton!
    var visiter1 = LiveVisitors()
    let imageURL = "http://www.digitalgorkha.com/officeupload/"
    var liveVisiter = LiveVisitors() {
        didSet{
            bindData()
        }
    }
     
    override func awakeFromNib() {
        super.awakeFromNib()
        userProfileImageView.layer.borderWidth = 1
        userProfileImageView.layer.masksToBounds = false
        userProfileImageView.layer.borderColor = UIColor.white.cgColor
        userProfileImageView.layer.cornerRadius = userProfileImageView.frame.height/2
        userProfileImageView.clipsToBounds = true
        
        // Initialization code
        
        statusLabel.layer.borderWidth = 1
        statusLabel.layer.masksToBounds = false
        statusLabel.layer.borderColor = UIColor.grayBorderColor.cgColor
        statusLabel.layer.cornerRadius = 8;
        statusLabel.clipsToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

     func bindData()  {
        self.visiterNameLabel.text = liveVisiter.name
        self.visiterMobileNo.text = liveVisiter.contactNo
        self.visiterMobileNo.textColor = UIColor .blue
//        self.purposeLabel.text = liveVisiter.purpose
//        self.checkInLabel.text = liveVisiter.checkIn
//        self.checkOutLabel.text = liveVisiter.checkOut
        let imageName = liveVisiter.profileImage
        if imageName == "" {
            self.userProfileImageView.image =  UIImage(named: "user")
        } else {
            let profileImage = imageURL + imageName
            self.userProfileImageView.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "user"))
        }
        
    }
}
