//
//  EventDetailTableViewCell.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 21/12/18.
//  Copyright © 2018 Ashok Londhe. All rights reserved.
//

import UIKit

class EventDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var eventDetailView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileNumberCell: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var detailImage: UIImageView!
    
    var guestEvent = Guest() {
        didSet{
            bindData()
        }
    }
    
    func bindData()  {
        self.nameLabel.text = guestEvent.guest_name
        self.typeLabel.text = guestEvent.guest_id
        self.dateLabel.text = guestEvent.whenCheckedIn
        self.mobileNumberCell.text = guestEvent.contact_no
        self.eventDetailView.layer.borderWidth = 1
        self.eventDetailView.layer.borderColor = UIColor(red:74/255, green:150/255, blue:132/255, alpha: 1).cgColor
        self.eventDetailView.layer.cornerRadius = 8;
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
