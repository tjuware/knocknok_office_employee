//
//  WatchListTableViewCell.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 01/03/19.
//  Copyright © 2019 Ashok Londhe. All rights reserved.
//

import UIKit

class WatchListTableViewCell: UITableViewCell {

    @IBOutlet weak var watchListView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var mobileNumerLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    var expectedVisiter = ExpectedVisitor() {
        didSet{
            
            bindData()
        }
    }
    
    func bindData()  {
        self.nameLabel.text = expectedVisiter.name
        self.mobileNumerLabel.text = "Mobile : " + expectedVisiter.contactNo
        self.watchListView.layer.borderWidth = 1
        self.watchListView.layer.borderColor = UIColor(red:74/255, green:150/255, blue:132/255, alpha: 1).cgColor
        self.watchListView.layer.cornerRadius = 8;
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
    