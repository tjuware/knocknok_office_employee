//
//  DeliveryTableViewCell.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 09/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class DeliveryTableViewCell: UITableViewCell {

    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var mobileNoLabel: UILabel!
    @IBOutlet weak var deliveryCompanyName: UILabel!
    @IBOutlet weak var checkInLabel: UILabel!
    let imageURL = "http://www.digitalgorkha.com/officeupload/"

    var deliveryList = LiveVisitors() {
        didSet{
            bindData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageProfile.layer.borderWidth = 1
        imageProfile.layer.masksToBounds = false
        imageProfile.layer.borderColor = UIColor.white.cgColor
        imageProfile.layer.cornerRadius = imageProfile.frame.height/2
        imageProfile.clipsToBounds = true
        // Initialization code
        self.deliveryView.layer.borderWidth = 1
        self.deliveryView.layer.borderColor = UIColor(red:74/255, green:150/255, blue:132/255, alpha: 1).cgColor
        self.deliveryView.layer.cornerRadius = 8;
    }
    
    func bindData(){
        self.nameLabel.text = deliveryList.name
        self.mobileNoLabel.text = deliveryList.contactNo
        self.deliveryCompanyName.text = deliveryList.deliveryCompanyName
        self.checkInLabel.text = deliveryList.checkIn
        let imageName = deliveryList.profileImage
        if imageName == "" {
            self.imageProfile.image =  UIImage(named: "user")
        } else {
            let profileImage = imageURL + imageName
            self.imageProfile.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "user"))
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
