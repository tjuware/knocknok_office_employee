//
//  AddExpectedCell.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 11/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class AddExpectedCell: UITableViewCell {
   
    @IBOutlet weak var NameLabel: UILabel!
        @IBOutlet weak var mobileNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureContactInfo(contactInfo: ContactList) {
        NameLabel.text = "\(String(describing: contactInfo.givenName!)) \(String(describing: contactInfo.familyName!))"
        mobileNumberLabel.text = contactInfo.phoneNumber
        if contactInfo.isChecked == false{
            accessoryView = UIImageView(image: UIImage(named: "Uncheck"))
            accessoryType = .none
        }else{
            accessoryView = UIImageView(image: UIImage(named: "Check"))
            accessoryType = .checkmark
        }
    }
    
    
    func configureInvitationInfo(contactInfo: ContactList) {
        NameLabel.text = "\(String(describing: contactInfo.givenName!)) \(String(describing: contactInfo.familyName!))"
        mobileNumberLabel.text = contactInfo.phoneNumber
        accessoryView = UIImageView(image: UIImage(named: "Check"))
        accessoryType = .checkmark
    }

}
