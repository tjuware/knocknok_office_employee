//
//  EventTableViewCell.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 10/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var venuLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!

    var visitorEvent = Event() {
        didSet{
            bindData()
        }
    }
    
    func bindData()  {
        self.nameLabel.text = visitorEvent.eventName
        self.timeLabel.text = visitorEvent.eventTime
        self.dataLabel.text = visitorEvent.eventDate
        self.venuLabel.text = visitorEvent.eventVenue
        self.eventView.layer.borderWidth = 1
        self.eventView.layer.borderColor = UIColor(red:74/255, green:150/255, blue:132/255, alpha: 1).cgColor
        self.eventView.layer.cornerRadius = 8
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
