//
//  ImpContactsTableViewCell.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 08/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit
import SDWebImage

class ImpContactsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var impView: UIView!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var contactNoLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    let imageURL = "http://www.digitalgorkha.com/officeupload/"
    var impContact = LiveVisitors() {
        didSet{
            bindData()
        }
    }
    
    func bindData(){
        self.nameLabel.text = impContact.name
        self.contactNoLabel.text = impContact.contactNo
        let image = impContact.profileImage
        let imageProfile = imageURL + image
        self.profileImage.sd_setImage(with: URL(string: imageProfile), placeholderImage: UIImage(named: "user"))

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        self.impView.layer.borderWidth = 1
        self.impView.layer.borderColor = UIColor(red:74/255, green:150/255, blue:132/255, alpha: 1).cgColor
        self.impView.layer.cornerRadius = 8;
        // Initialization code
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
