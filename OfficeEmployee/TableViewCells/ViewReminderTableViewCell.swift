//
//  ViewReminderTableViewCell.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 09/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class ViewReminderTableViewCell: UITableViewCell {

    @IBOutlet weak var reminderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var reminderLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    var reminderV = ViewReminder() {
        didSet{
            bindData()
        }
    }
    
    func bindData()  {
        
        self.titleLabel.text = reminderV.title
        self.reminderLabel.text = reminderV.reminder
        self.dateLabel.text = reminderV.date
        self.timeLabel.text = reminderV.time
        self.reminderView.layer.borderWidth = 1
        self.reminderView.layer.borderColor = UIColor(red:74/255, green:150/255, blue:132/255, alpha: 1).cgColor
        self.reminderView.layer.cornerRadius = 8;
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
