//
//  DataManager.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 02/08/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import Foundation

import SwiftyJSON


class DataManager {
    
    var count = 0
    var username = ""
    var password = ""
    var deviceToken = ""//"3e7b9141b5c5088d"
    var gcm_id = "asdasffafaffa"
    var method = ""
    var userDetails = User()
    
    // MARK: Created Shared instance of DataManager class
    class func sharedInstance() -> DataManager {
        struct Static {
            static let sharedInstance = DataManager()
        }
        return Static.sharedInstance
    }
    
    
    // MARK: Login API call
    func loginRequest(username: String ,password: String,device: String,gcmId: String, apiName: String, closure: @escaping (Result<User,Error>) -> Void) {
        
        let parameters = ["username": username, "password": password, "device_id": "3e7b9141b5c5088d","method": "authenticate"];
        print(parameters)
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&device=\(device)&gcm_id=\(gcmId)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString, apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                let user = JSONMapper.userMapper(responseJson: response)
                closure(.success(user))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    // 9FC8E90E-5045-4E02-ADD4-0229D3FA2F14
    //9FC8E90E-5045-4E02-ADD4-0229D3FA2F14
    func LiveVistorsRequest(empId:String, officeId:String,startDate:String,endDate:String,apiName: String, closure: @escaping (Result<[LiveVisitors],Error>) -> Void) {
        
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&method=\(method)&employee_id=\(empId)&office_id=\(officeId)&startdate=\(startDate)&enddate=\(endDate)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                print(response)
                let liveVisitersList = JSONMapper.LiveVisitorsMap(responseJson: response[0]["visitor_details"] as JSON)
                print(liveVisitersList)
                closure(.success(liveVisitersList))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: expected/ live vistitors API call
//    func expectVistorsRequest(empId:String, officeId:String, apiName: String, closure: @escaping (Result<[ExpectedVisitor],Error>) -> Void) {
//
//         let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&method=\(method)&employee_id=\(empId)&office_id=\(officeId)"
//        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
//            switch result {
//            case .success(let response):
//                let visitersList = JSONMapper.ExpectedVisitors(responseJson: response[0]["expected_visitor_details"] as JSON)
//                closure(.success(visitersList))
//            case .failure(let error):
//                closure(.failure(error))
//            }
//        }
//    }
    
    func expectVistorsRequest(empId:String, officeId:String, apiName: String, closure: @escaping (Result<[ExpectedVisitor],Error>) -> Void) {
        
         let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&method=\(method)&employee_id=\(empId)&office_id=\(officeId)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                print(result)
                let visitersList = JSONMapper.ExpectedVisitors(responseJson: response[0]["expected_visitor_details"] as JSON)
                closure(.success(visitersList))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }

    // MARK: WatchList Request API call
    func WatchListRequest(empId:String, officeId:String,apiName: String, closure: @escaping (Result<[ExpectedVisitor],Error>) -> Void) {
        
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                let spamVisitersList = JSONMapper.WatchList(responseJson: response[0]["spam_list"] as JSON)
                closure(.success(spamVisitersList))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: IMPContact Request API call
    func ImpContactRequest(empId:String, officeId:String,apiName: String, closure: @escaping (Result<[LiveVisitors],Error>) -> Void) {
        
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                let impContactList = JSONMapper.impContactList(responseJson: response[0]["imp_contact"] as JSON)
                closure(.success(impContactList))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: Events Request API call
    func EventsRequest(empId:String, officeId:String,apiName: String, closure: @escaping (Result<[Event],Error>) -> Void) {
        
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                let eventList = JSONMapper.EventList(responseJson: response[0]["eventsData"] as JSON)
                closure(.success(eventList))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: delivery Request API call
    func DeliveryRequest(empId:String, officeId:String,startDate:String,endDate:String,apiName: String, closure: @escaping (Result<[LiveVisitors],Error>) -> Void) {
        
        // let parameters = ["username": username, "password": password,"device_id": deviceToken,"method": "delivery", "start_date": startDate, "end_date":endDate];
        //  print(parameters)
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&method=\(method)&employee_id=\(empId)&office_id=\(officeId)&start_date=\(startDate)&end_date=\(endDate)"
        ServerManager.sharedInstance().postRequest(postString: jsonString, apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                let deliveryList = JSONMapper.deliveryList(responseJson: response[0]["delivery_details"] as JSON)
                closure(.success(deliveryList))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: ViewReminder Request API call
    func ViewReminderRequest(empId:String, officeId:String,apiName: String, closure: @escaping (Result<[ViewReminder],Error>) -> Void) {
        
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                let eventList = JSONMapper.ReminderList(responseJson: response[0]["data"] as JSON)
                closure(.success(eventList))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: ChnagePassword Request API call
    func ChnagePasswordRequest(empId:String,changPass:String, newPassword:String, confrimPass:String,apiName: String, closure: @escaping (Result<JSON,Error>) -> Void) {
        
        let jsonString = "username=\(username)&password=\(changPass)&device_id=\(deviceToken)&emp_id=\(empId)&method=\(method)&npassword=\(newPassword)&cpassword=\(confrimPass)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
//                let eventList = JSONMapper.ReminderList(responseJson: response[0]["data"] as JSON)
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: Logout Request API call
    func LogoutRequest(empId:String,apiName: String, closure: @escaping (Result<JSON,Error>) -> Void) {
        
        let jsonString = "device_id=\(deviceToken)&employee_id=\(empId)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                //                let eventList = JSONMapper.ReminderList(responseJson: response[0]["data"] as JSON)
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: GuestList Request API call
    func GuestListRequest(empId:String, officeId:String, eventId:String, apiName: String, closure: @escaping (Result<[Guest],Error>) -> Void) {
        
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&event_id=\(eventId)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                let GuestList = JSONMapper.GuestList(responseJson: response[0]["eventVisitorsData"] as JSON)
                closure(.success(GuestList))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: AddReminder Request API call
    func AddReminderRequest(empId:String, officeId:String, visitorId:String, title:String, reminder:String, date:String, timestamp:String, time:String, apiName: String, closure: @escaping (Result<JSON,Error>) -> Void) {
        
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&visitor_id=\(visitorId)&title=\(title)&reminder=\(reminder)&timestamp=\(timestamp)&date=\(date)&time=\(time)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
//                let GuestList = JSONMapper.GuestList(responseJson: response[0]["eventVisitorsData"] as JSON)
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: AddImp Request API call
    func AddImpRequest(empId:String, officeId:String, visitorId:String, starFlag:String,checkID:String, apiName: String, closure: @escaping (Result<JSON,Error>) -> Void) {
        
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&visitor_id=\(visitorId)&star_Flag=\(starFlag)&chk_id=\(checkID)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                //                let GuestList = JSONMapper.GuestList(responseJson: response[0]["eventVisitorsData"] as JSON)
                
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: CheckOut Request API call
    func CheckOutRequest(empId:String, officeId:String, visitorId:String, currentDate:String, apiName: String, closure: @escaping (Result<JSON,Error>) -> Void) {
        
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&visitor_id=\(visitorId)&date=\(currentDate)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                //                let GuestList = JSONMapper.GuestList(responseJson: response[0]["eventVisitorsData"] as JSON)
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: AddExpected Request API call
    func AddExpectedRequest(empId:String, officeId:String, expectDate:String, empName:String, visitor_count:String, data:[[String : Any]], apiName: String, closure: @escaping (Result<JSON,Error>) -> Void) {
         var myDictionary = [[String: Any]]()
        myDictionary = [["username": username, "password":password,"device_id": deviceToken, "emp_id":empId,"office_id": officeId, "expectDate":expectDate,"emp_name": empName, "visitor_count":visitor_count,"method":"add_expected", "data":data]]
//        var myDictionary = [String: Any]()
//        myDictionary = ["username": username, "password":password,"device_id": deviceToken, "emp_id":empId,"office_id": officeId, "expectDate":expectDate,"emp_name": empName, "visitor_count":visitor_count,"method":"add_expected", "data":data]
        
        print(myDictionary)
        
       if let theJSONData = try? JSONSerialization.data(withJSONObject: myDictionary,
            options: []) {
        let theJSONText = String(data: theJSONData,
                                     encoding: .utf8)
        let jsonString2 = "expectvisi=\(theJSONText!)"
        let string2 = jsonString2.replacingOccurrences(of: "\\", with: "")
        print(string2)
        ServerManager.sharedInstance().postRequest(postString: string2 , apiName: apiName, extraHeader: nil) { (result) in
            print(result)
            switch result {
            case .success(let response):
                //                let GuestList = JSONMapper.GuestList(responseJson: response[0]["eventVisitorsData"] as JSON)
                closure(.success(response))
            case .failure(let error):
                print(error)
                closure(.failure(error))
            }
        }
        }
    }
    
    // MARK: OTP Request API call
    func OTPRequest(mobile:String, apiName:String, closure: @escaping (Result<JSON,Error>) -> Void) {
        
        let jsonString = "device_id=\(deviceToken)&mobile=\(mobile)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                //                let GuestList = JSONMapper.GuestList(responseJson: response[0]["eventVisitorsData"] as JSON)
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: verifyOTPRequest Request API call
    func verifyOTPRequest(mobile:String, verifyOtp:String, apiName:String, closure: @escaping (Result<User,Error>) -> Void) {
        
        let jsonString = "device_id=\(deviceToken)&mobile=\(mobile)&otp=\(verifyOtp)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                let user = JSONMapper.userMapper(responseJson: response)
                closure(.success(user))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: AllowDenayRequest API call
    func AllowDenayRequest(visitorName:String, visitiorNo:String, empId:String, officeId:String, statusV:String, employeeName:String, receptionId:String, visitorId:String, reason:String, userImage:String, apiName:String, closure: @escaping (Result<JSON,Error>) -> Void) {
        
        let jsonString = "username=\(username)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&visitor_id=\(visitorId)&visitior_name=\(visitorName)&visitior_no=\(visitiorNo)&status=\(statusV)&employee_name=\(employeeName)&reception_id=\(receptionId)&reason=\(reason)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: FCM Token API call
    func FcmTokenRequest(empId:String, gcmId:String, password:String, apiName:String, closure: @escaping (Result<JSON,Error>) -> Void) {
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&gcm_id=\(gcmId)&employee_id=\(empId)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: LeaveRequestAPI call
    func LeaveRequest(empId:String, officeId:String, message:String, leaveStartDate:String, leaveEndDate:String, apiName:String, closure: @escaping (Result<JSON,Error>) -> Void) {
        let jsonString = "device_id=\(deviceToken)&office_id=\(officeId)&employee_id=\(empId)&msg=\(message)&leave_startdate=\(leaveStartDate)&leave_enddate=\(leaveEndDate)&method=\(method)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: LeaveRequestAPI call
    func addBlackRequest(empId:String, officeId:String, visitorName:String, visitorNo:String, apiName:String, closure: @escaping (Result<JSON,Error>) -> Void) {
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&method=\(method)&visitior_name=\(visitorName)&visitior_no=\(visitorNo)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: deleteBlackRequestAPI call
    func deleteBlackRequest(empId:String, officeId:String, visitoriD:String, visitorNo:String, apiName:String, closure: @escaping (Result<JSON,Error>) -> Void) {
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&method=\(method)&visitor_id=\(visitoriD)&visitior_no=\(visitorNo)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: UpdateFCMAPI call
    func UpdateFCMRequest(empId:String, officeId:String, visitoriD:String, visitorNo:String, apiName:String, closure: @escaping (Result<JSON,Error>) -> Void) {
        let jsonString = "username=\(username)&password=\(password)&device_id=\(deviceToken)&employee_id=\(empId)&office_id=\(officeId)&method=\(method)&visitor_id=\(visitoriD)&visitior_no=\(visitorNo)"
        ServerManager.sharedInstance().postRequest(postString: jsonString , apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                closure(.success(response))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }

    
}


