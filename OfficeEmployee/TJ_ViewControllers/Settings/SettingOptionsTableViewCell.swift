//
//  SettingOptionsTableViewCell.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 29/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

class SettingOptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_SettingOptionTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(settingsTitle:String){
        self.lbl_SettingOptionTitle.text = settingsTitle
    }
    
}
