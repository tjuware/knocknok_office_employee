//
//  SettingsVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 29/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit
import MessageUI

class SettingsVC: MasterVC ,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var settingsTitleArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTopTitle(title: "Settings")
        footerView.delegate = self
        
        settingsTitleArray = ["FAQ","LOGOUT","FEEDBACK","LEAVE A REVIEW","LET'S DO BUSINESS"]
        setupTableView()
    }
    
    func setupTableView(){
        tableView.register(UINib(nibName: "SettingOptionsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingOptionsTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 80
    }

}

extension SettingsVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingOptionsTableViewCell", for: indexPath) as! SettingOptionsTableViewCell
        cell.configureCell(settingsTitle:settingsTitleArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            print("FAQ")
            break
        case 1:
            LogoutVistors()
            break
        case 2:
            let emailTitle = "Feedback/ Suggestion for DGEmployee - Ios App"
            let messageBody = ""
            let toRecipents = ["jeetshethia@gmail.com"]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents)
            self.present(mc, animated: true, completion: nil)
            break
        case 3:
            print("LEAVE A REVIEW")
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id1024941703"),
                UIApplication.shared.canOpenURL(url)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            break
        case 4:
            print("LET'S DO BUSINESS")
            let emailTitle = "Let's do a Business"
            let messageBody = ""
            let toRecipents = ["jeetshethia@gmail.com"]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents)
            self.present(mc, animated: true, completion: nil)
            break
        default:
            print("No Case")
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        //    switch result {
        //    case MFMailComposeResultCancelled:
        //      print("Mail cancelled")
        //    case MFMailComposeResultSaved:
        //      print("Mail saved")
        //    case MFMailComposeResultSent:
        //      print("Mail sent")
        //    case MFMailComposeResultFailed:
        //      print("Mail sent failure: \(error?.localizedDescription)")
        //    default:
        //      break
        //    }
        self.dismiss(animated: true, completion: nil)
    }
    
        func LogoutVistors() {
            showLoadingIndictor()
            DataManager.sharedInstance().method = "logout"
            DataManager.sharedInstance().LogoutRequest(empId: self.getEmployeeID(), apiName: logout,closure: { (result) in
                print(result)
                self.hideLoadingIndictor()
                switch result {
                case .success(let data):
                    print(data)
    //                UIApplication.shared.unregisterForRemoteNotifications()
                    UserDefaults.standard.removeObject(forKey: "userName_key")
                    UserDefaults.standard.removeObject(forKey: "password_key")
                    
                    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.app.navController = UINavigationController(rootViewController: loginVC)
                    self.app.navController.setNavigationBarHidden(true, animated: true)
                    self.app.navController.isToolbarHidden = true
                    self.app.window?.rootViewController = self.app.navController
                    break
                case .failure(let error):
                    print(error)
                    self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                }
            })
        }
    
}

extension SettingsVC : FooterViewDelegate{
    func didSelectHome() {
        jumpToVC(viewController: HomeVC(), isFromSB: true)
    }
}
