//
//  MasterVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 28/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit



class MasterVC : BaseViewController{
    
    @IBOutlet weak var topView: TopView!
    @IBOutlet weak var footerView: FooterView!
    
    var headertitleTxt :String!
    let app = UIApplication.shared.delegate as! AppDelegate
    
    public func setTopTitle(title : String){
        self.topView.setLabelText(lbl_text: title)
    }
    
    func formatDateAsDay(date:Date)->String{
      
      let startFormatter = DateFormatter()
      startFormatter.dateFormat = "dd-MM-yyyy"
      let startDateString = startFormatter.string(from: date)
      return startDateString
      
    }
    
    public func formatDate(date:Date)->String{
        
        let startFormatter = DateFormatter()
        startFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let startDateString = startFormatter.string(from: date)
        return startDateString
        
    }
    
    //isFromSB - From StoryBoard
    func jumpToVC<T:UIViewController>(viewController:T,isFromSB:Bool){
        if isFromSB {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let settingsVC = mainStoryboard.instantiateViewController(withIdentifier: "\(T.self)") as! T
            self.app.navController.pushViewController(settingsVC, animated: true)
        }else{
            self.app.navController.pushViewController(T(nibName: "\(T.self)", bundle: nil), animated: true)
        }
        
    }
    
    func setRound<T:UIView>(view:T,radius:CGFloat,clr:UIColor? = nil,bw:CGFloat? = nil){
        DispatchQueue.main.async {
            view.layer.cornerRadius = radius
            view.clipsToBounds = true
            if let clr = clr{
                view.layer.borderColor = clr.cgColor
            }
            if let bw = bw{
                view.layer.borderWidth = bw
            }
        }
    }
    
//    public func formatDateDay(date:Date)->String{
//
//        let startFormatter = DateFormatter()
//        startFormatter.dateFormat = "yyyy-MM-dd"
//        let startDateString = startFormatter.string(from: date)
//        return startDateString
//
//    }
    
}

extension UIViewController : TopViewDelegate{
    
    func didClickedInfo() {
            let infoPopUpVC = InfoPopUpVC(nibName: "InfoPopUpVC", bundle: nil)
            infoPopUpVC.modalPresentationStyle = .overCurrentContext
            infoPopUpVC.modalTransitionStyle = .crossDissolve
            // Present View "Modally"
            self.present(infoPopUpVC, animated: true, completion: nil)
    }
    
    func didClickedBack() {
        self.navigationController?.popViewController(animated: true)
    }
}





