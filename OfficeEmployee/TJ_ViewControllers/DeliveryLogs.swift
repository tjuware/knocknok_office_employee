//
//  DeliveryLogs.swift
//  OfficeEmployee
//
//  Created by Kush Thakkar on 18/09/20.
//  Copyright © 2020 Knocknok. All rights reserved.
//

import UIKit

class DeliveryLogs: MasterVC {

    @IBOutlet weak var footer_View: FooterView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnFrom: DesignableButton!
    @IBOutlet weak var btnTo: DesignableButton!
    @IBOutlet weak var lblNoDelivery: UILabel!
    
    var DeliveryArray = [LiveVisitors]()
    var searchDeliveryArray = [LiveVisitors]()
    var label = UILabel()
    
    var selectedStartDate = ""
    var selectedEndDate = ""
    var startDateString = ""
    var endDateString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTopTitle(title: "Delivery")
        footer_View.delegate = self
        self.topView.btn_Info.isHidden = true
        searchBar.delegate = self
        
        label.center = self.tableView.center
        label.textAlignment = .center
        label.text = "No Data!"
        tableView.addSubview(label)
        label.isHidden = false
        
        setUpInitialDates()
        registerTableView()
        DeliveryList(startDateDelivery: startDateString, endDateDelivery: endDateString)
        
    }

    func registerTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 90
        tableView.register(UINib(nibName: "DeliveryLogCell", bundle: nil), forCellReuseIdentifier: "DeliveryLogCell")
    }
    
    private func setUpInitialDates(){
        selectedEndDate = formatDate(date: Date())
        guard let selStartDate = Calendar.current.date(byAdding: .day, value: -7, to: Date()) else { return }
        selectedStartDate = formatDate(date: selStartDate)
        //For buttons to display in day format
        startDateString = formatDateAsDay(date: selStartDate)
        endDateString = formatDateAsDay(date: Date())
        self.btnFrom.setTitle("From : \(startDateString)", for: .normal)
        self.btnTo.setTitle("To : \(endDateString)", for: .normal)
    }
    
    @IBAction func btnActionDateSelected(_ sender: DesignableButton) {
        
        let pastVisitorsCalendarPopUpVC = PastVisitorsCalendarPopUpVC(nibName: "PastVisitorsCalendarPopUpVC", bundle: nil)
        pastVisitorsCalendarPopUpVC.delegate = self
        pastVisitorsCalendarPopUpVC.selectedButtonIndex = sender.tag
        pastVisitorsCalendarPopUpVC.selectedStartDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())!
        pastVisitorsCalendarPopUpVC.selectedEndDate = Date()
        pastVisitorsCalendarPopUpVC.modalPresentationStyle = .overCurrentContext
        pastVisitorsCalendarPopUpVC.modalTransitionStyle = .crossDissolve
        // Present View "Modally"
        self.present(pastVisitorsCalendarPopUpVC, animated: true, completion: nil)
        
    }
    
    func DeliveryList(startDateDelivery: String, endDateDelivery:String) {
        print(startDateDelivery)
        print(endDateDelivery)
        showLoadingIndictor()
        DataManager.sharedInstance().method = "delivery"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().DeliveryRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(),startDate:startDateDelivery ,endDate:endDateDelivery, apiName: delivery,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.DeliveryArray = data;
                self.searchDeliveryArray = self.DeliveryArray
                self.label.isHidden = (self.DeliveryArray.count == 0) ? false : true
                DispatchQueue.main.async {
                    self.lblNoDelivery.isHidden = self.searchDeliveryArray.count == 0 ? false : true
                    self.tableView.reloadData()
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
}

extension DeliveryLogs : PastVisitorsCalendarPopUpVCDelegate{
    
    func didSelectDate(date: Date, selectedButtonIndex: Int) {
        print(date)
        if(selectedButtonIndex == 1){
            startDateString = formatDateAsDay(date: date)
            btnFrom.setTitle("From : \(startDateString)", for: .normal)
        }else if(selectedButtonIndex == 2){
            endDateString = formatDateAsDay(date: date)
            btnTo.setTitle("To : \(endDateString)", for: .normal)
        }
        DeliveryList(startDateDelivery: startDateString, endDateDelivery: endDateString)
    }
    
}

extension DeliveryLogs : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchDeliveryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryLogCell", for: indexPath) as! DeliveryLogCell
        cell.visitor = searchDeliveryArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

extension DeliveryLogs : UISearchBarDelegate{
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    
  {
    searchBar.text = ""
    
    searchDeliveryArray = DeliveryArray
    
    searchBar.endEditing(true)
    DispatchQueue.main.async {
      self.tableView.reloadData()
    }
    
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    
  {
    searchDeliveryArray = searchText.isEmpty ? DeliveryArray : DeliveryArray.filter{
      (liveVis: LiveVisitors) -> Bool in
        // If dataItem matches the searchText, return true to include
        return liveVis.name.range(of: searchText, options: .caseInsensitive, range: nil,locale: nil) != nil
    }
    DispatchQueue.main.async {
      self.tableView.reloadData()
    }
  }
}

extension DeliveryLogs : FooterViewDelegate{
    
    //MARK : FooterViewDelegates
    
    func didSelectHome() {
        jumpToVC(viewController: HomeVC(), isFromSB: true)
    }
    
    func didSelectSettings() {
        jumpToVC(viewController: SettingsVC(), isFromSB: true)
    }
    
}
