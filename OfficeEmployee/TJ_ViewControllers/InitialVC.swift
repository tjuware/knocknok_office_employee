//
//  InitialVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 27/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

class InitialVC: BaseViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var containerVIew: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.containerVIew.isHidden = true
        
        UIView.animate(withDuration: 4,
                                   delay: 0,
                                   usingSpringWithDamping: 0.1,
                                   initialSpringVelocity: 0.4,
                                   options: .transitionCrossDissolve,
                                   animations: {
                                    self.imageView.frame = self.containerVIew.frame
                                    
        }, completion: {
            //Code to run after animating
            (value: Bool) in
            
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            UIApplication.shared.keyWindow?.rootViewController = loginVC
        })

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
