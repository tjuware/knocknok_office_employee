//
//  ChangePasswordPopUpVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 27/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit
//ChangePasswordPopUpVCDelegate
protocol ChangePasswordPopUpVCDelegate {
    func didSetNewPassword(newPwd:String)
}

class ChangePasswordPopUpVC: MasterVC {
    
    @IBOutlet weak var txt_NewPassword: UITextField!
    @IBOutlet weak var txt_ConfirmPassword: UITextField!
    @IBOutlet weak var lineOneView: UIView!
    @IBOutlet weak var lineTwoView: UIView!
    @IBOutlet weak var btn_CheckBox: UIButton!
    
    var delegate:ChangePasswordPopUpVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnActionSubmit(_ sender: Any) {
        if (txt_NewPassword.text != ""){
            self.presentWindow!.makeToast(message: "Password Field must be filled")
        }else if(txt_NewPassword.text != txt_ConfirmPassword.text){
            self.presentWindow!.makeToast(message: "Password Mismatched")
        }else if(txt_NewPassword.text == txt_ConfirmPassword.text){
            self.delegate?.didSetNewPassword(newPwd:txt_NewPassword.text!)
        }
    }


      @IBAction func btnActionCheckBox(_ sender: UIButton) {
      if sender.tag == 0 {
        sender.tag = 1
        txt_NewPassword?.isSecureTextEntry = false
        txt_ConfirmPassword?.isSecureTextEntry = false
        sender.setImage(UIImage(named: "checkYes"), for: .normal)
      }
      else {
        sender.tag = 0
        txt_NewPassword?.isSecureTextEntry = true
        txt_ConfirmPassword?.isSecureTextEntry = true
        sender.setImage(UIImage(named: "checkNo"), for: .normal)
      }
    }
    
    @IBAction func btnActionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}
