//
//  ChangePasswordVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 27/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

class ChangePasswordVC: MasterVC,UITextFieldDelegate {
 
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var mobileNoTextField: UITextField!
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var sendOTPButton: UIButton!
    @IBOutlet weak var verifyOTPButton: UIButton!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    var userDetails = DataManager.sharedInstance().userDetails
    var countdownTimer: Timer!
    var totalTime = 30
    var vcIdentifier = "LoginOTP"
    var newPassword = ""
    var confPassword = ""
    var sendOTP = false
    var otp = ""
    var updateFcmController = LoginViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        if vcIdentifier == "LoginOTP" {
            addBackButtonToVC()
        } else {
                    //TJ Commented
//              slideMenuShow(menuButton, viewcontroller: self)
        }
        setupView()
        self.timerLabel.isHidden = true
        self.otpTextField.isUserInteractionEnabled = false
        self.verifyOTPButton.isUserInteractionEnabled = false
        self.title = "Change Password"
        self.mobileNoTextField.delegate = self
        self.otpTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNoTextField{
            guard let textFieldText = mobileNoTextField!.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            if count > 10{
                DispatchQueue.main.async {
                    self.presentWindow!.makeToast(message: "Mobile Number should be 10 digit")
                }
            }
            return count <= 10
        }else{
            guard let textFieldText = otpTextField!.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            if count > 4{
                DispatchQueue.main.async {
                    self.presentWindow!.makeToast(message: "Mobile Number should be 10 digit")
                }
            }
            return count <= 4
        }
    }
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        let timerValue = "\(timeFormatted(totalTime))"
        let stringOtp = "You will get OTP by SMS:- "
        let timerString =  stringOtp+timerValue
        timerLabel.text = timerString
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
            totalTime = 30
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
//        self.resendOTPButton.isHidden = false
        self.sendOTPButton.isUserInteractionEnabled = true
        self.sendOTPButton.titleLabel?.text = "Send OTP"

        timerLabel.text = "Didn't get the OTP?"
        
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    func networkStatusChanged(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo
        print(userInfo!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView() {
        mobileNoTextField.clipsToBounds = true
        mobileNoTextField.setTextFieldRoundBorder()
        otpTextField.clipsToBounds = true
        otpTextField.setTextFieldRoundBorder()
        verifyOTPButton.setButtonAllRoundBorder()
        sendOTPButton.setButtonAllRoundBorder()
    }
    
    func addBackButtonToVC() {
        let backButton = UIButton(frame: CGRect(x: 20,y: 30 ,width: 30,height: 30))
        backButton.setBackgroundImage(UIImage(named: "greenBackArrow"), for: UIControlState())
        backButton.isExclusiveTouch = true
        backButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
        self.view.addSubview(backButton)
    }
    
    func otpRequestAPI() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "otp"
        DataManager.sharedInstance().OTPRequest(mobile: self.mobileNoTextField.text!, apiName:otpRequest , closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"]
                let messageAert = theDictionary["message"].string
                
                if errorCode == "0"{
                    let otpGet = theDictionary["otp"].int
                    self.otp = String(otpGet!)

                    self.presentWindow!.makeToast(message: messageAert!)
                    self.view.endEditing(true)
                    self.otpTextField.isUserInteractionEnabled = true
                    self.verifyOTPButton.isUserInteractionEnabled = true
                    self.timerLabel.isHidden = false
                    self.startTimer()
                    self.sendOTPButton.isUserInteractionEnabled = false
                    self.sendOTP = true
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
    func verifyOtpRequestAPI() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().method = "login_by_otp"
        DataManager.sharedInstance().verifyOTPRequest(mobile: self.mobileNoTextField.text!, verifyOtp: otpTextField.text!, apiName:otpRequest , closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let user):
                let message = user.message
                print (user.is_error)
                if user.is_error == "1" {
                    DispatchQueue.main.async {
                        self.showValidationAlert(title: "Alert!!", message: message)
                    }
                    
                }else{
                    self.userDetails = user
                    UserDefaults.standard.set(self.userDetails.password, forKey: "password_key")
                    UserDefaults.standard.set(self.userDetails.username, forKey: "userName_key")
                    //// Profile Detail
                    UserDefaults.standard.set(self.userDetails.name, forKey: "nameProfile_key")
                    UserDefaults.standard.set(self.userDetails.email, forKey: "email_key")
                    UserDefaults.standard.set(self.userDetails.contactNo, forKey: "contactNo_key")
                    UserDefaults.standard.set(self.userDetails.emp_id, forKey: "empID_key")
                    UserDefaults.standard.set(self.userDetails.employeeId, forKey: "employee_key")
                    UserDefaults.standard.set(self.userDetails.officId, forKey: "office_key")
                    UserDefaults.standard.set(self.userDetails.qrImage, forKey: "qrImage_key")
                    UserDefaults.standard.set(self.userDetails.profileImage, forKey: "profileImage_key")
//                    UserDefaults.standard.set(self.userDetails.receptionId, forKey: "receptionId_key")

                    UserDefaults.standard.synchronize()
                    
                    DataManager.sharedInstance().username = UserDefaults.standard.value(forKey: "userName_key") as? String ?? ""
                    DataManager.sharedInstance().password = UserDefaults.standard.value(forKey: "afterLoginPass_key") as? String ?? ""
                    
                    self.hideLoadingIndictor()
                    if self.otpTextField.text == self.otp {
                        if self.sendOTP == true {
//                            let alertController = UIAlertController(title: "", message: "Change Password", preferredStyle: UIAlertControllerStyle.alert)
//
//                            let save = UIAlertAction(title: "Save", style: .default) { (alertAction) in
//                                let textField = alertController.textFields![0] as UITextField
//                                let textField2 = alertController.textFields![1] as UITextField
//                                self.newPassword = textField.text!
//                                self.confPassword = textField2.text!
//                                if self.newPassword == self.confPassword{
//                                    self.ChangePasswprdVistors()
//                                }else{
//                                    self.presentWindow!.makeToast(message: "Password Mismatched")
//                                }
//
//                            }
//                            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
//                                (action : UIAlertAction!) -> Void in
//
//                            })
//
//                            alertController.addTextField { (textField : UITextField!) -> Void in
//                                textField.placeholder = "New Password"
//                            }
//                            alertController.addTextField { (textField : UITextField!) -> Void in
//                                textField.placeholder = "Confirm Password"
//                            }
//
//                            alertController.addAction(save)
//                            alertController.addAction(cancelAction)
//
//                            self.present(alertController, animated: true, completion: nil)
                            let changePasswordPopUpVC = ChangePasswordPopUpVC(nibName: "ChangePasswordPopUpVC", bundle: nil)
                            changePasswordPopUpVC.modalPresentationStyle = .overCurrentContext
                            changePasswordPopUpVC.modalTransitionStyle = .crossDissolve
                            changePasswordPopUpVC.delegate = self
                            // Present View "Modally"
                            self.present(changePasswordPopUpVC, animated: true, completion: nil)
                        }else{
                            self.presentWindow!.makeToast(message: "Send OTP First")
                            
                        }
                    }else{
                        self.presentWindow!.makeToast(message: "Entered OTP is Wrong")
                    }
                    
                }
                
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    func ChangePasswprdVistors() {
       
        showLoadingIndictor()
        DataManager.sharedInstance().method = "changepassword"
        DataManager.sharedInstance().ChnagePasswordRequest(empId: self.getEmployeeID(),changPass:self.userDetails.password, newPassword:self.newPassword, confrimPass:self.confPassword, apiName: changePassword,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"].string
                let messageAert = theDictionary["message"].string
                if errorCode == "0"{
                    let alertController = UIAlertController(title: "Alert!!", message: messageAert, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        if self.vcIdentifier == "LoginOTP" {
                            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
                            UIApplication.shared.keyWindow?.rootViewController = mainVC
                         
                        }else{
                            self.LogoutVistors()
                        }
                    }
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                   
                }else{
                    self.showValidationAlert(title: "", message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
    
    func LogoutVistors() {
        showLoadingIndictor()
        DataManager.sharedInstance().method = "logout"
        DataManager.sharedInstance().LogoutRequest(empId: self.getEmployeeID(), apiName: logout,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                UserDefaults.standard.removeObject(forKey: "userName_key")
                UserDefaults.standard.removeObject(forKey: "password_key")
                let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
                UIApplication.shared.keyWindow?.rootViewController = mainVC
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    @IBAction func verifyOTPAction(_ sender: Any) {
            self.verifyOtpRequestAPI()
        
    }
    
  
    @IBAction func sendOtpAction(_ sender: AnyObject) {
//        if isValidatePhoneNumber(value: mobileNoTextField.text!){
            self.otpRequestAPI()
//        }else{
//            self.showValidationAlert(title: "", message: "Mobile Number is Wrong")
//        }
        
    }
    
}

extension ChangePasswordVC : ChangePasswordPopUpVCDelegate{
    
    func didSetNewPassword(newPwd: String) {
        self.newPassword = newPwd
        self.confPassword = newPwd
        ChangePasswprdVistors()
    }
}

extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}
