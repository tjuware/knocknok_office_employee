//
//  FooterView.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 30/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import Foundation
import UIKit

@objc protocol FooterViewDelegate {
    @objc optional func didSelectHome()
    @objc optional func didSelectPhone()
    @objc optional func didSelectSettings()
}

class FooterView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    
    var delegate : FooterViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        
        let str = NSStringFromClass(type(of: self))
        let className = str.components(separatedBy:".").last! as String
        Bundle.main.loadNibNamed(className, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        if #available(iOS 11.0, *) {
            layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    override func layoutSubviews() {
        
        layer.cornerRadius = 20.0
        self.clipsToBounds = true
        let gradientBot = CAGradientLayer()
        gradientBot.frame = bounds
        gradientBot.colors = [UIColor(red: 255.00/255.0, green: 153.00/255.0, blue: 102.00/255.0, alpha: 1.00).cgColor, UIColor(red: 255.00/255.0, green: 94.00/255.0, blue: 98.00/255.0, alpha: 1.00).cgColor]
        gradientBot.startPoint = CGPoint(x: 0.1, y: 0.78)
        gradientBot.endPoint = CGPoint(x: 1.0, y: 0.78)
        layer.insertSublayer(gradientBot, at: 0)
        
    }
    
    //MARK : Calling Delegates
    @IBAction func homeButtonClicked(_ sender: UIButton) {
        if let delegate = self.delegate {
             delegate.didSelectHome?()
        }
    }
    
    @IBAction func phoneButtonClicked(_ sender: UIButton) {
//        if let delegate = self.delegate {
//             delegate.didSelectHome?()
//        }
    }
    
    @IBAction func settingsButtonClicked(_ sender: UIButton) {
        if let delegate = self.delegate {
             delegate.didSelectSettings?()
        }
    }
    
}

