//
//  ProfileVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 28/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileVC: MasterVC {
    @IBOutlet weak var imageView_Profile: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cnst_ImageViewProfileTop: NSLayoutConstraint!
    
    var titleArray = ["Name","Contact","Email","Employee Id"]
    var descArray = [String]()
//    let imageURL = "http://www.digitalgorkha.com/officeupload/"
    let imageURL = "http://knocknok.co/officeupload"
    let qrImageURL = "http://www.digitalgorkha.com/officeupload/QR_images/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Adjusting ImageViewTop to center
        cnst_ImageViewProfileTop.constant = -(imageView_Profile.frame.height/2)
        
        //Setting Profile Image
        setImage()
        
        //TapGesture to zoom image
        imageView_Profile?.isUserInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        imageView_Profile?.addGestureRecognizer(tapRecognizer)
        
        //Register tableview
        registerTableView()
        
        //Confirming FooterViewDelegates
        footerView.delegate = self
        
        //Getting Values set at login from userdefaults and adding to descArray
        let userName: String? = UserDefaults.standard.string(forKey: "nameProfile_key")
        let usermobile: String? = UserDefaults.standard.string(forKey: "contactNo_key")
        let employeeID: String? = UserDefaults.standard.string(forKey: "empID_key")
        let userEmail: String? = UserDefaults.standard.string(forKey: "email_key")

        descArray = [(userName ?? ""),(usermobile ?? ""),userEmail ?? "",employeeID ?? ""]
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setRound(view: self.imageView_Profile, radius: self.imageView_Profile.frame.size.width/2, clr: UIColor.white, bw: 1)
    }
    
    func registerTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 80
        tableView.register(UINib(nibName: "ProfileOptionsTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileOptionsTableViewCell")
    }
    
    func setImage(){
        let profileImage: String? = UserDefaults.standard.string(forKey: "profileImage_key")
        print(profileImage!)
        let profileImg = imageURL + profileImage!
        imageView_Profile.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "girl"))
    }
    
    @objc private func imageTapped() {
        Utils.zoomImageView(image: imageView_Profile?.image, viewCtrlParent: self)
    }
}

extension ProfileVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileOptionsTableViewCell", for: indexPath) as! ProfileOptionsTableViewCell
        cell.setData(title: titleArray[indexPath.row],Desc:descArray[indexPath.row])
        return cell
    } 
}

    //MARK : FooterViewDelegates
extension ProfileVC : FooterViewDelegate{
    func didSelectHome() {
        jumpToVC(viewController: HomeVC(),isFromSB: true)
    }
    
    func didSelectSettings() {
        jumpToVC(viewController: SettingsVC(),isFromSB: true)
    }
}
