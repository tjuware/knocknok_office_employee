//
//  ProfileOptionsTableViewCell.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 28/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

class ProfileOptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_Description: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(title:String,Desc:String){
        self.lbl_Title.text = title
        self.lbl_Description.text = Desc
    }
    
}
