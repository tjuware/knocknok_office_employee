//
//  TopView.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 28/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

protocol TopViewDelegate {
    func didClickedBack()
    func didClickedInfo()
}

class TopView: UIView {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btn_Back: UIButton!
    @IBOutlet weak var btn_Info: UIButton!
    
    var delegate:TopViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func initWithDelegate(delegateController:UIViewController){
        self.btn_Back.isHidden = false
        self.delegate = delegateController
    }
    
    @IBAction func btn_ActionBack(_ sender: UIButton) {
        self.delegate?.didClickedBack()
    }
    
    @IBAction func btn_ActionInfo(_ sender: UIButton) {
        self.delegate?.didClickedInfo()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        
        let str = NSStringFromClass(type(of: self))
        let className = str.components(separatedBy:".").last! as String
        Bundle.main.loadNibNamed(className, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        if #available(iOS 11.0, *) {
            layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    func setLabelText(lbl_text : String){
        lbl_title.isHidden = false
        lbl_title.text = lbl_text
        
    }
    override func layoutSubviews() {
        
        layer.cornerRadius = 20.0
        self.clipsToBounds = true
        let gradientBot = CAGradientLayer()
        gradientBot.frame = bounds
        gradientBot.colors = [UIColor(red: 255.00/255.0, green: 153.00/255.0, blue: 102.00/255.0, alpha: 1.00).cgColor, UIColor(red: 255.00/255.0, green: 94.00/255.0, blue: 98.00/255.0, alpha: 1.00).cgColor]
        gradientBot.startPoint = CGPoint(x: 0.1, y: 0.78)
        gradientBot.endPoint = CGPoint(x: 1.0, y: 0.78)
        layer.insertSublayer(gradientBot, at: 0)
        
    }
}


