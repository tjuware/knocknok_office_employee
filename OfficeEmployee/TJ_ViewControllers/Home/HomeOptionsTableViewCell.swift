//
//  HomeOptionsTableViewCell.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 30/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

class HomeOptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageView_Option: UIImageView!
    @IBOutlet weak var lbl_OptionTitle: UILabel!
    @IBOutlet weak var lbl_OptionDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(optionDict:[String:String]){
        self.imageView_Option.image = UIImage(named: optionDict["Image"]!)
        self.lbl_OptionTitle.text = optionDict["Title"]
        self.lbl_OptionDescription.text = optionDict["Description"]
    }
    
}
