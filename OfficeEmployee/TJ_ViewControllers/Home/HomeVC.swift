//
//  HomeVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 28/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit
import SDWebImage

class HomeVC: MasterVC {
    
    @IBOutlet weak var imageView_Profile: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var cnst_ImageViewProfileTop: NSLayoutConstraint!
    
    let imageURL = "http://knocknok.co/officeupload"
    var OptionsArray = [["Image":"Asset 1@4x","Title":"Visitors","Description":"Today,Past & Expected Visitors"],["Image":"call","Title":"Important Contacts","Description":"Your Primary Contacts"],["Image":"call","Title":"Delivery Logs","Description":"Your Delivery Logs"],["Image":"call","Title":"Block List","Description":"Add Block List"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        footerView.delegate = self
        cnst_ImageViewProfileTop.constant = -(imageView_Profile.frame.height/2)
        setImage()
        registerTableView()
        addGestureToImageView()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setRound(view: self.imageView_Profile, radius: self.imageView_Profile.frame.size.width/2, clr: UIColor.white, bw: 1)
    }
    
    func setImage(){
        let profileImage: String? = UserDefaults.standard.string(forKey: "profileImage_key")
        let profileImg = imageURL + profileImage!
        imageView_Profile.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "girl"))
    }
    
    func registerTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 100
        tableView.register(UINib(nibName: "HomeOptionsTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeOptionsTableViewCell")
    }
    
    func addGestureToImageView(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView_Profile.isUserInteractionEnabled = true
        imageView_Profile.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let profileVC = mainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.app.navController.pushViewController(profileVC, animated: true)
    }
}

extension HomeVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OptionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeOptionsTableViewCell", for: indexPath) as! HomeOptionsTableViewCell
        cell.configureCell(optionDict: OptionsArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            jumpToVC(viewController: VisitorListVC(),isFromSB: false)
        case 1:
            jumpToVC(viewController: impContactsVC(), isFromSB: true)
        case 2:
            jumpToVC(viewController: DeliveryLogs(), isFromSB: false)
        case 3:
            jumpToVC(viewController: BlackListVC(), isFromSB: false)
        default:
            print("error")
        }
    }
}


extension HomeVC : FooterViewDelegate{
    
    func didSelectSettings() {
        jumpToVC(viewController: SettingsVC(), isFromSB: true)
    }
}
