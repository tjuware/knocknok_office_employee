//
//  VisitorVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 06/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//


//
//  VisitorVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 04/05/20.
//  Copyright © 2020 James Frost. All rights reserved.
//

import UIKit
import Contacts

class VisitorVC: MasterVC,UIScrollViewDelegate {
    
    //MARK : Outlets
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var lbl_One: UILabel!
    @IBOutlet weak var lbl_two: UILabel!
    @IBOutlet weak var lbl_Three: UILabel!
    
    @IBOutlet weak var view_MoreInfo: UIView!
    @IBOutlet weak var lbl_CompanyName: UILabel!
    @IBOutlet weak var lbl_Purpose: UILabel!
    @IBOutlet weak var lbl_CheckIn: UILabel!
    @IBOutlet weak var lbl_CheckOut: UILabel!
    
    
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var footer_View: FooterView!
    @IBOutlet weak var addVisitorsView: UIView!
    //    @IBOutlet weak var cnst_TopViewHeight: NSLayoutConstraint!
 
    @IBOutlet weak var cnst_AddVisitorViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cnst_CalendarViewHeight: NSLayoutConstraint!
    //  @IBOutlet weak var cnst_BottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var btnFrom: DesignableButton!
    @IBOutlet weak var btnTo: DesignableButton!
    @IBOutlet weak var middleView: UIView!
    
    //MARK : VARIABLES
    
    
    var tableViewOne = UITableView()
    var tableViewTwo = UITableView()
    var tableViewThree = UITableView()
    var displayWidth: CGFloat?
    var displayHeight: CGFloat?
    var scrollView = UIScrollView()
    var containerView = UIView()
    var currentIndex = 0
    var changeIndex = 100
    var labelArray = [UILabel]()
    var liveVisitorsArray = [LiveVisitors]()
    var pastVisitorsArray = [LiveVisitors]()
    var expectedVistorsArray = [ExpectedVisitor]()
    var visitor:LiveVisitors?
    var visitorHelper:VistorHelperClass?{
        didSet{
            self.liveVisitorsArray = self.visitorHelper?.liveVisitors ?? []
            self.pastVisitorsArray = self.visitorHelper?.PastVisitors ?? []
            
            switch self.currentIndex {
            case 0:
                tableViewOne.reloadData()
                tableViewThree.reloadData()
            case 2:
                tableViewTwo.reloadData()
            default:
                print("No Reload")
            }
        }
    }
    var startDateString = ""
    var endDateString = ""
    //  var expectedVisitorsArray = ExpectedVisitors.visitors()
    var isFromNotification:Bool = false
    var selectedStartDate = ""
    var selectedEndDate = ""
    var notificationData:NSDictionary?
    var didAddEpectedVisitor = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelArray = [lbl_One,lbl_two,lbl_Three]
        
        //Setting Segment Control
        let selectedtitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 255.00/255.0, green: 94.00/255.0, blue: 98.00/255.0, alpha: 1.00)]
        UISegmentedControl.appearance().setTitleTextAttributes(selectedtitleTextAttributes, for: .selected)
        let normaltitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        UISegmentedControl.appearance().setTitleTextAttributes(normaltitleTextAttributes, for: .normal)
        
        //MARK : LiveVisitorsNotificationPopUp
        //        if (isFromNotification == true) {
        //            isFromNotification = false
        //            let liveVisitorsNotificationPopUpVC = LiveVisitorsNotificationPopUpVC(nibName: "LiveVisitorsNotificationPopUpVC", bundle: nil)
        //            liveVisitorsNotificationPopUpVC.delegate = self
        //            if let notificationdata = notificationData{
        //                liveVisitorsNotificationPopUpVC.visitorData = notificationdata
        //            }
        //            liveVisitorsNotificationPopUpVC.modalPresentationStyle = .overCurrentContext
        //            liveVisitorsNotificationPopUpVC.modalTransitionStyle = .crossDissolve
        //            // Present View "Modally"
        //            self.present(liveVisitorsNotificationPopUpVC, animated: true, completion: nil)
        //        }
        
        setTopTitle(title: "VISITORS")
        footer_View.delegate = self
        self.topView.delegate = self
        self.topView.btn_Info.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if(!didAddEpectedVisitor){
            setupScrollView()
            
            addTableViews()
            
            segementControlChanged(currentPage: 0)
        }
    }
    
    func setupScrollView(){
        //Scrollview setup
        displayWidth = self.middleView.frame.size.width
        displayHeight = self.middleView.frame.size.height
        
        scrollView.frame = middleView.bounds
        scrollView.contentSize = CGSize(width: displayWidth!*3, height: displayHeight!)
        scrollView.alwaysBounceVertical = false
        //    scrollView.contentOffset.x = middleView.bounds.origin.x
        //    scrollView.contentOffset.y = middleView.bounds.origin.y
        scrollView.backgroundColor = UIColor.clear
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        middleView.addSubview(scrollView)
    }
    
    func addTableViews(){
        addTableView(xpoint: displayWidth!, ypoint: middleView.bounds.origin.y, bgColor: .clear, tableViewNum: 1)
        addTableView(xpoint: 2*(displayWidth!), ypoint: middleView.bounds.origin.y, bgColor: .clear, tableViewNum: 2)
        addTableView(xpoint: 0, ypoint: middleView.bounds.origin.y, bgColor: .clear, tableViewNum: 0)
    }
    
    func addTableView(xpoint:CGFloat,ypoint:CGFloat,bgColor:UIColor,tableViewNum:Int){
        switch tableViewNum {
        case 0:
            tableViewOne.removeFromSuperview()
            tableViewOne.register(UINib(nibName: "LiveVisitorTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveVisitorTableViewCell")
            tableViewOne.separatorStyle = .none
            tableViewOne.backgroundColor = bgColor
            tableViewOne.delegate = self
            tableViewOne.dataSource = self
            tableViewOne.frame = CGRect(x: xpoint, y: ypoint, width: displayWidth!, height: displayHeight!)
            scrollView.addSubview(tableViewOne)
//            scrollToVisibleFrame(rect: tableViewOne.frame)
            break
        case 1:
            tableViewTwo.removeFromSuperview()
            tableViewTwo.register(UINib(nibName: "ExpectedVisitorTableViewCell", bundle: nil), forCellReuseIdentifier: "ExpectedVisitorTableViewCell")
            tableViewTwo.separatorStyle = .none
            tableViewTwo.backgroundColor = bgColor
            tableViewTwo.delegate = self
            tableViewTwo.dataSource = self
            tableViewTwo.frame = CGRect(x: xpoint, y: ypoint, width: displayWidth!, height: displayHeight!)
            scrollView.addSubview(tableViewTwo)
            break
        case 2:
            tableViewThree.removeFromSuperview()
            tableViewThree.register(UINib(nibName: "PastVisitorTableViewCell", bundle: nil), forCellReuseIdentifier: "PastVisitorTableViewCell")
            tableViewThree.separatorStyle = .none
            tableViewThree.backgroundColor = bgColor
            tableViewThree.delegate = self
            tableViewThree.dataSource = self
            tableViewThree.frame = CGRect(x: xpoint, y: ypoint, width: displayWidth!, height: displayHeight!)
            scrollView.addSubview(tableViewThree)
            break
        default:
            break
        }
        
    }
    
    @IBAction func didChangeSegmentControl(_ sender: UISegmentedControl) {
        currentIndex = sender.selectedSegmentIndex
        segementControlChanged(currentPage: currentIndex)
    }
    
    func segementControlChanged(currentPage:Int){
        segmentControl.selectedSegmentIndex = currentPage
        changeIndex = currentPage
        
//        (currentIndex == 2) ? hideShowCalendarView(shouldShowCalendar: false, cnst_Height: 0.08):hideShowCalendarView(shouldShowCalendar: true, cnst_Height: 0.0001)
        
//        (currentIndex == 1) ? hideShowAddVisitorView(shouldShowCalendar: false, cnst_Height: 0.1):hideShowAddVisitorView(shouldShowCalendar: true, cnst_Height: 0.0001)
        
        (currentIndex == 2) ? hideShowCalendarView(shouldShowCalendar: false, cnst_Height: 55):hideShowCalendarView(shouldShowCalendar: true, cnst_Height: 0)
                
        (currentIndex == 1) ? hideShowAddVisitorView(shouldShowCalendar: false, cnst_Height: 55):hideShowAddVisitorView(shouldShowCalendar: true, cnst_Height: 0)
        
        for index in 0..<3{
            if(index == currentIndex){
                labelArray[index].backgroundColor = UIColor(red: 255.00/255.0, green: 94.00/255.0, blue: 98.00/255.0, alpha: 1.00)
            }else{
                labelArray[index].backgroundColor = UIColor.clear
            }
        }
        switch segmentControl.selectedSegmentIndex {
        case 0:
            guard let startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date()) else { return }
            startDateString = self.formatDateAsDay(date: startDate)
            endDateString = self.formatDateAsDay(date: Date())
            getLiveVisters(startDate: startDateString, endDate: endDateString)
            scrollToVisibleFrame(rect: tableViewOne.frame)
            break
        case 1:
            getExpectedVistors()
            scrollToVisibleFrame(rect: tableViewTwo.frame)
            break
        case 2:
            setUpInitialDates()
            getLiveVisters(startDate: startDateString, endDate: endDateString)
            scrollToVisibleFrame(rect: tableViewThree.frame)
            break
        default:
            break
        }
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(self.scrollView.contentOffset.x,self.scrollView.contentOffset.y)
        
        if(self.scrollView.contentOffset.x>=0){
            let currentPage = self.scrollView.contentOffset.x / self.view.frame.size.width
            currentIndex = Int(currentPage)
            if(changeIndex != currentIndex){
                segementControlChanged(currentPage: currentIndex)
            }
        }
    }
    
    // MARK : Helper Methods
    
    func reloadTableView(num:Int){
        DispatchQueue.main.async {
            if num == 0{
                guard let startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date()) else { return }
                self.startDateString = self.formatDateAsDay(date: startDate)
                self.endDateString = self.formatDateAsDay(date: Date())
                self.getLiveVisters(startDate: self.startDateString, endDate: self.endDateString)
            }else if num == 1{
                self.tableViewTwo.reloadData()
            }else{
                self.setUpInitialDates()
                self.getLiveVisters(startDate: self.startDateString, endDate: self.endDateString)
            }
        }
    }
    
    func scrollToVisibleFrame(rect:CGRect){
        scrollView.scrollRectToVisible(rect, animated: false)
    }
    
    private func setUpInitialDates(){
        selectedEndDate = formatDate(date: Date())
        guard let selStartDate = Calendar.current.date(byAdding: .day, value: -7, to: Date()) else { return }
        selectedStartDate = formatDate(date: selStartDate)
        //For buttons to display in day format
        startDateString = formatDateAsDay(date: selStartDate)
        endDateString = formatDateAsDay(date: Date())
        self.btnFrom.setTitle("From : \(startDateString)", for: .normal)
        self.btnTo.setTitle("To : \(endDateString)", for: .normal)
    }
    private func hideShowCalendarView(shouldShowCalendar:Bool,cnst_Height:CGFloat){
//        self.cnst_CalendarViewHeight = self.cnst_CalendarViewHeight.setMultiplier(multiplier: cnst_Height)
        self.cnst_CalendarViewHeight.constant = cnst_Height
        calendarView.isHidden = shouldShowCalendar
        self.view.layoutIfNeeded()
    }
    
    private func hideShowAddVisitorView(shouldShowCalendar:Bool,cnst_Height:CGFloat){
//        self.cnst_AddVisitorViewHeight = self.cnst_AddVisitorViewHeight.setMultiplier(multiplier: cnst_Height)
        self.cnst_AddVisitorViewHeight.constant = cnst_Height
        addVisitorsView.isHidden = shouldShowCalendar
        self.view.layoutIfNeeded()
    }
    
    // MARK : Outlet Actions
    
    @IBAction func btnAction_Cancel(_ sender: UIButton) {
        view_MoreInfo.isHidden = true
    }
    
    @IBAction func btnActionDateSelected(_ sender: DesignableButton) {
        
        let pastVisitorsCalendarPopUpVC = PastVisitorsCalendarPopUpVC(nibName: "PastVisitorsCalendarPopUpVC", bundle: nil)
        pastVisitorsCalendarPopUpVC.delegate = self
        pastVisitorsCalendarPopUpVC.selectedButtonIndex = sender.tag
        pastVisitorsCalendarPopUpVC.selectedStartDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())!
        pastVisitorsCalendarPopUpVC.selectedEndDate = Date()
        pastVisitorsCalendarPopUpVC.modalPresentationStyle = .overCurrentContext
        pastVisitorsCalendarPopUpVC.modalTransitionStyle = .crossDissolve
        // Present View "Modally"
        self.present(pastVisitorsCalendarPopUpVC, animated: true, completion: nil)
        
    }
    
    @IBAction func addVisitorButtonClicked(_ sender: DesignableButton) {
        
        let contactOtherPopUpVC = ContactOtherPopUpVC(nibName: "ContactOtherPopUpVC", bundle: nil)
        contactOtherPopUpVC.delegate = self
        contactOtherPopUpVC.modalPresentationStyle = .overCurrentContext
        contactOtherPopUpVC.modalTransitionStyle = .crossDissolve
        // Present View "Modally"
        self.present(contactOtherPopUpVC, animated: true, completion: nil)
    }
    
    //MARK : API CALLING
    func getLiveVisters(startDate: String, endDate:String) {
        
        showLoadingIndictor()
        liveVisitorsArray = []
        DataManager.sharedInstance().method = "live_visitor"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().LiveVistorsRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(),startDate:startDateString ,endDate:endDateString, apiName: liveVisitors,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            
            switch result {
            case .success(let data):
                self.visitorHelper = VistorHelperClass.init(allVisitor: data)
                switch self.currentIndex {
                case 0:
                    DispatchQueue.main.async {
                        self.tableViewOne.reloadData()
                    }
                case 2:
                    DispatchQueue.main.async {
                        self.tableViewThree.reloadData()
                    }
                default:
                    break
                }
                
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
    func getExpectedVistors() {
        showLoadingIndictor()
        DataManager.sharedInstance().method = "expected_visitor"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().expectVistorsRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), apiName: expectedVisitors,closure: { (result) in
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)                
                self.expectedVistorsArray = data;
                
                if self.expectedVistorsArray.count == 0 {
                    //                    self.noDataLabel.isHidden = false
                    //                    self.addUserBool = true
                } else {
                    //                    self.noDataLabel.isHidden = true
                    DispatchQueue.main.async {
                        self.tableViewTwo.reloadData()
                    }
                }
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
    func Accept(btnTag:Int,liveOrPast:Int){
        let employeName = UserDefaults.standard.string(forKey: "nameProfile_key")!
        let liveVisit = (liveOrPast == 0) ? self.liveVisitorsArray[btnTag] : self.pastVisitorsArray[btnTag]
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_allow_deny"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().AllowDenayRequest(visitorName: liveVisit.name, visitiorNo: liveVisit.contactNo, empId: self.getEmployeeID(), officeId: self.getOfficeID(), statusV: "1", employeeName: employeName, receptionId: liveVisit.receptionid, visitorId: liveVisit.visitorId,reason: liveVisit.purposeLive, userImage: liveVisit.profileImage, apiName: visitorAllowDeny, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                if errorCode == 0{
                    if liveOrPast == 0{
                        self.reloadTableView(num: 0)
                    }else{
                        self.reloadTableView(num: 2)
                    }
                    self.presentWindow!.makeToast(message: "You have Allowed Visitor")
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
    func Deny(btnTag:Int, liveOrPast: Int){
        let employeName = UserDefaults.standard.string(forKey: "nameProfile_key")!
        let liveVisit = (liveOrPast == 0) ? self.liveVisitorsArray[btnTag] : self.pastVisitorsArray[btnTag]
        let visitorId = liveVisit.visitorId
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_allow_deny"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().AllowDenayRequest(visitorName: liveVisit.name, visitiorNo: liveVisit.contactNo, empId: self.getEmployeeID(), officeId: self.getOfficeID(), statusV: "2", employeeName: employeName, receptionId:liveVisit.receptionid , visitorId: visitorId,reason: liveVisit.purposeLive, userImage: liveVisit.profileImage, apiName: visitorAllowDeny, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                print(theDictionary)
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                
                if errorCode == 0{
                    if liveOrPast == 0{
                        self.reloadTableView(num: 0)
                    }else{
                        self.reloadTableView(num: 2)
                    }
                    self.presentWindow!.makeToast(message: "You have Denied Visitor")
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    func checkOutMethod(btnTag:Int, liveOrPast: Int)  {
        let liveVisit = (liveOrPast == 0) ? self.liveVisitorsArray[btnTag] : self.pastVisitorsArray[btnTag]
        let visitorIdContact = liveVisit.visitorId
        let dateFormatter : DateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "checkout_visitor"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().CheckOutRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(),visitorId: visitorIdContact, currentDate: dateString ,apiName: visitor_checkout,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                let errorCode = theDictionary["error_code"].string
                let messageAlert = theDictionary["checkout_details"].string

                if errorCode == "0"{
                    if liveOrPast == 0{
                        self.reloadTableView(num: 0)
                    }else{
                        self.reloadTableView(num: 2)
                    }
                    self.presentWindow!.makeToast(message: "Checkout Done")
                }else{
                    self.presentWindow!.makeToast(message: messageAlert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
    
    func addImpContact(btnTag:Int){
        if(currentIndex == 0){
            visitor = liveVisitorsArray[btnTag]
        }else{
            visitor = pastVisitorsArray[btnTag]
        }
        if let visitor = visitor{
            let visitorIdContact = visitor.visitorId
            let checkId = visitor.checkId
            let isImp = visitor.isImpContact
        
            if(isImp != "1"){
                showLoadingIndictor()
                DataManager.sharedInstance().method = "imp_contact"
                DataManager.sharedInstance().AddImpRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(),visitorId: visitorIdContact, starFlag: "1", checkID: checkId ,apiName: add_imp_contact,closure: { (result) in
                    print(result)
                    self.hideLoadingIndictor()
                    switch result {
                    case .success(let data):
                        let theDictionary = data[0]
                        print(theDictionary)
                        let errorCode = theDictionary["error_code"].string
                        let messageAlert = theDictionary["message"].string

                        if errorCode == "0"{
                           self.presentWindow!.makeToast(message: messageAlert!)
                            DispatchQueue.main.async {
                                self.tableViewOne.reloadData()
                                self.tableViewThree.reloadData()
                            }
                        }else{
                           self.presentWindow!.makeToast(message: messageAlert!)
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                    }
                })
            }else{
                self.presentWindow!.makeToast(message: "Visitor already added!")
            }
            
        }
        
    }
    
    func displayMoreInfo(btnTag:Int){
        if(currentIndex == 0){
            visitor = liveVisitorsArray[btnTag]
        }else{
            visitor = pastVisitorsArray[btnTag]
        }
        if let visitor = visitor{
            view_MoreInfo.isHidden = false
            view_MoreInfo.layer.borderWidth = 1.0
            view_MoreInfo.layer.borderColor = UIColor(red:241/255, green:100/255, blue:99/255, alpha: 1).cgColor
            lbl_CompanyName.text = ":  \(visitor.deliveryCompanyName)"
            lbl_Purpose.text = ":  \(visitor.purposeLive)"
            lbl_CheckIn.text = ":  \(visitor.checkIn)"
            if(visitor.checkOut == ""){
                lbl_CheckOut.text = ":  0000-00-00 00:00:00"
            }else{
                lbl_CheckOut.text = ":  \(visitor.checkOut)"
            }
            
        }
    }

}

extension VisitorVC : PastVisitorsCalendarPopUpVCDelegate{
    
    func didSelectDate(date: Date, selectedButtonIndex: Int) {
        print(date)
        if(selectedButtonIndex == 1){
            startDateString = formatDateAsDay(date: date)
            btnFrom.setTitle("From : \(startDateString)", for: .normal)
        }else if(selectedButtonIndex == 2){
            endDateString = formatDateAsDay(date: date)
            btnTo.setTitle("To : \(endDateString)", for: .normal)
        }
        getLiveVisters(startDate: startDateString, endDate: endDateString)
    }
    
}

extension VisitorVC: OtherVCDelegate{
    func didAddExpectedVisitor(){
        getExpectedVistors()
    }
}

extension VisitorVC: PhoneContactsVCDelegate{
    func didAddMultipleExpectedVisitor() {
        didAddEpectedVisitor = true
        getExpectedVistors()
    }
}


extension VisitorVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(currentIndex)
        switch currentIndex {
        case 0:
            return liveVisitorsArray.count
        case 1:
            return expectedVistorsArray.count
        case 2:
            print(pastVisitorsArray.count)
            return pastVisitorsArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch currentIndex {
        case 0:
            
            let cell:LiveVisitorTableViewCell = self.tableViewOne.dequeueReusableCell(withIdentifier: "LiveVisitorTableViewCell", for: indexPath) as! LiveVisitorTableViewCell
            cell.delegate = self
            let liveVisitor: LiveVisitors = liveVisitorsArray[indexPath.row]
            cell.lbl_VIsitorPhoneNo.textColor = (liveVisitor.isImpContact == "1")  ?   UIColor.green : UIColor.red
            cell.btn_Imp.tag = indexPath.row
            cell.btn_MoreInfo.tag = indexPath.row
            cell.btnAccept.tag = indexPath.row
            cell.btnDeny.tag = indexPath.row
            cell.btnCheckout.tag = indexPath.row
            cell.configureCell(liveVisitor: liveVisitor)
            return cell
            
        case 1:
            
            let cell:ExpectedVisitorTableViewCell = self.tableViewTwo.dequeueReusableCell(withIdentifier: "ExpectedVisitorTableViewCell", for: indexPath) as! ExpectedVisitorTableViewCell
            let expectedVisitor: ExpectedVisitor = expectedVistorsArray[indexPath.row]
            print(expectedVisitor.name)
            cell.configureCell(expectedVistor: expectedVisitor)
            
            return cell
            
        case 2:
            let cell:PastVisitorTableViewCell = self.tableViewThree.dequeueReusableCell(withIdentifier: "PastVisitorTableViewCell", for: indexPath) as! PastVisitorTableViewCell
            cell.delegate = self
            let pastVisitor: LiveVisitors = pastVisitorsArray[indexPath.row]
            cell.lbl_VIsitorPhoneNo.textColor = (pastVisitor.isImpContact == "1")  ?   UIColor.green : UIColor.red
            cell.btn_Imp.tag = indexPath.row
            cell.btn_MoreInfo.tag = indexPath.row
            cell.btnAccept.tag = indexPath.row
            cell.btnDeny.tag = indexPath.row
            cell.btnCheckout.tag = indexPath.row
            cell.configureCell(liveVisitor: pastVisitor)
            return cell
            
        default:
            break
        }
        let cell:LiveVisitorTableViewCell = self.tableViewOne.dequeueReusableCell(withIdentifier: "LiveVisitorTableViewCell", for: indexPath) as! LiveVisitorTableViewCell
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (currentIndex) {
        case 0:
            return (liveVisitorsArray.count != 0 ? UITableViewAutomaticDimension : 0)
        case 1:
            return (expectedVistorsArray.count != 0 ? 100 : 0)
        case 2:
            return (pastVisitorsArray.count != 0 ? UITableViewAutomaticDimension : 0)
        default:
            print("Unable to load cell")
        }
        return 0
    }
}

extension VisitorVC : LiveVisitorTableViewCellDelegate,PastVisitorTableViewCellDelegate{
    func didClickBtnAcceptLive(tag: Int) {
        Accept(btnTag: tag, liveOrPast: 0)
    }
    
    func didClickBtnDenyLive(tag: Int) {
        Deny(btnTag: tag, liveOrPast: 0)
    }
    
    func didClickBtnCheckoutLive(tag: Int) {
        checkOutMethod(btnTag: tag, liveOrPast: 0)
    }
    
    
    func didClickBtnImpLive(tag: Int) {
        addImpContact(btnTag: tag)
    }
    
    func didClickBtnMoreInfoLive(tag: Int) {
        displayMoreInfo(btnTag: tag)
    }
    
    func didClickBtnAcceptPast(tag: Int) {
        Accept(btnTag: tag, liveOrPast: 1)
    }
    
    func didClickBtnDenyPast(tag: Int) {
        Deny(btnTag: tag, liveOrPast: 1)
    }
    
    func didClickBtnCheckoutPast(tag: Int) {
        checkOutMethod(btnTag: tag, liveOrPast: 1)
    }
    
    func didClickBtnImpPast(tag: Int) {
        addImpContact(btnTag: tag)
    }

    func didClickBtnMoreInfoPast(tag: Int) {
        displayMoreInfo(btnTag: tag)
    }
    
}


//extension VisitorVC : LiveVisitorTableViewCellDelegate,PastTableViewCellDelegate
//{
//    func didSelectButton() {
//        self.loadLiveVisitorsData()
//    }
//
//    func didButtonClickFromPastVisitors() {
//        self.loadLiveVisitorsData()
//    }
//
//}

extension VisitorVC: ContactOtherPopUpVCDelegate{
    
    func didSelectFromContacts() {
        self.dismiss(animated: true, completion: nil)
        requestAccess { (access) in
            if(access){
                DispatchQueue.main.async {
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let phoneContactsVC = mainStoryboard.instantiateViewController(withIdentifier: "PhoneContactsVC") as! PhoneContactsVC
                    phoneContactsVC.delagate = self
                    self.app.navController.pushViewController(phoneContactsVC, animated: true)
                }
            }
        }
    }
    
    func didSelectOther() {
        self.dismiss(animated: true, completion: nil)
        let otherVC = OtherVC(nibName: "OtherVC", bundle: nil)
        otherVC.delegate = self
        otherVC.modalPresentationStyle = .overCurrentContext
        otherVC.modalTransitionStyle = .crossDissolve
        // Present View "Modally"
        self.present(otherVC, animated: true, completion: nil)
    }
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            let store = CNContactStore()
            store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                }
                else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplicationOpenSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                completionHandler(false)
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
}

extension VisitorVC : FooterViewDelegate{
    
    //MARK : FooterViewDelegates
    
    func didSelectHome() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.app.navController.pushViewController(homeVC, animated: true)
    }
    
    func didSelectSettings() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let settingsVC = mainStoryboard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.app.navController.pushViewController(settingsVC, animated: true)
    }
    
}

//update multiplier
extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
