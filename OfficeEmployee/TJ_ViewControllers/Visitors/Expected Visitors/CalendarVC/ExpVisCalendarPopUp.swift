//
//  ExpVisCalendarPopUp.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 08/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//
import UIKit
import FSCalendar

protocol ExpVisCalendarPopUpDelegate {
  func didSelectDate(selectedDate:Date)
}

class ExpVisCalendarPopUp: UIViewController {
  
  @IBOutlet weak var fsCalendar: FSCalendar!
  
  var delegate:ExpVisCalendarPopUpDelegate?
  var selectedDate:Date = Date()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    fsCalendar.delegate = self
    fsCalendar.dataSource = self
  }
  
  @IBAction func btnActionOkay(_ sender: UIButton) {
    self.delegate?.didSelectDate(selectedDate:selectedDate)
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func btnActionCancel(_ sender: UIButton) {
    self.dismiss(animated: true, completion: nil)
  }
    
    @IBAction func btnPrevAction(_ sender: Any) {
      
      fsCalendar.setCurrentPage(getPreviousMonth(date: fsCalendar.currentPage), animated: true)
    }
    @IBAction func btnNextAction(_ sender: UIButton) {
      
      fsCalendar.setCurrentPage(getNextMonth(date: fsCalendar.currentPage), animated: true)
    }
  
}
extension ExpVisCalendarPopUp : FSCalendarDelegate,FSCalendarDataSource{
  
  func minimumDate(for calendar: FSCalendar) -> Date {
    return Date()
  }
  
  func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
//    selectedDate = Calendar.current.date(byAdding: .day, value: 1, to: date)!
    selectedDate = date
    print("Selected Date : ",selectedDate)
  }
  
  func getNextMonth(date:Date)->Date {
    return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
  }
  
  func getPreviousMonth(date:Date)->Date {
    return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
  }
}
