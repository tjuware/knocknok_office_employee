//
//  PhoneContactsVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 08/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit
import Contacts

protocol PhoneContactsVCDelegate {
    func didAddMultipleExpectedVisitor()
}

class PhoneContactsVC: MasterVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchbar: UISearchBar!
    
    
    var contactNamesDict = [String: String]()
    var contactsArray = [[String: String]]()
    var contactsFilteredArray = [[String:String]]()
    var selectedContactsArray = [[String:String]]()
    var expectedDate:Date = Date()
    var delagate:PhoneContactsVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "PhoneContactTableViewCell", bundle: nil), forCellReuseIdentifier: "PhoneContactTableViewCell")
        fetchContacts()
        
        searchbar.delegate = self
        contactsFilteredArray = contactsArray
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.delagate?.didAddMultipleExpectedVisitor()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_ActionAddExpectedVisitor(_ sender: UIButton) {
            if(selectedContactsArray.count != 0){
              let expVisCalendarPopUp = ExpVisCalendarPopUp(nibName: "ExpVisCalendarPopUp", bundle: nil)
              expVisCalendarPopUp.delegate = self
              expVisCalendarPopUp.modalPresentationStyle = .overCurrentContext
              expVisCalendarPopUp.modalTransitionStyle = .crossDissolve
              self.present(expVisCalendarPopUp, animated: true, completion: nil)
            }else{
              let alertController = UIAlertController(title: "Error", message:
                "Name & Number Fields are mandatory.", preferredStyle: UIAlertControllerStyle.alert)
              alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
              DispatchQueue.main.async { [weak self] in
        //        self?.presentWindow!.makeToast(message: "Name & Number Fields are mandatory.")
                self?.present(alertController, animated: true, completion: nil)
              }
            }
    }
    
    func AddExpectedVistor() {
            let userName: String? = UserDefaults.standard.string(forKey: "nameProfile_key")
            showLoadingIndictor()
            DataManager.sharedInstance().method = "add_expected"

            
            DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
            DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
            DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().AddExpectedRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), expectDate: formatDateAsDay(date: expectedDate), empName: userName!, visitor_count: "\(selectedContactsArray.count)" , data: selectedContactsArray, apiName: addExpected, closure: { (result) in
                print(result)
                self.hideLoadingIndictor()
                switch result {
                case .success(let data):
                    print(data)
                    let theDictionary = data[0]
                    print(theDictionary)
                    
                    let errorCode = theDictionary["error_code"].string
                    let messageAert = theDictionary["message"].string
                    
                    if errorCode == "0"{
                                            let alertController = UIAlertController(title: "Success!", message: "Added Successfully", preferredStyle: .alert)
                                            let defaultAction = UIAlertAction(title: "OK", style: .default) { (action) in
                                                self.delagate?.didAddMultipleExpectedVisitor()
                                                self.dismiss(animated: true, completion: nil)
                                            }
                                            alertController.addAction(defaultAction)
                                            self.present(alertController, animated: true, completion: nil)
//                        self.showValidationAlertWithButton(title: "Success!", message: "Added Successfully", buttonArray: ["OK"]) { (button) in
//                            if(button == "OK"){
//                                self.delegate?.didAddExpectedVisitor()
//                            }
//                        }
                    }else{
                        self.presentWindow!.makeToast(message: messageAert!)
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                    
                }
            })
        }
}

extension PhoneContactsVC: ExpVisCalendarPopUpDelegate {
    
  func didSelectDate(selectedDate: Date) {
    expectedDate = selectedDate
    AddExpectedVistor()
  }
}


extension PhoneContactsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contactsFilteredArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneContactTableViewCell", for: indexPath) as! PhoneContactTableViewCell
        cell.btn_Check.tag = indexPath.row
        cell.btn_Check.addTarget(self, action: #selector(checkBoxButtonClicked(sender:)), for: .touchUpInside)
        cell.configureCell(contactDict: contactsFilteredArray[indexPath.row])
        return cell
    }
    
}
extension PhoneContactsVC{
    
    @objc func checkBoxButtonClicked(sender:UIButton){
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.tableView.cellForRow(at: indexPath) as! PhoneContactTableViewCell
        print(sender.image(for: .normal) as Any)
        print(UIImage(named: "checkNo") as Any)
        print(UIImage(named: "checkYes") as Any)
        switch sender.image(for: .normal) {
        case UIImage(named: "checkNo"):
            cell.btn_Check.setImage(UIImage(named: "checkYes.png"), for: .normal)
            selectedContactsArray.append(contactsArray[sender.tag])
            print(selectedContactsArray)
        case UIImage(named: "checkYes"):
            cell.btn_Check.setImage(UIImage(named: "checkNo.png"), for: .normal)
            selectedContactsArray.removeAll { (dict) -> Bool in
                return dict == contactsArray[sender.tag]
            }
            print(selectedContactsArray)
        default:
            print(sender.image(for: .normal) as Any)
            print("Error")
        }
    }
    
    private func fetchContacts(){
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { (granted, err) in
            if let error = err{
                print("failed to request Access :",error.localizedDescription)
            }
            if granted{
                let keys = [CNContactGivenNameKey,CNContactFamilyNameKey,CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do{
                    
                    try store.enumerateContacts(with: request,usingBlock:{ (contact, stopPointerToStopEnumerating) in
                        //                print(contact.givenName,contact.familyName,contact.phoneNumbers.first?.value.stringValue ?? "")
                        self.contactNamesDict["name"] = "\(contact.givenName) \(contact.familyName)"
                        self.contactNamesDict["number"] = (contact.phoneNumbers.first?.value.stringValue ?? "")
                        self.contactsArray.append(self.contactNamesDict)
                        self.contactNamesDict.removeAll()
                    })
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }catch let err{
                    print("Failed to Enumerate Contacts",err)
                }
                
                
            }else{
                let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
                if
                    let settings = URL(string: UIApplicationOpenSettingsURLString),
                    UIApplication.shared.canOpenURL(settings) {
                    alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                        UIApplication.shared.open(settings)
                    })
                }
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
                    self.dismiss(animated: true, completion: nil)
                })
                self.present(alert, animated: true)
            }
        }
    }
    
}

extension PhoneContactsVC : UISearchBarDelegate{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
        
    {
        searchBar.text = ""
        
        contactsFilteredArray = contactsArray
        
        searchBar.endEditing(true)
        DispatchQueue.main.async {
            self.tableView?.reloadData()
        }
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
        
    {
        
        contactsFilteredArray = searchText.isEmpty ? contactsArray : contactsArray.filter{
            (contact: [String:String]) -> Bool in
            
            // If dataItem matches the searchText, return true to include
            return contact["name"]?.range(of: searchText, options: .caseInsensitive, range: nil,locale: nil) != nil
        }
        tableView?.reloadData()
    }
}

