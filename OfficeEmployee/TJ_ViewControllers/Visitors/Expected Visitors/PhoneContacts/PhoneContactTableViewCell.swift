//
//  PhoneContactTableViewCell.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 08/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

class PhoneContactTableViewCell: UITableViewCell {
  
  @IBOutlet weak var lbl_ContactName: UILabel!
  @IBOutlet weak var lbl_ContactNo: UILabel!
    @IBOutlet weak var btn_Check: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
  func configureCell(contactDict:[String:String]){
    self.lbl_ContactName.text = contactDict["name"]
    self.lbl_ContactNo.text = contactDict["number"]
  }
  
}
