//
//  ExpectedVisitorTableViewCell.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 06/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

class ExpectedVisitorTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_ExpVisName: UILabel!
    @IBOutlet weak var lbl_ExpVisMobNo: UILabel!
    @IBOutlet weak var lbl_ExpDate: UILabel!

  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(expectedVistor: ExpectedVisitor) {
        print(expectedVistor.arrivalDate)
        self.lbl_ExpVisName.text = expectedVistor.name
        self.lbl_ExpVisMobNo.text = "Mobile : \(expectedVistor.contactNo)"
        self.lbl_ExpDate.text = "Date : \(expectedVistor.checkInDate)"
    }
    
}

