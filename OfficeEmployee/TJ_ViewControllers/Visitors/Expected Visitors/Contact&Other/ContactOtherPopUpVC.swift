//
//  ContactOtherPopUpVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 07/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

protocol ContactOtherPopUpVCDelegate {
  func didSelectFromContacts()
  func didSelectOther()
}

class ContactOtherPopUpVC: UIViewController {
  
  var delegate:ContactOtherPopUpVCDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
    self.view.addGestureRecognizer(tap)
  }
  
  @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func btn_ActionFromContacts(_ sender: DesignableButton) {
    self.delegate?.didSelectFromContacts()
  }
  
  @IBAction func btn_ActionOther(_ sender: Any) {
    self.delegate?.didSelectOther()
  }
  
  
}
