//
//  OtherVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 08/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//
import UIKit

protocol OtherVCDelegate {
    func didAddExpectedVisitor()
}

class OtherVC: MasterVC {
    @IBOutlet weak var txtEnterName: UITextField!
    @IBOutlet weak var txtEnterNumber: UITextField!
    @IBOutlet weak var lineOneView: UIView!
    @IBOutlet weak var lineTwoView: UIView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var bgView: UIView!
    
    var expectedDate:Date = Date()
    var delegate:OtherVCDelegate?
    var expVisArray = [[String: String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.bgView.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: middleView))!{
            return false
        }
        return true
    }
    
    @IBAction func btnActionAdd(_ sender: UIButton) {
        
        // Present View "Modally"
        if(txtEnterName.text != "" && txtEnterNumber.text != ""){
            let expVisCalendarPopUp = ExpVisCalendarPopUp(nibName: "ExpVisCalendarPopUp", bundle: nil)
            expVisCalendarPopUp.delegate = self
            expVisCalendarPopUp.modalPresentationStyle = .overCurrentContext
            expVisCalendarPopUp.modalTransitionStyle = .crossDissolve
            self.present(expVisCalendarPopUp, animated: true, completion: nil)
        }else{
            let alertController = UIAlertController(title: "Error", message:
                "Name & Number Fields are mandatory.", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            DispatchQueue.main.async { [weak self] in
                self?.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    func AddExpectedVistor() {
        let userName: String? = UserDefaults.standard.string(forKey: "nameProfile_key")
        showLoadingIndictor()
        DataManager.sharedInstance().method = "add_expected"
        //        let myCount = String(contactCountArray.count)
        let dict:[String:String] = ["number":txtEnterNumber.text!,
                                    "name":txtEnterName.text!]
        expVisArray.append(dict)
//        let visCount = String(expVisArray.count)
        
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().AddExpectedRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), expectDate: formatDateAsDay(date: expectedDate), empName: userName!, visitor_count: "\(expVisArray.count)" , data: expVisArray, apiName: addExpected, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"].string
                let messageAert = theDictionary["message"].string
                
                if errorCode == "0"{
                                        let alertController = UIAlertController(title: "Success!", message: "Added Successfully", preferredStyle: .alert)
                                        let defaultAction = UIAlertAction(title: "OK", style: .default) { (action) in
                                            self.delegate?.didAddExpectedVisitor()
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                        alertController.addAction(defaultAction)
                                        self.present(alertController, animated: true, completion: nil)
//                    self.showValidationAlertWithButton(title: "Success!", message: "Added Successfully", buttonArray: ["OK"]) { (button) in
//                        if(button == "OK"){
//                            self.delegate?.didAddExpectedVisitor()
//                        }
//                    }
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
}

extension OtherVC: ExpVisCalendarPopUpDelegate {
    func didSelectDate(selectedDate: Date) {
        expectedDate = selectedDate
        AddExpectedVistor()
        txtEnterName.text = ""
        txtEnterNumber.text = ""
        self.dismiss(animated: true, completion: nil)
    }
}
