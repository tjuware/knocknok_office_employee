//
//  VisitorListVC.swift
//  OfficeEmployee
//
//  Created by Kush Thakkar on 22/09/20.
//  Copyright © 2020 Knocknok. All rights reserved.
//

import UIKit
import Contacts

class VisitorListVC: MasterVC{
    
    //MARK : Outlets
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var lbl_One: UILabel!
    @IBOutlet weak var lbl_two: UILabel!
    @IBOutlet weak var lbl_Three: UILabel!
    
    @IBOutlet weak var view_MoreInfo: UIView!
    @IBOutlet weak var lbl_CompanyName: UILabel!
    @IBOutlet weak var lbl_Purpose: UILabel!
    @IBOutlet weak var lbl_CheckIn: UILabel!
    @IBOutlet weak var lbl_CheckOut: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var footer_View: FooterView!
    @IBOutlet weak var addVisitorsView: UIView!
    @IBOutlet weak var cnst_AddVisitorViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cnst_CalendarViewHeight: NSLayoutConstraint!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var btnFrom: DesignableButton!
    @IBOutlet weak var btnTo: DesignableButton!
    @IBOutlet weak var lblNoVisitor: UILabel!
    
    var currentIndex = 0
    var changeIndex = 100
    var labelArray = [UILabel]()
    var liveVisitorsArray = [LiveVisitors]()
    var pastVisitorsArray = [LiveVisitors]()
    var expectedVistorsArray = [ExpectedVisitor]()
    
    var visitor:LiveVisitors?
    var visitorHelper:VistorHelperClass?{
        didSet{
            self.liveVisitorsArray = self.visitorHelper?.liveVisitors ?? []
            self.pastVisitorsArray = self.visitorHelper?.PastVisitors ?? []
            switch self.currentIndex {
            case 0:
                currentIndex = 0
            case 2:
                currentIndex = 2
            default:
                print("No Reload")
            }
            if self.liveVisitorsArray.count == 0 && currentIndex == 0{
                self.lblNoVisitor.text = "No live visitors!"
                self.lblNoVisitor.isHidden = false
            }else if self.pastVisitorsArray.count == 0 && currentIndex == 2{
                self.lblNoVisitor.text = "No past visitors!"
                self.lblNoVisitor.isHidden = false
            }else{
                self.lblNoVisitor.isHidden = true
            }
            tableView.reloadData()
        }
    }
    var startDateString = ""
    var endDateString = ""
    //  var expectedVisitorsArray = ExpectedVisitors.visitors()
    var isFromNotification:Bool = false
    var selectedStartDate = ""
    var selectedEndDate = ""
    var notificationData:NSDictionary?
    var didAddEpectedVisitor = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelArray = [lbl_One,lbl_two,lbl_Three]
        
        //Setting Segment Control
        let selectedtitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 255.00/255.0, green: 94.00/255.0, blue: 98.00/255.0, alpha: 1.00)]
        UISegmentedControl.appearance().setTitleTextAttributes(selectedtitleTextAttributes, for: .selected)
        let normaltitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        UISegmentedControl.appearance().setTitleTextAttributes(normaltitleTextAttributes, for: .normal)
        
        setTopTitle(title: "VISITORS")
        footer_View.delegate = self
        self.topView.delegate = self
        self.topView.btn_Info.isHidden = false
        
        registerTableView()
        AddGestureToTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if(!didAddEpectedVisitor){
            segementControlChanged(currentPage: 0)
        }
    }
    
    func registerTableView(){
        tableView.register(UINib(nibName: "LiveVisitorTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveVisitorTableViewCell")
        tableView.register(UINib(nibName: "PastVisitorTableViewCell", bundle: nil), forCellReuseIdentifier: "PastVisitorTableViewCell")
        tableView.register(UINib(nibName: "ExpectedVisitorTableViewCell", bundle: nil), forCellReuseIdentifier: "ExpectedVisitorTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func AddGestureToTableView(){
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        gesture.direction = UISwipeGestureRecognizerDirection.right
        tableView.addGestureRecognizer(gesture)
        
        let gesture1 = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        gesture1.direction = UISwipeGestureRecognizerDirection.left
        tableView.addGestureRecognizer(gesture1)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        print(currentIndex)
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if(currentIndex>0 && currentIndex<=2){
                    currentIndex -= 1
                    segmentControl.selectedSegmentIndex = currentIndex
                    segementControlChanged(currentPage: currentIndex)
                }else{
                    currentIndex = 0
                }
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                if(currentIndex>=0 && currentIndex<2){
                    currentIndex += 1
                    segmentControl.selectedSegmentIndex = currentIndex
                    segementControlChanged(currentPage: currentIndex)
                }else{
                    currentIndex = 2
                }
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @IBAction func didChangeSegmentControl(_ sender: UISegmentedControl) {
        currentIndex = sender.selectedSegmentIndex
        segementControlChanged(currentPage: currentIndex)
    }
    
    @IBAction func btnAction_Cancel(_ sender: UIButton) {
        view_MoreInfo.isHidden = true
    }
    
    @IBAction func btnActionDateSelected(_ sender: DesignableButton) {
        
        let pastVisitorsCalendarPopUpVC = PastVisitorsCalendarPopUpVC(nibName: "PastVisitorsCalendarPopUpVC", bundle: nil)
        pastVisitorsCalendarPopUpVC.delegate = self
        pastVisitorsCalendarPopUpVC.selectedButtonIndex = sender.tag
        pastVisitorsCalendarPopUpVC.selectedStartDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())!
        pastVisitorsCalendarPopUpVC.selectedEndDate = Date()
        pastVisitorsCalendarPopUpVC.modalPresentationStyle = .overCurrentContext
        pastVisitorsCalendarPopUpVC.modalTransitionStyle = .crossDissolve
        // Present View "Modally"
        self.present(pastVisitorsCalendarPopUpVC, animated: true, completion: nil)
        
    }
    
    @IBAction func addVisitorButtonClicked(_ sender: DesignableButton) {
        
        let contactOtherPopUpVC = ContactOtherPopUpVC(nibName: "ContactOtherPopUpVC", bundle: nil)
        contactOtherPopUpVC.delegate = self
        contactOtherPopUpVC.modalPresentationStyle = .overCurrentContext
        contactOtherPopUpVC.modalTransitionStyle = .crossDissolve
        // Present View "Modally"
        self.present(contactOtherPopUpVC, animated: true, completion: nil)
    }
    
    
    func segementControlChanged(currentPage:Int){
        segmentControl.selectedSegmentIndex = currentPage
        changeIndex = currentPage
        
        (currentIndex == 2) ? hideShowCalendarView(shouldShowCalendar: false, cnst_Height: 55):hideShowCalendarView(shouldShowCalendar: true, cnst_Height: 0)
        
        (currentIndex == 1) ? hideShowAddVisitorView(shouldShowCalendar: false, cnst_Height: 55):hideShowAddVisitorView(shouldShowCalendar: true, cnst_Height: 0)
        
        for index in 0..<3{
            if(index == currentIndex){
                labelArray[index].backgroundColor = UIColor(red: 255.00/255.0, green: 94.00/255.0, blue: 98.00/255.0, alpha: 1.00)
            }else{
                labelArray[index].backgroundColor = UIColor.clear
            }
        }
        switch segmentControl.selectedSegmentIndex {
        case 0:
            guard let startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date()) else { return }
            startDateString = self.formatDateAsDay(date: startDate)
            endDateString = self.formatDateAsDay(date: Date())
            getLiveVisters(startDate: startDateString, endDate: endDateString)
            break
        case 1:
            getExpectedVistors()
            break
        case 2:
            setUpInitialDates()
            getLiveVisters(startDate: startDateString, endDate: endDateString)
            break
        default:
            break
        }
        
    }
    
    private func setUpInitialDates(){
        selectedEndDate = formatDate(date: Date())
        guard let selStartDate = Calendar.current.date(byAdding: .day, value: -7, to: Date()) else { return }
        selectedStartDate = formatDate(date: selStartDate)
        //For buttons to display in day format
        startDateString = formatDateAsDay(date: selStartDate)
        endDateString = formatDateAsDay(date: Date())
        self.btnFrom.setTitle("From : \(startDateString)", for: .normal)
        self.btnTo.setTitle("To : \(endDateString)", for: .normal)
    }
    
    func reloadTableView(num:Int){
        DispatchQueue.main.async {
            if num == 0{
                self.currentIndex = 0
                guard let startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date()) else { return }
                self.startDateString = self.formatDateAsDay(date: startDate)
                self.endDateString = self.formatDateAsDay(date: Date())
                self.getLiveVisters(startDate: self.startDateString, endDate: self.endDateString)
            }else if num == 1{
                self.currentIndex = 1
            }else{
                self.currentIndex = 2
                self.setUpInitialDates()
                self.getLiveVisters(startDate: self.startDateString, endDate: self.endDateString)
            }
            self.tableView.reloadData()
        }
    }
    
    private func hideShowCalendarView(shouldShowCalendar:Bool,cnst_Height:CGFloat){
        self.cnst_CalendarViewHeight.constant = self.currentIndex == 2 ? 55 : 0
        calendarView.isHidden = self.currentIndex == 2 ? false : true
        self.view.layoutIfNeeded()
    }
    
    private func hideShowAddVisitorView(shouldShowCalendar:Bool,cnst_Height:CGFloat){
        self.cnst_AddVisitorViewHeight.constant = self.currentIndex == 1 ? 55 : 0
        addVisitorsView.isHidden = self.currentIndex == 1 ? false : true
        self.view.layoutIfNeeded()
    }
    
    //MARK : API CALLING
    func getLiveVisters(startDate: String, endDate:String) {
        
        showLoadingIndictor()
        liveVisitorsArray = []
        DataManager.sharedInstance().method = "live_visitor"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().LiveVistorsRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(),startDate:startDateString ,endDate:endDateString, apiName: liveVisitors,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            
            switch result {
            case .success(let data):
                self.visitorHelper = VistorHelperClass.init(allVisitor: data)
                switch self.currentIndex {
                case 0:
                    self.currentIndex = 0
                case 2:
                    self.currentIndex = 2
                default:
                    break
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
    func getExpectedVistors() {
        showLoadingIndictor()
        DataManager.sharedInstance().method = "expected_visitor"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().expectVistorsRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), apiName: expectedVisitors,closure: { (result) in
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.expectedVistorsArray = data;
                
                if self.expectedVistorsArray.count == 0 {
                    self.lblNoVisitor.isHidden = false
                    self.lblNoVisitor.text = "No expected visitors!"
                } else {
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            case .failure(let error):
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    func Accept(btnTag:Int,liveOrPast:Int){
        let employeName = UserDefaults.standard.string(forKey: "nameProfile_key")!
        let liveVisit = (liveOrPast == 0) ? self.liveVisitorsArray[btnTag] : self.pastVisitorsArray[btnTag]
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_allow_deny"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().AllowDenayRequest(visitorName: liveVisit.name, visitiorNo: liveVisit.contactNo, empId: self.getEmployeeID(), officeId: self.getOfficeID(), statusV: "1", employeeName: employeName, receptionId: liveVisit.receptionid, visitorId: liveVisit.visitorId,reason: liveVisit.purposeLive, userImage: liveVisit.profileImage, apiName: visitorAllowDeny, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                if errorCode == 0{
                    if liveOrPast == 0{
                        self.reloadTableView(num: 0)
                    }else{
                        self.reloadTableView(num: 2)
                    }
                    self.presentWindow!.makeToast(message: "You have Allowed Visitor")
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    func Deny(btnTag:Int, liveOrPast: Int){
        let employeName = UserDefaults.standard.string(forKey: "nameProfile_key")!
        let liveVisit = (liveOrPast == 0) ? self.liveVisitorsArray[btnTag] : self.pastVisitorsArray[btnTag]
        let visitorId = liveVisit.visitorId
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_allow_deny"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().AllowDenayRequest(visitorName: liveVisit.name, visitiorNo: liveVisit.contactNo, empId: self.getEmployeeID(), officeId: self.getOfficeID(), statusV: "2", employeeName: employeName, receptionId:liveVisit.receptionid , visitorId: visitorId,reason: liveVisit.purposeLive, userImage: liveVisit.profileImage, apiName: visitorAllowDeny, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                print(theDictionary)
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                
                if errorCode == 0{
                    if liveOrPast == 0{
                        self.reloadTableView(num: 0)
                    }else{
                        self.reloadTableView(num: 2)
                    }
                    self.presentWindow!.makeToast(message: "You have Denied Visitor")
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    func checkOutMethod(btnTag:Int, liveOrPast: Int)  {
        let liveVisit = (liveOrPast == 0) ? self.liveVisitorsArray[btnTag] : self.pastVisitorsArray[btnTag]
        let visitorIdContact = liveVisit.visitorId
        let dateFormatter : DateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "checkout_visitor"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().CheckOutRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(),visitorId: visitorIdContact, currentDate: dateString ,apiName: visitor_checkout,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                let errorCode = theDictionary["error_code"].string
                let messageAlert = theDictionary["checkout_details"].string
                
                if errorCode == "0"{
                    if liveOrPast == 0{
                        self.reloadTableView(num: 0)
                    }else{
                        self.reloadTableView(num: 2)
                    }
                    self.presentWindow!.makeToast(message: "Checkout Done")
                }else{
                    self.presentWindow!.makeToast(message: messageAlert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
    func addImpContact(btnTag:Int){
        if(currentIndex == 0){
            visitor = liveVisitorsArray[btnTag]
        }else{
            visitor = pastVisitorsArray[btnTag]
        }
        if let visitor = visitor{
            let visitorIdContact = visitor.visitorId
            let checkId = visitor.checkId
            let isImp = visitor.isImpContact
            
            if(isImp != "1"){
                showLoadingIndictor()
                DataManager.sharedInstance().method = "imp_contact"
                DataManager.sharedInstance().AddImpRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(),visitorId: visitorIdContact, starFlag: "1", checkID: checkId ,apiName: add_imp_contact,closure: { (result) in
                    print(result)
                    self.hideLoadingIndictor()
                    switch result {
                    case .success(let data):
                        let theDictionary = data[0]
                        print(theDictionary)
                        let errorCode = theDictionary["error_code"].string
                        let messageAlert = theDictionary["message"].string
                        
                        if errorCode == "0"{
                            self.presentWindow!.makeToast(message: messageAlert!)
                            DispatchQueue.main.async {
                                self.getLiveVisters(startDate: self.startDateString, endDate: self.endDateString)
                                self.tableView.reloadData()
                            }
                        }else{
                            self.presentWindow!.makeToast(message: messageAlert!)
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                    }
                })
            }else{
                self.presentWindow!.makeToast(message: "Visitor already added!")
            }
            
        }
        
    }
    
    func displayMoreInfo(btnTag:Int){
        if(currentIndex == 0){
            visitor = liveVisitorsArray[btnTag]
        }else{
            visitor = pastVisitorsArray[btnTag]
        }
        if let visitor = visitor{
            view_MoreInfo.isHidden = false
            view_MoreInfo.layer.borderWidth = 1.0
            view_MoreInfo.layer.borderColor = UIColor(red:241/255, green:100/255, blue:99/255, alpha: 1).cgColor
            lbl_CompanyName.text = ":  \(visitor.deliveryCompanyName)"
            lbl_Purpose.text = ":  \(visitor.purposeLive)"
            lbl_CheckIn.text = ":  \(visitor.checkIn)"
            if(visitor.checkOut == ""){
                lbl_CheckOut.text = ":  0000-00-00 00:00:00"
            }else{
                lbl_CheckOut.text = ":  \(visitor.checkOut)"
            }
            
        }
    }
    
}

extension VisitorListVC : PastVisitorsCalendarPopUpVCDelegate{
    
    func didSelectDate(date: Date, selectedButtonIndex: Int) {
        print(date)
        if(selectedButtonIndex == 1){
            startDateString = formatDateAsDay(date: date)
            btnFrom.setTitle("From : \(startDateString)", for: .normal)
        }else if(selectedButtonIndex == 2){
            endDateString = formatDateAsDay(date: date)
            btnTo.setTitle("To : \(endDateString)", for: .normal)
        }
        getLiveVisters(startDate: startDateString, endDate: endDateString)
    }
    
}

extension VisitorListVC: OtherVCDelegate{
    func didAddExpectedVisitor(){
        getExpectedVistors()
    }
}

extension VisitorListVC: PhoneContactsVCDelegate{
    func didAddMultipleExpectedVisitor() {
        didAddEpectedVisitor = true
        getExpectedVistors()
    }
}


extension VisitorListVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(currentIndex)
        switch currentIndex {
        case 0:
            return liveVisitorsArray.count
        case 1:
            return expectedVistorsArray.count
        case 2:
            return pastVisitorsArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch currentIndex {
        case 0:
            
            let cell:LiveVisitorTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "LiveVisitorTableViewCell", for: indexPath) as! LiveVisitorTableViewCell
            cell.delegate = self
            let liveVisitor: LiveVisitors = liveVisitorsArray[indexPath.row]
            cell.lbl_VIsitorPhoneNo.textColor = (liveVisitor.isImpContact == "1")  ?   UIColor.green : UIColor.red
            cell.btn_Imp.tag = indexPath.row
            cell.btn_MoreInfo.tag = indexPath.row
            cell.btnAccept.tag = indexPath.row
            cell.btnDeny.tag = indexPath.row
            cell.btnCheckout.tag = indexPath.row
            cell.configureCell(liveVisitor: liveVisitor)
            return cell
            
        case 1:
            
            let cell:ExpectedVisitorTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ExpectedVisitorTableViewCell", for: indexPath) as! ExpectedVisitorTableViewCell
            let expectedVisitor: ExpectedVisitor = expectedVistorsArray[indexPath.row]
            print(expectedVisitor.name)
            cell.configureCell(expectedVistor: expectedVisitor)
            
            return cell
            
        case 2:
            let cell:PastVisitorTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "PastVisitorTableViewCell", for: indexPath) as! PastVisitorTableViewCell
            cell.delegate = self
            let pastVisitor: LiveVisitors = pastVisitorsArray[indexPath.row]
            cell.lbl_VIsitorPhoneNo.textColor = (pastVisitor.isImpContact == "1")  ?   UIColor.green : UIColor.red
            cell.btn_Imp.tag = indexPath.row
            cell.btn_MoreInfo.tag = indexPath.row
            cell.btnAccept.tag = indexPath.row
            cell.btnDeny.tag = indexPath.row
            cell.btnCheckout.tag = indexPath.row
            cell.configureCell(liveVisitor: pastVisitor)
            return cell
            
        default:
            break
        }
        let cell:LiveVisitorTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "LiveVisitorTableViewCell", for: indexPath) as! LiveVisitorTableViewCell
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (currentIndex) {
        case 0:
            return (liveVisitorsArray.count != 0 ? UITableViewAutomaticDimension : 0)
        case 1:
            return (expectedVistorsArray.count != 0 ? 100 : 0)
        case 2:
            return (pastVisitorsArray.count != 0 ? UITableViewAutomaticDimension : 0)
        default:
            print("Unable to load cell")
        }
        return 0
    }
}

extension VisitorListVC : LiveVisitorTableViewCellDelegate,PastVisitorTableViewCellDelegate{
    func didClickBtnAcceptLive(tag: Int) {
        Accept(btnTag: tag, liveOrPast: 0)
    }
    
    func didClickBtnDenyLive(tag: Int) {
        Deny(btnTag: tag, liveOrPast: 0)
    }
    
    func didClickBtnCheckoutLive(tag: Int) {
        checkOutMethod(btnTag: tag, liveOrPast: 0)
    }
    
    
    func didClickBtnImpLive(tag: Int) {
        addImpContact(btnTag: tag)
    }
    
    func didClickBtnMoreInfoLive(tag: Int) {
        displayMoreInfo(btnTag: tag)
    }
    
    func didClickBtnAcceptPast(tag: Int) {
        Accept(btnTag: tag, liveOrPast: 1)
    }
    
    func didClickBtnDenyPast(tag: Int) {
        Deny(btnTag: tag, liveOrPast: 1)
    }
    
    func didClickBtnCheckoutPast(tag: Int) {
        checkOutMethod(btnTag: tag, liveOrPast: 1)
    }
    
    func didClickBtnImpPast(tag: Int) {
        addImpContact(btnTag: tag)
    }
    
    func didClickBtnMoreInfoPast(tag: Int) {
        displayMoreInfo(btnTag: tag)
    }
    
}


//extension VisitorListVC : LiveVisitorTableViewCellDelegate,PastTableViewCellDelegate
//{
//    func didSelectButton() {
//        self.loadLiveVisitorsData()
//    }
//
//    func didButtonClickFromPastVisitors() {
//        self.loadLiveVisitorsData()
//    }
//
//}

extension VisitorListVC: ContactOtherPopUpVCDelegate{
    
    func didSelectFromContacts() {
        self.dismiss(animated: true, completion: nil)
        requestAccess { (access) in
            if(access){
                DispatchQueue.main.async {
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let phoneContactsVC = mainStoryboard.instantiateViewController(withIdentifier: "PhoneContactsVC") as! PhoneContactsVC
                    phoneContactsVC.delagate = self
                    self.app.navController.pushViewController(phoneContactsVC, animated: true)
                }
            }
        }
    }
    
    func didSelectOther() {
        self.dismiss(animated: true, completion: nil)
        let otherVC = OtherVC(nibName: "OtherVC", bundle: nil)
        otherVC.delegate = self
        otherVC.modalPresentationStyle = .overCurrentContext
        otherVC.modalTransitionStyle = .crossDissolve
        // Present View "Modally"
        self.present(otherVC, animated: true, completion: nil)
    }
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            let store = CNContactStore()
            store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                }
                else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplicationOpenSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                completionHandler(false)
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
}

    //MARK : FooterViewDelegates
extension VisitorListVC : FooterViewDelegate{
    
    func didSelectHome() {
        jumpToVC(viewController: HomeVC(), isFromSB: true)
    }
    
    func didSelectSettings() {
        jumpToVC(viewController: SettingsVC(), isFromSB: true)
    }
    
}
