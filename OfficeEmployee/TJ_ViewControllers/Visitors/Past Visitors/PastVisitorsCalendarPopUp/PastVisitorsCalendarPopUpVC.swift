//
//  PastVisitorsCalendarPopUpVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 09/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit
import FSCalendar

protocol PastVisitorsCalendarPopUpVCDelegate {
  func didSelectDate(date:Date,selectedButtonIndex:Int)
}

class PastVisitorsCalendarPopUpVC : UIViewController {
  
  @IBOutlet weak var fsCalendar: FSCalendar!
  
  var delegate:PastVisitorsCalendarPopUpVCDelegate?
  var selectedDate:Date = Date()
  //Vars to Fetch Past Visitors
  var selectedStartDate:Date = Date()
  var selectedEndDate:Date = Date()
  var selectedButtonIndex : Int = -2
  
  override func viewDidLoad() {
    super.viewDidLoad()
    fsCalendar.delegate = self
    fsCalendar.dataSource = self
  
    if(selectedButtonIndex == 1){
      fsCalendar.select(selectedStartDate)
    }else if(selectedButtonIndex == 2){
      fsCalendar.select(selectedEndDate)
    }
  }
  
  
  @IBAction func btnActionOkay(_ sender: UIButton) {
    self.delegate?.didSelectDate(date:selectedDate,selectedButtonIndex:selectedButtonIndex)
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func btnActionCancel(_ sender: UIButton) {
    self.dismiss(animated: true, completion: nil)
  }
  @IBAction func btnPrevAction(_ sender: Any) {
    
    fsCalendar.setCurrentPage(getPreviousMonth(date: fsCalendar.currentPage), animated: true)
  }
  @IBAction func btnNextAction(_ sender: UIButton) {
    
    fsCalendar.setCurrentPage(getNextMonth(date: fsCalendar.currentPage), animated: true)
  }
  
}

extension PastVisitorsCalendarPopUpVC : FSCalendarDelegate,FSCalendarDataSource{
  
  func maximumDate(for calendar: FSCalendar) -> Date {
    return Date()
  }
  
  func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {

//    selectedDate = Calendar.current.date(byAdding: .day, value: 1, to: date)!
    selectedDate = date
    print("Selected Date : ",selectedDate)
  }
  
  func getNextMonth(date:Date)->Date {
    return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
  }
  
  func getPreviousMonth(date:Date)->Date {
    return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
  }
}
