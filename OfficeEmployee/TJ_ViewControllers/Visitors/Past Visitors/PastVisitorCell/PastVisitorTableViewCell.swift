//
//  PastVisitorTableViewCell.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 09/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//
import UIKit
import SDWebImage

protocol PastVisitorTableViewCellDelegate {
    func didClickBtnImpPast(tag:Int)
    func didClickBtnMoreInfoPast(tag:Int)
    func didClickBtnAcceptPast(tag:Int)
    func didClickBtnDenyPast(tag:Int)
    func didClickBtnCheckoutPast(tag:Int)
}

class PastVisitorTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var lbl_VisitorName: UILabel!
    @IBOutlet weak var lbl_VIsitorPhoneNo: UILabel!
    @IBOutlet weak var img_Status: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var enterImageViewStaus: UIImageView!
    @IBOutlet weak var logoutImageViewStatus: UIImageView!
    @IBOutlet weak var lbl_EnterTime: UILabel!
    @IBOutlet weak var lbl_ExitTime: UILabel!
    @IBOutlet weak var permissionView: UIView!
    @IBOutlet weak var exitView: UIView!
    @IBOutlet weak var btn_Imp: UIButton!
    @IBOutlet weak var btn_MoreInfo: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var btnCheckout: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnDeny: UIButton!
    
    let imageURL = "http://knocknok.co/officeupload/"
    var delegate: PastVisitorTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2
            self.profileImageView.clipsToBounds = true
        }
    }
    
    func HideView(permission:Bool,status:Bool,Exit:Bool){
        statusView.isHidden = status
        permissionView.isHidden = permission
        exitView.isHidden = Exit
        lbl_ExitTime.isHidden = true
        logoutImageViewStatus.isHidden = true
    }
    
    func configureCell(liveVisitor: LiveVisitors) {
        
        let imageName = liveVisitor.profileImage
        let profileImg = imageURL + imageName
        print(profileImg)
        profileImageView.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "girl"))
        
        lbl_VisitorName.text = liveVisitor.name
        lbl_VIsitorPhoneNo.textColor = (liveVisitor.isImpContact == "1")  ?   UIColor.green : UIColor.red
        lbl_VIsitorPhoneNo.text = "\(liveVisitor.contactNo) | \(liveVisitor.purposeLive)"
        
        if liveVisitor.isImpContact == "0" {
            btn_Imp.setImage(UIImage(named: "star_unselected"), for: UIControlState.normal)
        }else{
            btn_Imp.setImage(UIImage(named: "star_selected"), for: UIControlState.normal)
        }
        
//        print(liveVisitor.isAllow,liveVisitor.checkIn,liveVisitor.checkOut,liveVisitor.checkId,liveVisitor.deliveryCompanyName,liveVisitor.isExpected,liveVisitor.isImpContact,liveVisitor.receptionid)
        print(liveVisitor.isAllow,liveVisitor.checkOut)
        if liveVisitor.isAllow == "0"{
            HideView(permission: false, status: true, Exit: true)
            img_Status.image = UIImage(named: "pending")
        }else if liveVisitor.isAllow == "1" && liveVisitor.checkOut == "0000-00-00 00:00:00"{
            HideView(permission: true, status: false, Exit: false)
            img_Status.image = UIImage(named: "allow")
        }else if liveVisitor.isAllow == "2" {
            HideView(permission: true, status: false, Exit: true)
            img_Status.image = UIImage(named: "block")
        }else if liveVisitor.isAllow == "1" && liveVisitor.checkOut != "0000-00-00 00:00:00" {
            HideView(permission: true, status: false, Exit: true)
            img_Status.image = UIImage(named: "allow")
            lbl_ExitTime.isHidden = false
            lbl_ExitTime.text = liveVisitor.checkOut
            logoutImageViewStatus.isHidden = false
        }
        
    }
    
    @IBAction func btnActionAccept(_ sender: UIButton) {
        self.delegate?.didClickBtnAcceptPast(tag: sender.tag)
    }
    
    @IBAction func btnActionDeny(_ sender: UIButton) {
        self.delegate?.didClickBtnDenyPast(tag: sender.tag)
    }
    
    @IBAction func btnActionCheckout(_ sender: UIButton) {
        self.delegate?.didClickBtnCheckoutPast(tag: sender.tag)
    }
    
    
    @IBAction func btnAction_AddImp(_ sender: UIButton) {
        self.delegate?.didClickBtnImpPast(tag: sender.tag)
    }
    
    @IBAction func btnAction_MoreInfo(_ sender: UIButton) {
        self.delegate?.didClickBtnMoreInfoPast(tag: sender.tag)
    }
}
