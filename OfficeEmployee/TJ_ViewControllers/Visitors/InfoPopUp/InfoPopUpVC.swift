//
//  InfoPopUpVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 07/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

class InfoPopUpVC: UIViewController {

  @IBOutlet var backgroundView: UIView!
  
  override func viewDidLoad() {
        super.viewDidLoad()

    let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
    self.view.addGestureRecognizer(tap)
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
      self.dismiss(animated: true, completion: nil)
    }

}
