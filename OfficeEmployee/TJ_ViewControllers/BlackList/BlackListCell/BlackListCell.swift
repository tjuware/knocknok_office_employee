//
//  BlackListCell.swift
//  OfficeEmployee
//
//  Created by Kush Thakkar on 18/09/20.
//  Copyright © 2020 Knocknok. All rights reserved.
//

import UIKit

protocol BlackListCellDelegate{
    func didRemoveFromBlockList(btnTag:Int)
}

class BlackListCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobNo: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    var expVisitor = ExpectedVisitor() {
        didSet{
            configureCell()
        }
    }
    var delegate : BlackListCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(){
        self.lblName.text = expVisitor.name
        self.lblMobNo.text = "Mobile No : \(expVisitor.contactNo)"
    }

    @IBAction func btnActionDelete(_ sender: UIButton) {
        self.delegate?.didRemoveFromBlockList(btnTag:sender.tag)
    }
}
