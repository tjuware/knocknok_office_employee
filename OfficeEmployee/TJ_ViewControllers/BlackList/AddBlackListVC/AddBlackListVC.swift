//
//  AddBlackListVC.swift
//  OfficeEmployee
//
//  Created by Kush Thakkar on 18/09/20.
//  Copyright © 2020 Knocknok. All rights reserved.
//

import UIKit

protocol AddBlackListVCDelegate {
    func didSubmit()
}

class AddBlackListVC: MasterVC,UITextFieldDelegate {
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldMobNo: UITextField!
    
    var delegate : AddBlackListVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFieldMobNo.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.bgView.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnActionSubmit(_ sender: UIButton) {
        if txtFieldName.isEmpty() {
            showValidationAlert(title: "", message: "Please,Enter your Name")
        } else if txtFieldMobNo.isEmpty() {
            showValidationAlert(title: "", message: "Please,Enter your Mobile Number")
        }else {
            AddBlackListApi()
        }
    }
    
    func AddBlackListApi() {
        self.showLoadingIndictor()
        DataManager.sharedInstance().method = "add_black_list"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().addBlackRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), visitorName: self.txtFieldName.text!, visitorNo: self.txtFieldMobNo.text!, apiName: addBlackList, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                print(theDictionary)
                let errorCode = theDictionary["error_code"].string
                let messageAert = theDictionary["message"].string
                if errorCode == "0"{
                    self.txtFieldName.text = ""
                    self.txtFieldMobNo.text = ""
                    self.presentWindow!.makeToast(message: messageAert!)
                    self.dismiss(animated: true, completion: nil)
                    self.delegate?.didSubmit()
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFieldMobNo{
            guard let textFieldText = txtFieldMobNo!.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            if count > 10{
                DispatchQueue.main.async {
                    self.presentWindow!.makeToast(message: "Mobile Number should be 10 digit")
                }
            }
            return count <= 10
        }
        return true
    }
}

