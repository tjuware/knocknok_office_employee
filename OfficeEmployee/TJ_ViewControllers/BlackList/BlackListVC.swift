//
//  BlackListVC.swift
//  OfficeEmployee
//
//  Created by Kush Thakkar on 18/09/20.
//  Copyright © 2020 Knocknok. All rights reserved.
//

import UIKit

class BlackListVC: MasterVC {

    @IBOutlet weak var footer_View: FooterView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    
    var blackListArray = [ExpectedVisitor]()
    var searchBlackListArray = [ExpectedVisitor]()
    var label = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.center = self.tableView.center
        label.textAlignment = .center
        label.text = "No Data!"
        tableView.addSubview(label)
        label.isHidden = false
        
        searchBar.delegate = self
        setTopTitle(title: "Block List")
        footer_View.delegate = self
        self.topView.btn_Info.isHidden = true
        registerTableView()
        DispatchQueue.main.async {
            self.btnAdd.layer.cornerRadius = self.btnAdd.frame.height/2
            self.btnAdd.clipsToBounds = true
        }
        WatchList()
    }
    
    func registerTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 90
        tableView.register(UINib(nibName: "BlackListCell", bundle: nil), forCellReuseIdentifier: "BlackListCell")
    }
    
    @IBAction func btnActionAdd(_ sender: UIButton) {
        let addBlackListVC = AddBlackListVC(nibName: "AddBlackListVC", bundle: nil)
        addBlackListVC.delegate = self
        addBlackListVC.modalPresentationStyle = .overCurrentContext
        addBlackListVC.modalTransitionStyle = .crossDissolve
        // Present View "Modally"
        self.present(addBlackListVC, animated: true, completion: nil)
    }
    
    func WatchList() {
        showLoadingIndictor()
        DataManager.sharedInstance().method = "spamlist"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().WatchListRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), apiName: watchList,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.blackListArray = data;
                self.searchBlackListArray = self.blackListArray
                self.label.isHidden = (self.blackListArray.count == 0) ? false : true
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
    
    func deleteFromBlockList(btnTag:Int)  {
        let expectVisit = blackListArray[btnTag]
        let visitorId = expectVisit.visitorId
        let visitorNumber = expectVisit.contactNo
        showLoadingIndictor()
        DataManager.sharedInstance().method = "delete_block_visitor"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().deleteBlackRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(),visitoriD: visitorId,visitorNo: visitorNumber, apiName: deleteBlackList,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"].string
                let messageAert = theDictionary["message"].string
                
                if errorCode == "0"{
                    DispatchQueue.main.async {
                        self.WatchList()
                    }
                   self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
}

extension BlackListVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchBlackListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlackListCell", for: indexPath) as! BlackListCell
        cell.btnDelete.tag = indexPath.row
        cell.delegate = self
        cell.expVisitor = searchBlackListArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension BlackListVC : BlackListCellDelegate{
    func didRemoveFromBlockList(btnTag: Int) {
        deleteFromBlockList(btnTag: btnTag)
    }
}

extension BlackListVC : AddBlackListVCDelegate{
    func didSubmit() {
        WatchList()
    }
}

extension BlackListVC : UISearchBarDelegate{
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    
  {
    searchBar.text = ""
    
    searchBlackListArray = blackListArray
    
    searchBar.endEditing(true)
    DispatchQueue.main.async {
      self.tableView.reloadData()
    }
    
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    
  {
    
    searchBlackListArray = searchText.isEmpty ? blackListArray : blackListArray.filter{
      (expVis: ExpectedVisitor) -> Bool in
        
        // If dataItem matches the searchText, return true to include
        return expVis.name.range(of: searchText, options: .caseInsensitive, range: nil,locale: nil) != nil
    }
    DispatchQueue.main.async {
      self.tableView.reloadData()
    }
  }
}

extension BlackListVC : FooterViewDelegate{
    
    //MARK : FooterViewDelegates
    
    func didSelectHome() {
        jumpToVC(viewController: HomeVC(), isFromSB: true)
    }
    
    func didSelectSettings() {
        jumpToVC(viewController: SettingsVC(), isFromSB: true)
    }
    
}
