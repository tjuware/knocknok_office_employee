//
//  Utils.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 30/04/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit

class Utils {


  internal class func zoomImageView(image: UIImage?, viewCtrlParent: UIViewController?) {
    guard let image = image, let viewCtrlParent = viewCtrlParent else {
      return
    }
    
    let zoomingView = UIView()
    zoomingView.frame = viewCtrlParent.view.frame
    zoomingView.backgroundColor = UIColor.clear
    
    let zoomImageView = UIImageView(image: image)
    let frame = zoomingView.frame
    zoomImageView.frame = CGRect(x: (frame.width*0.5)-((frame.width*0.8)/2), y: (frame.height*0.5)-((frame.width*0.8)/2), width: frame.width*0.8, height: frame.width*0.8)
    zoomImageView.layer.cornerRadius = (zoomImageView.bounds.size.width)/2
    zoomImageView.layer.borderWidth = 2
    zoomImageView.layer.borderColor = UIColor.white.cgColor
    zoomImageView.layer.masksToBounds = false
    zoomImageView.clipsToBounds = true
    zoomImageView.backgroundColor = .black
    zoomImageView.contentMode = .scaleAspectFill
    
    zoomingView.addSubview(zoomImageView)
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(Utils.dismissFullscreenImage(_:)))
    zoomingView.addGestureRecognizer(tap)
    
    UIView.animate(withDuration: 0.5, animations: {
      zoomingView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
    }) { (finished) in
      UIView.animate(withDuration:0.5, animations: {
        zoomingView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        viewCtrlParent.view.addSubview(zoomingView)
      })
    }
  }
  
  // Use to back from full mode
  @objc internal class func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
    sender.view?.removeFromSuperview()
  }
}

