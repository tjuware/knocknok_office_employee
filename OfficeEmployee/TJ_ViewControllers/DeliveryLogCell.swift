//
//  DeliveryLogCell.swift
//  OfficeEmployee
//
//  Created by Kush Thakkar on 18/09/20.
//  Copyright © 2020 Knocknok. All rights reserved.
//

import UIKit

class DeliveryLogCell: UITableViewCell {
    
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobNo: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblCheckIn: UILabel!
    
    let imageURL = "http://knocknok.co/officeupload/"
    
    var visitor = LiveVisitors() {
        didSet{
            configureCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.imgViewProfile.layer.cornerRadius = self.imgViewProfile.frame.height/2
            self.imgViewProfile.clipsToBounds = true
            self.imgViewProfile.layer.borderWidth = 1
            self.imgViewProfile.layer.borderColor = UIColor(red:241/255, green:100/255, blue:99/255, alpha: 1).cgColor
        }
    }
    
    func configureCell(){
        self.lblName.text = visitor.name
        self.lblMobNo.text = "Mobile No : \(visitor.contactNo)"
        self.lblCheckIn.text = "Check In : \(visitor.checkIn)"
        self.lblCompany.text = "Company : \(visitor.deliveryCompanyName)"
        let image = visitor.profileImage
        let imageProfile = imageURL + image
        self.imgViewProfile.sd_setImage(with: URL(string: imageProfile), placeholderImage: UIImage(named: "user"))
    }
    
}
