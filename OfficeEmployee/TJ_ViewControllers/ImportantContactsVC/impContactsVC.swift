//
//  impContactsVC.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 05/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit
import SDWebImage

class impContactsVC: MasterVC {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var footer_View: FooterView!
    
    var impVisitorArray = [LiveVisitors]()
    var label = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label.center = self.tableView.center
        label.textAlignment = .center
        label.text = ""
        tableView.addSubview(label)
        label.isHidden = false
        
        registerTableView()
        footerView.delegate = self
        self.topView.delegate = self
        setTopTitle(title: "IMPORTANT CONTACTS")
        ImpContactList()
    }
    
    func registerTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 90
        tableView.register(UINib(nibName: "impoContactsTableViewCell", bundle: nil), forCellReuseIdentifier: "impoContactsTableViewCell")
    }
    
    func ImpContactList() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "imp_visitor"
        DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
        DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().ImpContactRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(), apiName: imp_contact_list,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.impVisitorArray = data
                if self.impVisitorArray.count == 0 {
                    self.label.isHidden = false
                } else {
                    self.label.isHidden = true
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
    
    func RemoveImpContact() {
            
            showLoadingIndictor()
            DataManager.sharedInstance().method = "imp_visitor"
            DataManager.sharedInstance().username = UserDefaults.standard.object(forKey: "userName_key") as! String
            DataManager.sharedInstance().password = UserDefaults.standard.object(forKey: "password_key") as! String
            DataManager.sharedInstance().deviceToken = getDeviceToken()
            DataManager.sharedInstance().ImpContactRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(), apiName: imp_contact_list,closure: { (result) in
                print(result)
                self.hideLoadingIndictor()
                switch result {
                case .success(let data):
                    print(data)
                    self.impVisitorArray = data
                    if self.impVisitorArray.count == 0 {
                        self.label.isHidden = false
                    } else {
                        self.label.isHidden = true
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

                }
            })
        }
}



extension impContactsVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return impVisitorArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "impoContactsTableViewCell", for: indexPath) as! impoContactsTableViewCell
        cell.btnRemoveContact.tag = indexPath.row
        cell.delegate = self
        cell.impContact = self.impVisitorArray[indexPath.row]
        return cell
    }
}

extension impContactsVC : impoContactsTableViewCellDelegate{
    func didRemoveFromImpContacts(btnTag:Int) {
        print("Removed")
    }
}

extension impContactsVC : FooterViewDelegate{
    func didSelectHome() {
        jumpToVC(viewController: HomeVC(), isFromSB: true)
    }
    
    func didSelectSettings() {
        jumpToVC(viewController: SettingsVC(), isFromSB: true)
    }
}
