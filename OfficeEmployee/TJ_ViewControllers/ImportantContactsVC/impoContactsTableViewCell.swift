//
//  impoContactsTableViewCell.swift
//  DigitalGorkha
//
//  Created by Kush Thakkar on 05/05/20.
//  Copyright © 2020 Ashok Londhe. All rights reserved.
//

import UIKit
import SDWebImage

protocol impoContactsTableViewCellDelegate{
    func didRemoveFromImpContacts(btnTag:Int)
}

class impoContactsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageView_Profile: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    
    @IBOutlet weak var btnRemoveContact: UIButton!
    var delegate : impoContactsTableViewCellDelegate?
    
//    let imageURL = "http://www.digitalgorkha.com/officeupload/"
    let imageURL = "http://knocknok.co/officeupload/"
    
    var impContact = LiveVisitors() {
        didSet{
            configureCell()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.imageView_Profile.layer.cornerRadius = self.imageView_Profile.frame.height/2
            self.imageView_Profile.clipsToBounds = true
            self.imageView_Profile.layer.borderWidth = 1
//            self.imageView_Profile.layer.borderColor = UIColor.white.cgColor
            self.imageView_Profile.layer.borderColor = UIColor(red:241/255, green:100/255, blue:99/255, alpha: 1).cgColor
        }
    }

    
    func configureCell(){
        self.lbl_Name.text = impContact.name
        self.lbl_Phone.text = "Mobile No : \(impContact.contactNo)"
        let image = impContact.profileImage
        let imageProfile = imageURL + image
        self.imageView_Profile.sd_setImage(with: URL(string: imageProfile), placeholderImage: UIImage(named: "user"))

    }
    
    @IBAction func btnActionRemoveImpContact(_ sender: UIButton) {
        self.delegate?.didRemoveFromImpContacts(btnTag:sender.tag)
    }
    
}
