//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setBackButtonForNavigation(){
        
        let backButton = UIButton(frame: CGRect(x: 0,y: -5,width: 30,height: 30))
        backButton.setBackgroundImage(UIImage(named: "back"), for: UIControlState())
        let barbackButtonitem = UIBarButtonItem(customView: backButton)
        backButton.isExclusiveTouch = true
        backButton.addTarget(self, action: #selector(UIViewController.popCurrentViewController), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = barbackButtonitem
        
        self.navigationItem.hidesBackButton = true
    }
    
    func setHomeButtonForNavigation(){
        let homeButton = UIButton(frame: CGRect(x: 0,y: -5,width: 30,height: 30))
        homeButton.setBackgroundImage(UIImage(named: "menu"), for: UIControlState())
        let barbackButtonitem = UIBarButtonItem(customView: homeButton)
        homeButton.isExclusiveTouch = true
        homeButton.addTarget(self, action: #selector(UIViewController.popToRootViewController), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = barbackButtonitem
    }

    
    @objc func popCurrentViewController() {
        _ = self.navigationController?.popViewController(animated: true)  
    }
    
    @objc func popToRootViewController() {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }

    //TJ Commented
    
//    func slideMenuShow(_ menuButton:UIBarButtonItem , viewcontroller:UIViewController){
//        if self.revealViewController() != nil {
//            self.view.endEditing(true)
//            menuButton.target = self.revealViewController()
//            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//        }else{
//            self.view.endEditing(true)
//        }
//        self.revealViewController().rearViewRevealWidth = self.view.frame.width - 100
//    }
//
//    func revealTouch(_ controller:UIViewController) {
//        let revealController = controller.revealViewController()
//        let tap = revealController?.tapGestureRecognizer()
//        self.view.addGestureRecognizer(tap!)
//    }
    
}
