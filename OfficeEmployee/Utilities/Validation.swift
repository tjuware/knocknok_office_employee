//
//  Validation.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 16/07/17.
//  Copyright © 2017 Ankit Bhardwaj. All rights reserved.
//

import Foundation
import UIKit


extension UITextField {

    func isValidEmail() -> (Bool) {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self.text)
        return (result)
    }
    
    func isValidatePhoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }

    func isEmpty() -> (Bool) {
        let trimmedstr: String = self.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if trimmedstr.isEmpty {
            return true
        }
        return false
    }
}
