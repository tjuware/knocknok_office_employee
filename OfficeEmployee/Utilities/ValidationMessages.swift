//
//  ValidationMessages.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 03/08/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import Foundation

// URL's
var login = "employee_login.php"
var expectedVisitors = "expected_list.php"
var delivery = "delivery.php"
var liveVisitors = "live_visitor.php"
var studentDetails = "student_detail.php"
var studentMom = "student_mom.php"
var studentRem = "student_rem.php"
var visitorsReminder = "visitor_rem.php"
var student_rem_list = "student_rem_list.php"
var visitor_rem_list = "visitor_rem_list.php"
var student_checkout = "student_checkout.php"
var imp_contact_list = "imp_contact_list.php"
var changePassword = "changepassword.php"
var watchList = "spamlist.php"
var visitor_checkout = "visitor_checkout.php"
var add_imp_contact = "imp_contact.php"
var event_list = "event_list.php"
var event_member_list = "event_member_list.php"
var update_gcm = "update_gcm.php"
var logout = "logout.php"
var visitor_allow_deny = "visitor_allow_deny.php"
var addExpected = "add_expected.php"
var otpRequest = "otp"
var updateFcm = "update_gcm.php"
var visitorAllowDeny = "visitor_allow_deny.php"
var leaveRequest = "add_leave_request.php"
var addBlackList = "add_black_list.php"
var deleteBlackList = "delete_block_visitor.php"


//Validation Messages

// MARK: Login

var errMsgUserNameEmpty = "Please enter email id"
var errMsgUserNameInvalid = "Please enter valid email id"
var errMsgPasswordEmpty = "Please enter a password"
//var errMsgUserNameEmpty = "Please enter email id"
//var errMsgUserNameEmpty = "Please enter email id"
//var errMsgUserNameEmpty = "Please enter email id"

