//
//  ExpectedVisitor.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 17/12/18.
//  Copyright © 2018 Ashok Londhe. All rights reserved.
//

import Foundation

class ExpectedVisitor: NSObject {
    var visitorId = ""
    var name = ""
    var contactNo = ""
    var purpose = ""
    var image = ""
    var checkInDate = ""
    var checkOutDate = ""
    var arrivalDate = ""
    var email = ""
    
}
