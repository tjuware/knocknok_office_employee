//
//  ContactList.swift
//  DigitalGorkha
//
//  Created by Ankit on 7/31/19.
//  Copyright © 2019 Ashok Londhe. All rights reserved.
//

import UIKit

class ContactList {
    var id: Int?
    var givenName: String?
    var familyName: String?
    var phoneNumber: String?
    var isChecked: Bool?
    
    init(id: Int?, givenName: String?, familyName: String?, phoneNumber: String?, isChecked: Bool?) {
        self.id = id
        self.givenName = givenName
        self.familyName = familyName
        self.phoneNumber = phoneNumber
        self.isChecked = isChecked
    }
    
}
