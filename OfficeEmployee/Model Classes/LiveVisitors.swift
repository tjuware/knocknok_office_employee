//
//  Visitors.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 05/08/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import Foundation

class LiveVisitors: NSObject {
    var visitorId = ""
    var name = ""
    var contactNo = ""
    var purposeLive = ""
    var profileImage = ""
    var checkIn = ""
    var checkOut = ""
    var isAllow = ""
    var isImpContact = ""
    var checkId = ""
    var deliveryCompanyName = ""
    var receptionid = ""
    var isExpected = ""

}

class VistorHelperClass {
  
  var PastVisitors : [LiveVisitors]?
  var liveVisitors : [LiveVisitors]?
  
  init(allVisitor :[LiveVisitors]) {
    self.PastVisitors = allVisitor
    self.getLiveVisitors()
  }
  func getLiveVisitors()  {
    let startDate = Date()
    let startFormatter = DateFormatter()
    startFormatter.dateFormat = "yyyy-MM-dd"
    let startDateString = startFormatter.string(from: startDate)
    self.liveVisitors = self.PastVisitors?.filter({ (model) -> Bool in
        let arrs = model.checkIn.split(separator: " ")
        return arrs[0] == startDateString
    })
  }
  
}
