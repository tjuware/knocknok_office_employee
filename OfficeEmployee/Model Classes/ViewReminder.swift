//
//  ViewReminder.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 20/12/18.
//  Copyright © 2018 Ashok Londhe. All rights reserved.
//

import Foundation

class ViewReminder: NSObject {
    var reminderID = ""
    var title = ""
    var reminder = ""
    var date = ""
    var time = ""

}
