//
//  User.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 04/08/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import Foundation


import UIKit

class User: NSObject {
    var id = ""
    var name = ""
    var password = ""
    var profileImage = ""
    var dateOfBirth = ""
    var employeeId = ""
    var contactNo = ""
    var qrImage = ""
    var officId = ""
    var email = ""
    var officeName = ""
    var officeLogo = ""
    var emp_id = ""
    var username = ""
    var is_student = false
    var is_error = ""
    var message = ""
    
}
