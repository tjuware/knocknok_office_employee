//
//  Guest.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 21/12/18.
//  Copyright © 2018 Ashok Londhe. All rights reserved.
//

import Foundation

class Guest: NSObject {
    var guest_id = ""
    var guest_name = ""
    var contact_no = ""
    var event_code = ""
    var whenCheckedIn = ""
    var whenCheckedOut = ""
    /// Notification
    var visitor_id = ""
    var name = ""
    var visitor_no = ""
    

}
