//
//  Event.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 19/12/18.
//  Copyright © 2018 Ashok Londhe. All rights reserved.
//

import Foundation
import UIKit

class Event: NSObject {
    var eventId = ""
    var eventName = ""
    var eventDate = ""
    var eventTime = ""
    var eventDetails = ""
    var eventVenue = ""
    var officeId = ""
    var emloyeeId = ""
    var isactive = true
    
}
