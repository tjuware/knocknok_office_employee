//
//  JSONMapper.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 09/08/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit
import SwiftyJSON

class JSONMapper: NSObject {
    
    class func userMapper(responseJson: JSON) -> User {
        
        let userDetails = User()
        for user in responseJson {
            print(user)
            if let userName = user.1["data"][0]["employee_name"].string {
                userDetails.name = userName
            }
            
            if let emp_id = user.1["data"][0]["emp_id"].string {
                userDetails.emp_id = emp_id
            }
            
            if let employee_id = user.1["data"][0]["employee_id"].string {
                userDetails.employeeId = employee_id
            }
            
            if let email = user.1["data"][0]["email"].string {
                userDetails.email = email
            }
            
            if let imagename = user.1["data"][0]["imagename"].string {
                userDetails.profileImage = imagename
            }
            
            if let qr_image = user.1["data"][0]["qr_image"].string {
                userDetails.qrImage = qr_image
            }
            
            if let unique_code = user.1["data"][0]["unique_code"].string {
                userDetails.id = unique_code
            }
            
            if let contact_no = user.1["data"][0]["contact_no"].string {
                userDetails.contactNo = contact_no
            }
            
            if let username = user.1["data"][0]["username"].string {
                userDetails.username = username
            }
            
            if let password = user.1["data"][0]["password"].string {
                userDetails.password = password
            }
            
            if let logo = user.1["office_data"][0]["logo"].string {
                userDetails.officeLogo = logo
            }
            
            if let office_id = user.1["data"][0]["office_id"].string {
                userDetails.officId = office_id
            }

            if let is_student = user.1["office_data"][0]["is_student"].bool {
                userDetails.is_student = is_student
            }
            if let is_error = user.1["error_code"].string {
                userDetails.is_error = is_error
            }
            if let message = user.1["message"].string {
                userDetails.message = message
            }
    
        }
        return userDetails
    }
    
    class func LiveVisitorsMap(responseJson: JSON) -> [LiveVisitors] {
        
        var visitersList = [LiveVisitors]()
        for visiter in responseJson {
            let liveVisiterDetails = LiveVisitors()
            
            if let visitor_id = visiter.1["visitor_id"].string {
                liveVisiterDetails.visitorId = visitor_id
            }
            
            if let name = visiter.1["name"].string {
                liveVisiterDetails.name = name
            }
            
            if let imagename = visiter.1["imagename"].string {
                liveVisiterDetails.profileImage = imagename
            }
            
            if let arrivaldate = visiter.1["arrivaldate"].string {
                liveVisiterDetails.checkIn = arrivaldate
            }
            
            if let contact_no = visiter.1["contact_no"].string {
                liveVisiterDetails.contactNo = contact_no
            }
            
            if let isAllow = visiter.1["is_allow"].string {
                liveVisiterDetails.isAllow = isAllow
            }
            
            if let checkIn = visiter.1["check_in"].string {
                liveVisiterDetails.checkIn = checkIn
            }
            
            if let checkO = visiter.1["check_out"].string {
                liveVisiterDetails.checkOut = checkO
            }
            
            if let checkId = visiter.1["chk_id"].string {
                liveVisiterDetails.checkId = checkId
            }
            
            if let addImp = visiter.1["isImpContact"].string {
                liveVisiterDetails.isImpContact = addImp
            }
            
            if let receptioid = visiter.1["reception_id"].string {
                liveVisiterDetails.receptionid = receptioid
            }
            
            if let purpose = visiter.1["purpose"].string {
                liveVisiterDetails.purposeLive = purpose
            }
            if let comingFrom = visiter.1["comingfrom"].string {
                liveVisiterDetails.deliveryCompanyName = comingFrom
            }
            if let isExpected = visiter.1["isExpectedVisitor"].string {
                liveVisiterDetails.isExpected = isExpected
            }
            
            visitersList.append(liveVisiterDetails)
            
        }
        return visitersList
    }
    
    class func ExpectedVisitors(responseJson: JSON) -> [ExpectedVisitor] {
        var visitersList = [ExpectedVisitor]()
        for visiter in responseJson {
            let expectedVisitersList = ExpectedVisitor()
            
            if let visitor_id = visiter.1["visitor_id"].string {
                expectedVisitersList.visitorId = visitor_id
            }
            
            if let name = visiter.1["name"].string {
                expectedVisitersList.name = name
            }
            
            if let email = visiter.1["email"].string {
                expectedVisitersList.email = email
            }
            
            if let imagename = visiter.1["imagename"].string {
                expectedVisitersList.image = imagename
            }
            
            if let purpose = visiter.1["purpose"].string {
                expectedVisitersList.purpose = purpose
            }
            
            if let arrivaldate = visiter.1["arrivaldate"].string {
                expectedVisitersList.checkInDate = arrivaldate
            }
            
            if let contact_no = visiter.1["contact_no"].string {
                expectedVisitersList.contactNo = contact_no
            }
            
            visitersList.append(expectedVisitersList)
        }
        return visitersList
    }
    
    class func WatchList(responseJson: JSON) -> [ExpectedVisitor] {
        
        var spamList = [ExpectedVisitor]()
        for spamVisiter in responseJson {
            let spamVisitersList = ExpectedVisitor()
            
            if let visitor_id = spamVisiter.1["visitor_id"].string {
                spamVisitersList.visitorId = visitor_id
            }
            
            if let name = spamVisiter.1["name"].string {
                spamVisitersList.name = name
            }
            
            if let checkIn = spamVisiter.1["whencheckedin"].string {
                spamVisitersList.checkInDate = checkIn
            }
            
            if let checkOut = spamVisiter.1["whencheckedout"].string {
                spamVisitersList.checkOutDate = checkOut
            }
            
            if let contact_no = spamVisiter.1["contact_no"].string {
                spamVisitersList.contactNo = contact_no
            }
            
            if let arrivalDate = spamVisiter.1["arrivaldate"].string {
                spamVisitersList.arrivalDate = arrivalDate
            }
            
            spamList.append(spamVisitersList)
            
        }
        return spamList
    }
    
    class func impContactList(responseJson: JSON) -> [LiveVisitors] {
        
        var impVisitorList = [LiveVisitors]()
        for impVisitor in responseJson {
            let impVisitorsList = LiveVisitors()
            
            if let name = impVisitor.1["name"].string {
                impVisitorsList.name = name
            }
            
            if let contactNos = impVisitor.1["contact_no"].string {
                impVisitorsList.contactNo = contactNos
            }
            
            if let visitorId = impVisitor.1["visitor_id"].string {
                impVisitorsList.visitorId = visitorId
            }
            if let checkId = impVisitor.1["chk_id"].string {
                impVisitorsList.checkId = checkId
            }
            
            if let image = impVisitor.1["imagename"].string {
                impVisitorsList.profileImage = image
            }
            
            impVisitorList.append(impVisitorsList)
            
        }
        return impVisitorList
    }
    
    class func EventList(responseJson: JSON) -> [Event] {
        
        var EventList = [Event]()
        for event in responseJson {
            let eventVisitorsList = Event()
            
            if let eventID = event.1["event_id"].string {
                eventVisitorsList.eventId = eventID
            }
            
            if let eventName = event.1["event_name"].string {
                eventVisitorsList.eventName = eventName
            }
            
            if let eventDate = event.1["event_date"].string {
                eventVisitorsList.eventDate = eventDate
            }
            if let eventTime = event.1["event_time"].string {
                eventVisitorsList.eventTime = eventTime
            }
            if let eventVenu = event.1["event_venue"].string {
                eventVisitorsList.eventVenue = eventVenu
            }
            
            
            EventList.append(eventVisitorsList)
            
        }
        return EventList
    }
    
    class func deliveryList(responseJson: JSON) -> [LiveVisitors] {
        
        var deliveryList = [LiveVisitors]()
        for delivery in responseJson {
            let deliveryVisitorsList = LiveVisitors()
            
            if let name = delivery.1["name"].string {
                deliveryVisitorsList.name = name
            }
            
            if let contactNos = delivery.1["mobile"].string {
                deliveryVisitorsList.contactNo = contactNos
            }
            
            if let image = delivery.1["imagename"].string {
                deliveryVisitorsList.profileImage = image
            }
            
            if let companyName = delivery.1["deliveryCompanyName"].string {
                deliveryVisitorsList.deliveryCompanyName = companyName
            }
            
            if let checkIn = delivery.1["whencheckedin"].string {
                deliveryVisitorsList.checkIn = checkIn
            }
            
            if let checkOut = delivery.1["whencheckedout"].string {
                deliveryVisitorsList.checkOut = checkOut
            }
            
            deliveryList.append(deliveryVisitorsList)
        }
        return deliveryList
    }
    
    class func ReminderList(responseJson: JSON) -> [ViewReminder] {
        
        var reminderList = [ViewReminder]()
        for reminder in responseJson {
            let viewReminderList = ViewReminder()
            
            if let reminderID = reminder.1["rem_id"].string {
                viewReminderList.reminderID = reminderID
            }
            
            if let titleName = reminder.1["title"].string {
                viewReminderList.title = titleName
            }
            
            if let reminderL = reminder.1["reminder"].string {
                viewReminderList.reminder = reminderL
            }
            
            if let reminderDate = reminder.1["date"].string {
                viewReminderList.date = reminderDate
            }
            if let reminderTime = reminder.1["time"].string {
                viewReminderList.time = reminderTime
            }
            reminderList.append(viewReminderList)
            
        }
        return reminderList
    }
    
    class func GuestList(responseJson: JSON) -> [Guest] {
        
        var guestList = [Guest]()
        for guest in responseJson {
            let viewGuestList = Guest()
            
            if let guestID = guest.1["event_code"].string {
                viewGuestList.guest_id = guestID
            }
            
            if let titleName = guest.1["guest_name"].string {
                viewGuestList.guest_name = titleName
            }
            
            if let contactNo = guest.1["contact_no"].string {
                viewGuestList.contact_no = contactNo
            }
            
            if let checkIn = guest.1["whencheckedin"].string {
                viewGuestList.whenCheckedIn = checkIn
            }
            if let checkout = guest.1["whencheckedout"].string {
                viewGuestList.whenCheckedOut = checkout
            }
            
            
            guestList.append(viewGuestList)
            
        }
        return guestList
    }
    
}

