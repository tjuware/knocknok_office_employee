//
//  AppDelegate.swift
//  OfficeEmployee
//
//  Created by Kush Thakkar on 17/09/20.
//  Copyright © 2020 Knocknok. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import IQKeyboardManagerSwift
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var loginUrl: LoginViewController = LoginViewController()
    let gcmMessageIDKey = "gcm.message_id"
    var badgeCount: NSNumber = 0.0
    var notification:Bool = true
    var tokenID = ""
    var userInfo = [AnyHashable : Any]()
    var notiType = ""
    var navController = UINavigationController()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: [])
            let acceptAction = UNNotificationAction(identifier: "actionAccept",
                                                    title: "Accept",
                                                    options: UNNotificationActionOptions(rawValue: 0))
            let declineAction = UNNotificationAction(identifier: "actionDeny",
                                                     title: "Decline",
                                                     options: UNNotificationActionOptions(rawValue: 0))
            let visitorCategory =
                UNNotificationCategory(identifier: "VisitorNotification",
                                       actions: [acceptAction, declineAction],
                                       intentIdentifiers: [],
                                       hiddenPreviewsBodyPlaceholder: "",
                                       options: .customDismissAction)
            // Register the notification type.
            let notificationCenter = UNUserNotificationCenter.current()
            notificationCenter.setNotificationCategories([visitorCategory])
            application.registerUserNotificationSettings(settings)
            
        }
        application.registerForRemoteNotifications()
        
        IQKeyboardManager.shared.enable = true
        Messaging.messaging().delegate = self
        Messaging.messaging().shouldEstablishDirectChannel = true
        
        UNUserNotificationCenter.current().delegate = self
        if (launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary) != nil{
             // user tap notification
        }else{
            login()
        }
        setNavigationAppearance()
        // Override point for customization after application launch.
        return true
    }
    
    func login() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let userName: String? = UserDefaults.standard.string(forKey: "userName_key")
        let userPassword: String? = UserDefaults.standard.string(forKey: "password_key")
        
        if userName != nil && userPassword != nil {
            let homeView = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//            NotificationApi()
            navController = UINavigationController(rootViewController: homeView)
            navController.setNavigationBarHidden(true, animated: true)
            navController.isToolbarHidden = true
            self.window?.rootViewController = navController
        } else {
            let loginView = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            NotificationApi()
            navController = UINavigationController(rootViewController: loginView)
            navController.setNavigationBarHidden(true, animated: true)
            navController.isToolbarHidden = true
            self.window?.rootViewController = navController
        }
    }
    
    func setNavigationAppearance(){
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        UINavigationBar.appearance().isTranslucent = true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
    }
    
//    func NotificationApi() {
//        //// Firebase Notification
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//            // For iOS 10 data message (sent via FCM)
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            UIApplication.shared.registerUserNotificationSettings(settings)
//        }
//        UIApplication.shared.registerForRemoteNotifications()
//        // Add observer for InstanceID token refresh callback.
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.tokenRefreshNotification),
//                                               name: NSNotification.Name.InstanceIDTokenRefresh,
//                                               object: nil)
//        //        if  let token = InstanceID.instanceID().token() {
//        //            print("TOKEN....",token)
//        //
//        //        }
//    }
    
    
    //    func applicationDidEnterBackground(_ application: UIApplication) {
    //        //        log.info("Application: DidEnterBackground")
    //    }
    //    func applicationWillEnterForeground(_ application: UIApplication) {
    //        //        let notification: String? = UserDefaults.standard.string(forKey: "getNotification_key")
    //        //        if notification == "getNotification"{
    //        //            UserDefaults.standard.removeObject(forKey: "getNotification_key")
    //        //            let center = NotificationCenter.default
    //        //            center.post(Notification(name: NSNotification.Name("NotificationIdentifier"), object: nil))
    //        //        }
    //        print("Notification: refresh token from FCM -> )")
    //    }
    
    // [START receive_message]
    /*
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired till the user taps on the notification launching the application.
            // TODO: Handle data of notification

            // With swizzling disabled you must let Messaging know about the message, for Analytics
            Messaging.messaging().appDidReceiveMessage(userInfo)
            //        handleNotification(userInfo: userInfo as NSDictionary)

            // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }

            // Print full message.
            print(userInfo)
        }
    
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                         fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired till the user taps on the notification launching the application.
            // TODO: Handle data of notification
    
            // With swizzling disabled you must let Messaging know about the message, for Analytics
    
            Messaging.messaging().appDidReceiveMessage(userInfo)
            //        handleNotification(userInfo: userInfo as NSDictionary)
    
            // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
    
            // Print full message.
            print(userInfo)
    
            completionHandler(UIBackgroundFetchResult.newData)
        }
 */
    // [END receive_message]
    
//    @objc func tokenRefreshNotification(_ notification: Notification) {
//        //        print(InstanceID.instanceID().token() as Any)
//        //TJ Commented
//        //        if let refreshedToken = InstanceID.instanceID().token() {
//        //
//        //            print("Notification: refresh token from FCM -> \(refreshedToken)")
//        //            UserDefaults.standard.set(refreshedToken, forKey: "fcmToken_key")
//        //
//        //        }
//        InstanceID.instanceID().instanceID(handler: { (result, error) in
//            if let error = error {
//                print("Error fetching remote instange ID: \(error)") }
//            else if let result = result {
//                print("Remote instance ID token: \(result.token)")
//                let refreshedToken = result.token
//                UserDefaults.standard.set(refreshedToken, forKey: "fcmToken_key")
//            }
//        })
//
//        // Connect to FCM since connection may have failed when attempted before having a token.
//    }
        
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    

    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        Messaging.messaging().delegate = self
        Messaging.messaging().apnsToken = deviceToken
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)") }
            else if let result = result {
                print("Remote instance ID token: \(result.token)")
                let refreshedToken = result.token
                UserDefaults.standard.set(refreshedToken, forKey: "fcmToken_key")
                print("Firebase registration token: \(refreshedToken)")
                let fcmTokenKey = UserDefaults.standard.string(forKey: "fcmToken_key")
                print(fcmTokenKey)
            }
        })
        
        //        Messaging.messaging().setAPNSToken(deviceToken as Data, type: .prod)
        //        Messaging.messaging().apnsToken = deviceToken
        
        //        let refreshedToken = InstanceID.instanceID().token()
        //        print("APNs token retrieved: \(String(describing: refreshedToken))")
        // With swizzling disabled you must set the APNs token here.
    }

    func getEmployeeID() -> String {
        return UserDefaults.standard.value(forKey: "employee_key") as? String  ?? ""
    }
    
    func getOfficeID()  -> String {
        return UserDefaults.standard.value(forKey: "office_key") as? String  ?? ""
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "OfficeEmployee")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler:
        @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("Notification: iOS 10 delegate(willPresent notification)")
        userInfo = notification.request.content.userInfo
        UIApplication.shared.applicationIconBadgeNumber = 1
        //                self.notificationData()
        //                UserDefaults.standard.set("getNotification", forKey: "getNotification_key")
        //                let center = NotificationCenter.default
        //                center.post(Notification(name: NSNotification.Name("NotificationIdentifier"), object: nil));
        completionHandler([.alert,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Notification: iOS 10 delegate(didReceive response)")
        userInfo = response.notification.request.content.userInfo
        self.notificationData()
        switch response.actionIdentifier {
        case "actionAccept":
            print("Accept")
        case "actionDeny":
            print("Deny")
        default:
            break
        }
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
                UIApplication.shared.applicationIconBadgeNumber = 0
                switch UIApplication.shared.applicationState {
                case .active:
                    //      UIApplication.shared.applicationIconBadgeNumber = 0
                    pushToVCFromNoti()
                case .inactive:
                    pushToVCFromNoti()
                case .background:
                    pushToVCFromNoti()
                default:
                    break
                }
        let center = NotificationCenter.default
        center.post(Notification(name: NSNotification.Name("NotificationIdentifier"), object: nil));
        UserDefaults.standard.set("getNotification", forKey: "getNotification_key")
        
        completionHandler()
    }
    
    func notificationData(){
        print("notification- ",userInfo)
        // convert String to NSData
        let visitersList = userInfo["data"] as! String
        let data = visitersList.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            {
                print(jsonArray)
                let name = jsonArray[0]["name"] as! String
                let number = jsonArray[0]["number"] as! String
                let image = jsonArray[0]["image"] as! String
                if let type = jsonArray[0]["type"] as? String{
                    UserDefaults.standard.set(type, forKey: "type_key")
                }
                let title = jsonArray[0]["title"] as! String
                let guardContact = jsonArray[0]["guard_contact"] as! String
                let officeId = jsonArray[0]["office_id"] as! String
                let receptioId = jsonArray[0]["reception_id"] as! String
                let type1 = jsonArray[0]["type1"] as! String
                self.notiType = type1
                let visitorId = jsonArray[0]["visitor_id"] as! String
                if let comingfrom = jsonArray[0]["comingfrom"] as? String{
                    UserDefaults.standard.set(comingfrom, forKey: "comingfrom_key")
                }

                UserDefaults.standard.set(name, forKey: "name_key")
                UserDefaults.standard.set(number, forKey: "number_key")
                UserDefaults.standard.set(image, forKey: "image_key")
                UserDefaults.standard.set(title, forKey: "title_key")
                UserDefaults.standard.set(guardContact, forKey: "guard_key")
                UserDefaults.standard.set(officeId, forKey: "officeId_key")
                UserDefaults.standard.set(receptioId, forKey: "reception_key")
                UserDefaults.standard.set(receptioId, forKey: "reception_key")
                UserDefaults.standard.set(type1, forKey: "type1_key")
                UserDefaults.standard.set(visitorId, forKey: "visitoriD_key")
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    func pushToVCFromNoti(){
        if notiType == "1"{
//            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let visitorVC = mainStoryboard.instantiateViewController(withIdentifier: "VisitorVC") as! VisitorVC
//            navController = UINavigationController(rootViewController: visitorVC)
//            navController.setNavigationBarHidden(true, animated: true)
//            navController.isToolbarHidden = true
//            self.window?.rootViewController = navController
//
            let visitorVC = VisitorListVC(nibName: "VisitorListVC", bundle: nil)
            navController = UINavigationController(rootViewController: visitorVC)
            navController.setNavigationBarHidden(true, animated: true)
            navController.isToolbarHidden = true
            self.window?.rootViewController = navController
            
        }else if notiType == "2"{
            let deliveryLogs = DeliveryLogs(nibName: "DeliveryLogs", bundle: nil)
            navController = UINavigationController(rootViewController: deliveryLogs)
            navController.setNavigationBarHidden(true, animated: true)
            navController.isToolbarHidden = true
            self.window?.rootViewController = navController
        }
    }
}

//MARK: MessagingDelegate
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)") }
            else if let result = result {
                print("Remote instance ID token: \(result.token)")
                let refreshedToken = result.token
                UserDefaults.standard.set(refreshedToken, forKey: "fcmToken_key")
                print("Firebase registration token: \(fcmToken)")
                let fcmTokenKey = UserDefaults.standard.string(forKey: "fcmToken_key")
                print(fcmTokenKey)
            }
        })
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        //                self.updateGCM(gcmId: fcmToken)
    }

    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
