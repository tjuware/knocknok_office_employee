//
//  AddBlackListViewController.swift
//  DigitalGorkha
//
//  Created by Ankit  Bhardwaj on 20/02/19.
//  Copyright © 2019 Ankit Bhardwaj. All rights reserved.
//

import UIKit

protocol addUserDelegate: class {
    func  userCloseButtonAction()
}

class AddBlackListViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    weak var userDelegate : addUserDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mobileNumberTextField.delegate = self
        self.nameTextField.delegate = self
        showAnimate()
        createShadow(containerView: self.view)
        setUpView()

        // Do any additional setup after loading the view.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
            animateViewMoving(up: true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateViewMoving(up: false, moveValue: 100)
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    func setUpView()  {
        mobileNumberTextField.clipsToBounds = true
        mobileNumberTextField.setTextFieldRoundBorder()
        nameTextField.clipsToBounds = true
        nameTextField.setTextFieldRoundBorder()
        mobileNumberTextField.keyboardType = UIKeyboardType.phonePad

    }

    
    @IBAction func closeButtonAction(_ sender: Any) {
        removeAnimate()
        userDelegate?.userCloseButtonAction()
        self.view.endEditing(true)
        super.view.isUserInteractionEnabled = true
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if  nameTextField.isEmpty() || mobileNumberTextField.text == ""{
            self.presentWindow!.makeToast(message: "Please enter Name & Mobile Number")
        }else {
            AddBlackListApi()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    func AddBlackListApi() {
        self.submitButton.isUserInteractionEnabled = false
        let blur = UIVisualEffectView(effect: UIBlurEffect(style:
            UIBlurEffect.Style.prominent))
        blur.frame = submitButton.bounds
        blur.isUserInteractionEnabled = false //This allows touches to forward to the button.
        submitButton.insertSubview(blur, at: 0)
        self.showLoadingIndictor()
        DataManager.sharedInstance().method = "add_black_list"
        DataManager.sharedInstance().addBlackRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), visitorName: self.nameTextField.text!, visitorNo: self.mobileNumberTextField.text!, apiName: addBlackList, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                print(theDictionary)
                let errorCode = theDictionary["error_code"].string
                let messageAert = theDictionary["message"].string
                if errorCode == "0"{
//                    self.showValidationAlert(title: "Alert!!", message: messageAert!)
                    self.nameTextField.text = ""
                    self.mobileNumberTextField.text = ""
                    self.removeAnimate()
                    self.userDelegate?.userCloseButtonAction()
                    self.view.endEditing(true)
                    super.view.isUserInteractionEnabled = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationID"), object: nil)

                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumberTextField{
            guard let textFieldText = mobileNumberTextField!.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            if count > 10{
                DispatchQueue.main.async {
                    self.presentWindow!.makeToast(message: "Mobile Number should be 10 digit")
                }
            }
            return count <= 10
        }
        return true
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
