//
//  HomeViewController.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 07/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var todayButton: UIButton!
    @IBOutlet weak var lastSevenDaysButton: UIButton!
    @IBOutlet weak var lastThirtyDaysButton: UIButton!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var fromDateButton: UIButton!
    @IBOutlet weak var toDateButton: UIButton!
    @IBOutlet weak var liveVisitorsTableView: UITableView!
    
   
    @IBOutlet weak var buttonTappeds: UIButton!
    @IBOutlet weak var dateContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateContainerView: UIView!
    
    var viewControllerIdentifier = "LiveVisiterVC"
    var loginUrl: LoginViewController = LoginViewController()
    var notificationDilog = ""
    var tapGesture = UITapGestureRecognizer()
    var lastDays = ""
    let imageURL = "http://www.digitalgorkha.com/officeupload/"

    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var noDataFoundLabel: UILabel!
    var liveVisit =  LiveVisitors()
    var liveVistorsArray = [LiveVisitors]()
    var gblLiveVistorsArray = [LiveVisitors]()
    var userDetails = DataManager.sharedInstance().userDetails
    let datePicker = UIDatePicker()
    var name = ""
    var number = ""
    var nameNumber = ""
    var visitorId = ""
    var isimpContact = ""
    var isAllowContact = ""
    var checkInContact = ""
    var checkOutContact = ""

    var serachVisistorListArray = [LiveVisitors]()
    var afterloginPress = LoginViewController()
    
    var startDate = ""
    var endDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        afterLogin()
                //TJ Commented
//        slideMenuShow(menuButton, viewcontroller: self)
        liveVisitorsTableView.delegate = self
        liveVisitorsTableView.dataSource = self
        liveVisitorsTableView.layer.borderWidth = 3
        liveVisitorsTableView.layer.borderColor = UIColor.lightGrayBorderColor.cgColor
        liveVisitorsTableView.clipsToBounds = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
      //  updateFcmAPI()

//        let notification: String? = UserDefaults.standard.string(forKey: "getNotification_key")
//        if notification == "getNotification"{
//            NotificationCenter.default.addObserver(self, selector: #selector(methodOfReceivedNotification), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
//            UserDefaults.standard.removeObject(forKey: "getNotification_key")
//        }else{
              NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
//        }

    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        DispatchQueue.main.async
            {
                let popup : NotificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationViewController") as! NotificationViewController
                let navigationController = UINavigationController(rootViewController: popup)
                navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(navigationController, animated: true, completion: nil)
        }
       
    }
    
    @objc override func dismissKeyboard() {
        view.endEditing(true)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.liveVisitorsTableView.reloadData()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Inital Setup ...
    
    func setUpView() {
        
        if viewControllerIdentifier == "LiveVisiterVC"{
            self.navigationItem.title = "Live Visitors"
            if startDate == "" && endDate == "" {
                startDate = Date().dateString("dd-MM-yyyy")
                endDate = Date().dateString("dd-MM-yyyy")
            }
            self.searchBar.placeholder = "Search Visitor"
            getLiveVisters(startDate: startDate, endDate: endDate)
        }else if viewControllerIdentifier == "OldLiveVisiterVC"{
            self.navigationItem.title = "LiveVisiters OLD"
        }else if viewControllerIdentifier == "DeliveryLogsVC"{
            self.navigationItem.title = "Delivery Logs"
            self.searchBar.placeholder = "Search Delivery"
            if startDate == "" && endDate == "" {
                startDate = Date().dateString("dd-MM-yyyy")
                endDate = Date().dateString("dd-MM-yyyy")
            }
            DeliveryList(startDateDelivery: startDate, endDateDelivery: endDate)
        }else if viewControllerIdentifier == "ImpContactVC"{
            self.navigationItem.title = "Imp Contact"
            self.searchBar.placeholder = "Search Contact"
            ImpContactList()
        }
        
        lastSevenDaysButton.layer.borderColor = UIColor.grayBorderColor.cgColor
        lastSevenDaysButton.layer.borderWidth = 1.0
        
        lastThirtyDaysButton.layer.borderColor = UIColor.grayBorderColor.cgColor
        lastThirtyDaysButton.layer.borderWidth = 1.0
        
        fromDateButton.layer.borderColor = UIColor.grayBorderColor.cgColor
        fromDateButton.layer.borderWidth = 1.0
        
        toDateButton.layer.borderColor = UIColor.grayBorderColor.cgColor
        toDateButton.layer.borderWidth = 1.0
        
        dateView.layer.borderColor = UIColor.grayBorderColor.cgColor
        dateView.layer.borderWidth = 1.0
        
        todayButton.layer.borderColor = UIColor.white.cgColor
        todayButton.layer.borderWidth = 1.0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.gblLiveVistorsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewControllerIdentifier == "LiveVisiterVC" {
            var cell = tableView.dequeueReusableCell(withIdentifier: "visitorsTableViewCell", for: indexPath) as? VisitorsTableViewCell
            
            if cell == nil {
                cell = tableView.dequeueReusableCell(withIdentifier: "visitorsTableViewCell", for: indexPath) as? VisitorsTableViewCell
            }
            liveVisit = gblLiveVistorsArray[indexPath.row]
            isimpContact = liveVisit.isImpContact
            isAllowContact = liveVisit.isAllow
            checkInContact = liveVisit.checkIn
            checkOutContact = liveVisit.checkOut
            cell?.alowDenayView.isHidden = true
            if isimpContact == "0" {
                cell!.addImpButton.setImage(UIImage(named: "star_unselected"), for: UIControlState.normal)
            }else{
                cell!.addImpButton.setImage(UIImage(named: "star_selected"), for: UIControlState.normal)
            }
            
            if isAllowContact == "0" {
                cell!.statusLabel.text = "Pending"
                cell!.statusLabel.backgroundColor = UIColor(red: 0/255, green: 128/255, blue: 255/255, alpha: 1.0)
                 cell?.alowDenayView.isHidden = false
                cell!.checkInOutButton.setImage(UIImage(named: "checkout"), for: UIControlState.normal)
                cell!.checkoutLabel.text = "CheckOut"
            }else if isAllowContact == "1"{
                cell!.statusLabel.text = "Allowed"
                cell!.statusLabel.backgroundColor = UIColor(red:74/255, green:150/255, blue:132/255, alpha: 1)
                cell?.alowDenayView.isHidden = true
            }else if isAllowContact == "2" {
                cell!.statusLabel.text = "Denied"
                cell!.statusLabel.backgroundColor = UIColor(red:255/255, green:51/255, blue:51/255, alpha: 1)
                cell?.alowDenayView.isHidden = true
            }else if isAllowContact == "3" {
                cell!.statusLabel.text = "Delay"
                cell!.statusLabel.backgroundColor = UIColor(red:255/255, green:51/255, blue:51/255, alpha: 1)
                cell?.alowDenayView.isHidden = true
            }else if isAllowContact == "4" {
                cell!.statusLabel.text = "Not Availabel"
                cell!.statusLabel.backgroundColor = UIColor(red:255/255, green:51/255, blue:51/255, alpha: 1)
                cell?.alowDenayView.isHidden = true
            }
            if liveVisit.isExpected == "0"{
                if isAllowContact == "1" || isAllowContact == "2" || isAllowContact == "3" || isAllowContact == "4"{
                    cell?.alowDenayView.isHidden = true
                }else{
                    cell?.alowDenayView.isHidden = false
                }
            }else if liveVisit.isExpected  == "1"{
                cell!.statusLabel.text = "Expected Visitor"
                cell!.statusLabel.backgroundColor = UIColor(red: 143/255, green: 188/255, blue: 143/255, alpha: 1.0)
                cell?.alowDenayView.isHidden = true
                cell!.checkInOutButton.isHidden = false
            }
            if checkOutContact == "0000-00-00 00:00:00"{
                cell!.checkInOutButton.setImage(UIImage(named: "checkout"), for: UIControlState.normal)
                cell!.checkoutLabel.text = "CheckOut"

            }else {
                cell!.checkInOutButton.setImage(UIImage(named: ""), for: UIControlState.normal)
                cell!.checkoutLabel.text = ""
            }
                cell?.liveVisiter = gblLiveVistorsArray[indexPath.row]
            
            cell?.addImpButton.addTarget(self, action: #selector(self.addImp(sender:)), for: .touchUpInside)
            cell?.moreButton.addTarget(self, action: #selector(self.moreViewController(sender:)), for: .touchUpInside)
            cell?.checkInOutButton.addTarget(self, action: #selector(self.checkOutMethod(sender:)), for: .touchUpInside)
            cell?.checkInOutButton.addTarget(self, action: #selector(self.checkOutMethod(sender:)), for: .touchUpInside)
            cell?.allow.addTarget(self, action: #selector(self.AllowAPI(sender:)), for: .touchUpInside)
            cell?.denay.addTarget(self, action: #selector(self.DenayAPI(sender:)), for: .touchUpInside)
            cell?.visiterMobileNo.textColor = UIColor(red: 37.0/255.0, green: 170.0/255.0, blue: 145.0/255.0, alpha: 1.0)

            dateContainerView.isHidden = false
             /// Tap image
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.visitorImageTapped))
            cell?.userProfileImageView.addGestureRecognizer(tap)
            cell?.userProfileImageView.isUserInteractionEnabled = true
            /// Tap Mobile number Label
            let tapVisistorNo = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.tappedvisitorNumber))
            cell?.visiterMobileNo.addGestureRecognizer(tapVisistorNo)
            cell?.visiterMobileNo.isUserInteractionEnabled = true
            return cell!
        } else if viewControllerIdentifier == "DeliveryLogsVC"{
            var cell = tableView.dequeueReusableCell(withIdentifier: "deliveryTableViewCell", for: indexPath) as? DeliveryTableViewCell
            
            if cell == nil {
                cell = tableView.dequeueReusableCell(withIdentifier: "deliveryTableViewCell", for: indexPath) as? DeliveryTableViewCell
            }
            cell?.deliveryList = gblLiveVistorsArray[indexPath.row]
            dateContainerView.isHidden = false
            cell?.selectionStyle = UITableViewCellSelectionStyle.none

            return cell!
        } else {
            var cell = tableView.dequeueReusableCell(withIdentifier: "impContactsTableViewCell", for: indexPath) as? ImpContactsTableViewCell
            
            if cell == nil {
                cell = tableView.dequeueReusableCell(withIdentifier: "impContactsTableViewCell", for: indexPath) as? ImpContactsTableViewCell
            }
            cell?.impContact = gblLiveVistorsArray[indexPath.row]
            tapGesture = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.myviewTapped(_:)))
            tapGesture = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.contactNoLabel(_:)))

            tapGesture.numberOfTapsRequired = 1
            tapGesture.numberOfTouchesRequired = 1
            cell?.starImage.addGestureRecognizer(tapGesture)
            cell?.starImage.isUserInteractionEnabled = true
            cell?.contactNoLabel.addGestureRecognizer(tapGesture)
            cell?.contactNoLabel.isUserInteractionEnabled = true
            cell?.contactNoLabel.textColor = UIColor(red: 37.0/255.0, green: 170.0/255.0, blue: 145.0/255.0, alpha: 1.0)
            dateContainerView.isHidden = true
            dateContainerHeightConstraint.constant = 0
            cell?.selectionStyle = UITableViewCellSelectionStyle.none

            return cell!
        }
        
    }
    
    @objc func contactNoLabel(_ sender: UIGestureRecognizer) {
        var indexPath: IndexPath? = nil
        if let aSuperview = sender.view?.superview?.superview?.superview  as? ImpContactsTableViewCell {
            indexPath = liveVisitorsTableView.indexPath(for: aSuperview)
        }
        liveVisit = liveVistorsArray[indexPath!.row]
        let mobileNumber = liveVisit.contactNo
        var phoneNumberString = mobileNumber
        phoneNumberString = phoneNumberString.replacingOccurrences(of: " ", with: "")
        phoneNumberString = "tel:\(phoneNumberString)"
        let phoneNumberURL = URL(string: phoneNumberString)
        if let phoneNumberURL = phoneNumberURL {
            UIApplication.shared.openURL(phoneNumberURL)
        }
    }
    
    @objc func visitorImageTapped(sender: UIGestureRecognizer)
    {
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "visitorImageViewController") as! VisitorImageViewController
        var indexPath: IndexPath? = nil
        if let aSuperview = sender.view?.superview?.superview as? VisitorsTableViewCell {
            indexPath = liveVisitorsTableView.indexPath(for: aSuperview)
        }
        liveVisit = liveVistorsArray[indexPath!.row]
        let imageProfile = liveVisit.profileImage
        let visitorimageUrl = imageURL + imageProfile
        popOverVC.imageVisitor = visitorimageUrl
        self.addChildViewController(popOverVC)
        popOverVC.modalTransitionStyle = .crossDissolve
        popOverVC.view.frame = self.view.bounds
        self.view.addSubview(popOverVC.view)
        UIApplication.shared.windows.first?.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    
    @objc func tappedvisitorNumber(sender: UIGestureRecognizer)
    {
        var indexPath: IndexPath? = nil
        if let aSuperview = sender.view?.superview?.superview as? VisitorsTableViewCell {
            indexPath = liveVisitorsTableView.indexPath(for: aSuperview)
        }
        liveVisit = liveVistorsArray[indexPath!.row]
        let mobileNumber = liveVisit.contactNo
        var phoneNumberString = mobileNumber
        phoneNumberString = phoneNumberString.replacingOccurrences(of: " ", with: "")
        phoneNumberString = "tel:\(phoneNumberString)"
        let phoneNumberURL = URL(string: phoneNumberString)
        if let phoneNumberURL = phoneNumberURL {
            UIApplication.shared.openURL(phoneNumberURL)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func moreViewController(sender: UIButton)  {
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "viewMoreViewController") as! ViewMoreViewController
                var indexPath: IndexPath? = nil
                if let aSuperview = sender.superview?.superview?.superview?.superview as? VisitorsTableViewCell {
                    indexPath = liveVisitorsTableView.indexPath(for: aSuperview)
                }
                liveVisit = liveVistorsArray[indexPath!.row]
                visitorId = liveVisit.visitorId
                let purpose = liveVisit.purposeLive
                let companyName = liveVisit.deliveryCompanyName
                let checkIn = liveVisit.checkIn
                let checkOut = liveVisit.checkOut
                popOverVC.checkIn = checkIn
                popOverVC.checkOut = checkOut
                popOverVC.userPurpose = purpose
                popOverVC.companyName = companyName
                self.addChildViewController(popOverVC)
                popOverVC.modalTransitionStyle = .crossDissolve
                popOverVC.view.frame = self.view.bounds
                self.view.addSubview(popOverVC.view)
                UIApplication.shared.windows.first?.addSubview(popOverVC.view)
                popOverVC.didMove(toParentViewController: self)
    }
    
//    @objc func addReminder(sender: UIButton)  {
//        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "addReminderViewController") as! AddReminderViewController
//        var indexPath: IndexPath? = nil
//        if let aSuperview = sender.superview?.superview?.superview?.superview as? VisitorsTableViewCell {
//            indexPath = liveVisitorsTableView.indexPath(for: aSuperview)
//        }
//        liveVisit = liveVistorsArray[indexPath!.row]
//        visitorId = liveVisit.visitorId
//        name = liveVisit.name
//        number = liveVisit.contactNo
//        nameNumber = name + "-\(number)"
//        popOverVC.visitorname = name
//        popOverVC.visitorNumber = number
//        popOverVC.visitornameNumber = nameNumber
//        popOverVC.visitorID = visitorId
//        self.addChildViewController(popOverVC)
//        popOverVC.modalTransitionStyle = .crossDissolve
//        popOverVC.view.frame = self.view.bounds
//        self.view.addSubview(popOverVC.view)
//        UIApplication.shared.windows.first?.addSubview(popOverVC.view)
//        popOverVC.didMove(toParentViewController: self)
//    }
//    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        let touch = sender.location(in: liveVisitorsTableView)
        if let indexPath = liveVisitorsTableView.indexPathForRow(at: touch) {
            liveVisit = liveVistorsArray[indexPath.row]
            let visitorIdContact = liveVisit.visitorId
            let checkId = liveVisit.checkId
            
            showLoadingIndictor()
            DataManager.sharedInstance().method = "imp_contact"
            DataManager.sharedInstance().AddImpRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(),visitorId: visitorIdContact, starFlag: "0", checkID: checkId ,apiName: add_imp_contact,closure: { (result) in
                print(result)
                self.hideLoadingIndictor()
                switch result {
                case .success(let data):
                    let theDictionary = data[0]
                    print(theDictionary)
                    let errorCode = theDictionary["error_code"].string
                    let messageAlert = theDictionary["message"].string
                    
                    if errorCode == "0"{
                       self.presentWindow!.makeToast(message: messageAlert!)
                       self.setUpView()
                    }else{
                       self.presentWindow!.makeToast(message: messageAlert!)
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                    
                }
            })
        }
     
    }
    
    @objc func addImp(sender: AnyObject)  {
        
        var indexPath: IndexPath? = nil
        if let aSuperview = sender.superview??.superview?.superview?.superview as? VisitorsTableViewCell {
            indexPath = liveVisitorsTableView.indexPath(for: aSuperview)
        }
        liveVisit = liveVistorsArray[indexPath!.row]
        let impContactId = liveVisit.isImpContact
        let visitorIdContact = liveVisit.visitorId
        let checkId = liveVisit.checkId
        if impContactId == "1"{
            showLoadingIndictor()
            DataManager.sharedInstance().method = "imp_contact"
            DataManager.sharedInstance().AddImpRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(),visitorId: visitorIdContact, starFlag: "0", checkID: checkId ,apiName: add_imp_contact,closure: { (result) in
                print(result)
                self.hideLoadingIndictor()
                switch result {
                case .success(let data):
                    print(data)
                  
                    let theDictionary = data[0]
                    print(theDictionary)
                    let errorCode = theDictionary["error_code"].string
                    let messageAlert = theDictionary["message"].string
                    
                    if errorCode == "0"{
                        self.presentWindow!.makeToast(message: "Removed from Imp List")
                        if self.lastDays == "lastSevenDays"{
                            self.startDate = (Calendar.current.date(byAdding: .day, value: -7, to: Date())?.dateString("yyyy-MM-dd"))!
                            self.endDate = Date().dateString("yyyy-MM-dd")
                            self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                        } else if self.lastDays == "lastThirtyDays" {
                            self.startDate = (Calendar.current.date(byAdding: .day, value: -30, to: Date())?.dateString("yyyy-MM-dd"))!
                            self.endDate = Date().dateString("yyyy-MM-dd")
                            self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                        }else if self.lastDays == "lastDays" {
                            self.startDate = (self.fromDateButton.titleLabel?.text)!
                            self.endDate = Date().dateString("yyyy-MM-dd")
                            self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                        }
                        self.view.endEditing(true)
                         self.setUpView()
                    }else{
                        self.presentWindow!.makeToast(message: messageAlert!)
                        
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                    
                }
            })
        }else{
            showLoadingIndictor()
            DataManager.sharedInstance().method = "imp_contact"
            DataManager.sharedInstance().AddImpRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(),visitorId: visitorIdContact, starFlag: "1", checkID: checkId ,apiName: add_imp_contact,closure: { (result) in
                print(result)
                self.hideLoadingIndictor()
                switch result {
                case .success(let data):
                    print(data)
                    let theDictionary = data[0]
                    print(theDictionary)
                    
                    let errorCode = theDictionary["error_code"].string
                    let messageAlert = theDictionary["message"].string
                    
                    if errorCode == "0"{
                        
                            if self.lastDays == "lastSevenDays"{
                                self.startDate = (Calendar.current.date(byAdding: .day, value: -7, to: Date())?.dateString("yyyy-MM-dd"))!
                                self.endDate = Date().dateString("yyyy-MM-dd")
                                self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                            } else if self.lastDays == "lastThirtyDays" {
                                self.startDate = (Calendar.current.date(byAdding: .day, value: -30, to: Date())?.dateString("yyyy-MM-dd"))!
                                self.endDate = Date().dateString("yyyy-MM-dd")
                                self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                            }else if self.lastDays == "lastDays" {
                                self.startDate = (self.fromDateButton.titleLabel?.text)!
                                self.endDate = Date().dateString("yyyy-MM-dd")
                                self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                            }
                        self.presentWindow!.makeToast(message: "Add to IMP Successfully")
                         self.setUpView()
                        self.view.endEditing(true)
                        
                        
                    }else{
                        self.presentWindow!.makeToast(message: messageAlert!)
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                    
                }
            })
        }
        
    }
    
    @objc func checkOutMethod(sender: UIButton)  {
        var indexPath: IndexPath? = nil
        if let aSuperview = sender.superview?.superview?.superview?.superview as? VisitorsTableViewCell {
            indexPath = liveVisitorsTableView.indexPath(for: aSuperview)
        }
        liveVisit = liveVistorsArray[indexPath!.row]
        let visitorIdContact = liveVisit.visitorId
        let dateFormatter : DateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "checkout_visitor"
        DataManager.sharedInstance().CheckOutRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(),visitorId: visitorIdContact, currentDate: dateString ,apiName: visitor_checkout,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                let errorCode = theDictionary["error_code"].string
                let messageAlert = theDictionary["checkout_details"].string

                if errorCode == "0"{
                   
                        if self.lastDays == "lastSevenDays"{
                            self.startDate = (Calendar.current.date(byAdding: .day, value: -7, to: Date())?.dateString("yyyy-MM-dd"))!
                            self.endDate = Date().dateString("yyyy-MM-dd")
                            self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                        } else if self.lastDays == "lastThirtyDays" {
                            self.startDate = (Calendar.current.date(byAdding: .day, value: -30, to: Date())?.dateString("yyyy-MM-dd"))!
                            self.endDate = Date().dateString("yyyy-MM-dd")
                            self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                        }else if self.lastDays == "lastDays" {
                            self.startDate = (self.fromDateButton.titleLabel?.text)!
                            self.endDate = Date().dateString("yyyy-MM-dd")
                            self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                        }
                    self.presentWindow!.makeToast(message: "Checkout Done")
                     self.setUpView()
                    self.view.endEditing(true)
                }else{
                    self.presentWindow!.makeToast(message: messageAlert!)

                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
    
    @objc func DenayAPI(sender: UIButton) {
        
        let employeName = UserDefaults.standard.string(forKey: "nameProfile_key")!
        var indexPath: IndexPath? = nil
        if let aSuperview = sender.superview?.superview?.superview as? VisitorsTableViewCell {
            indexPath = liveVisitorsTableView.indexPath(for: aSuperview)
        }
        liveVisit = liveVistorsArray[indexPath!.row]
        visitorId = liveVisit.visitorId
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_allow_deny"
        DataManager.sharedInstance().AllowDenayRequest(visitorName: liveVisit.name, visitiorNo: liveVisit.contactNo, empId: self.getEmployeeID(), officeId: self.getOfficeID(), statusV: "2", employeeName: employeName, receptionId:liveVisit.receptionid , visitorId: visitorId,reason: liveVisit.purposeLive, userImage: liveVisit.profileImage, apiName: visitorAllowDeny, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                print(theDictionary)
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                
                if errorCode == 0{
                    self.presentWindow!.makeToast(message: "You have Denied Visitor")
                    if self.lastDays == "lastSevenDays"{
                        self.startDate = (Calendar.current.date(byAdding: .day, value: -7, to: Date())?.dateString("yyyy-MM-dd"))!
                        self.endDate = Date().dateString("yyyy-MM-dd")
                        self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                    } else if self.lastDays == "lastThirtyDays" {
                        self.startDate = (Calendar.current.date(byAdding: .day, value: -30, to: Date())?.dateString("yyyy-MM-dd"))!
                        self.endDate = Date().dateString("yyyy-MM-dd")
                        self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                    }else if self.lastDays == "lastDays" {
                        self.startDate = (self.fromDateButton.titleLabel?.text)!
                        self.endDate = Date().dateString("yyyy-MM-dd")
                        self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                    }
                    self.view.endEditing(true)
                    self.setUpView()
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
      @objc func AllowAPI(sender: UIButton)  {
        let employeName = UserDefaults.standard.string(forKey: "nameProfile_key")!
        var indexPath: IndexPath? = nil
        if let aSuperview = sender.superview?.superview?.superview as? VisitorsTableViewCell {
            indexPath = liveVisitorsTableView.indexPath(for: aSuperview)
        }
        liveVisit = liveVistorsArray[indexPath!.row]
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_allow_deny"
        DataManager.sharedInstance().AllowDenayRequest(visitorName: liveVisit.name, visitiorNo: liveVisit.contactNo, empId: self.getEmployeeID(), officeId: self.getOfficeID(), statusV: "1", employeeName: employeName, receptionId: liveVisit.receptionid, visitorId: liveVisit.visitorId,reason: liveVisit.purposeLive, userImage: liveVisit.profileImage, apiName: visitorAllowDeny, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                if errorCode == 0{
                    self.presentWindow!.makeToast(message: "You have Allowed Visitor")
                    if self.lastDays == "lastSevenDays"{
                        self.startDate = (Calendar.current.date(byAdding: .day, value: -7, to: Date())?.dateString("yyyy-MM-dd"))!
                        self.endDate = Date().dateString("yyyy-MM-dd")
                        self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                    } else if self.lastDays == "lastThirtyDays" {
                        self.startDate = (Calendar.current.date(byAdding: .day, value: -30, to: Date())?.dateString("yyyy-MM-dd"))!
                        self.endDate = Date().dateString("yyyy-MM-dd")
                        self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                    }else if self.lastDays == "lastDays" {
                        self.startDate = (self.fromDateButton.titleLabel?.text)!
                        self.endDate = Date().dateString("yyyy-MM-dd")
                        self.getLiveVisters(startDate: self.startDate, endDate: self.endDate)
                    }
                    self.view.endEditing(true)
                    self.setUpView()
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
    @IBAction func menuClicked(_ sender: UIBarButtonItem) {
        //        slideMenuShow(menuButton, viewcontroller: self)
    }
    
    @IBAction func lastSevenButtonAction(_ sender: UIButton) {
        // sender.backgroundColor = UIColor.white
    }
    
    @IBAction func lastThirtyButtonAction(_ sender: UIButton) {
        // sender.backgroundColor = UIColor.white
    }
    
    @IBAction func fromDateAction(_ sender: UIButton) {
        let currentDate = Date()
        var dateComponents = DateComponents()
        let calendar = Calendar.init(identifier: .gregorian)
        dateComponents.year = -100
        let minDate = calendar.date(byAdding: dateComponents, to: currentDate)
       
        RPicker.selectDate(title: "Select start date", hideCancel: true, minDate: minDate, maxDate: Date().dateByAddingYears(5), didSelectDate: { (selectedDate) in
            // TODO: Your implementation for date
            self.startDate = selectedDate.dateString("yyyy-MM-dd")
            self.fromDateButton.titleLabel?.text = selectedDate.dateString("yyyy-MM-dd")
            self.fromDateButton.setTitle(selectedDate.dateString("yyyy-MM-dd"), for: .normal)
        })
    }
  
    @IBAction func toDateAction(_ sender: UIButton) {
        // sender.backgroundColor = UIColor.white
        let currentDate = Date()
        var dateComponents = DateComponents()
        let calendar = Calendar.init(identifier: .gregorian)
        dateComponents.year = -100
        let minDates = calendar.date(byAdding: dateComponents, to: currentDate)
        self.fromDateButton.setTitle(startDate, for: .normal)
        RPicker.selectDate(title: "Select end date", hideCancel: true, minDate: minDates, maxDate: Date().dateByAddingYears(5), didSelectDate: { (selectedDate) in
            // TODO: Your implementation for date
            self.toDateButton.titleLabel?.text = selectedDate.dateString("yyyy-MM-dd")
            self.endDate = selectedDate.dateString("yyyy-MM-dd")
            self.setUpView()
        })
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        
        for subview in dateContainerView.subviews {
            if subview.isKind(of: UIButton.self) {
                subview.backgroundColor = UIColor.lightGrayButtonBackgroundColor
                subview.layer.borderColor = UIColor.grayBorderColor.cgColor
                
            }
        }
        
        for subview in dateView.subviews {
            if subview.isKind(of: UIButton.self) {
                subview.backgroundColor = UIColor.lightGrayButtonBackgroundColor
                subview.layer.borderColor = UIColor.grayBorderColor.cgColor
                
            }
        }
        sender.backgroundColor = UIColor.white
        sender.layer.borderColor = UIColor.white.cgColor
        
        var startDate = ""
        var endDate = ""
        
        if sender.tag == 1 {
            startDate = Date().dateString("yyyy-MM-dd")
            endDate = Date().dateString("yyyy-MM-dd")
            if viewControllerIdentifier == "LiveVisiterVC"{
                getLiveVisters(startDate: startDate, endDate: endDate)

            }else if viewControllerIdentifier == "DeliveryLogsVC"{
                DeliveryList(startDateDelivery: startDate, endDateDelivery: endDate)
            }
        } else  if sender.tag == 2 {
            startDate = (Calendar.current.date(byAdding: .day, value: -7, to: Date())?.dateString("yyyy-MM-dd"))!
            endDate = Date().dateString("yyyy-MM-dd")
            if viewControllerIdentifier == "LiveVisiterVC"{
                getLiveVisters(startDate: startDate, endDate: endDate)
                
            }else if viewControllerIdentifier == "DeliveryLogsVC"{
                DeliveryList(startDateDelivery: startDate, endDateDelivery: endDate)
            }
            self.lastDays = "lastSevenDays"
        }else if sender.tag == 3 {
            startDate = (Calendar.current.date(byAdding: .day, value: -30, to: Date())?.dateString("yyyy-MM-dd"))!
            endDate = Date().dateString("yyyy-MM-dd")
            if viewControllerIdentifier == "LiveVisiterVC"{
                getLiveVisters(startDate: startDate, endDate: endDate)
                
            }else if viewControllerIdentifier == "DeliveryLogsVC"{
                DeliveryList(startDateDelivery: startDate, endDateDelivery: endDate)
            }
            self.lastDays = "lastThirtyDays"
        }else if sender.tag == 4 {
            startDate = (self.fromDateButton.titleLabel?.text)!
            endDate = Date().dateString("yyyy-MM-dd")
            self.lastDays = "lastDays"
           // getLiveVisters(startDate: startDate, endDate: endDate)
            
        } else if sender.tag == 5 {
            startDate = (self.fromDateButton.titleLabel?.text)!
            endDate = (self.toDateButton.titleLabel?.text)!
            self.lastDays = "lastDays"
        }

    }
    
    func noData() {
        noDataFoundLabel.isHidden = false
        self.liveVisitorsTableView.reloadData()
    }
    
    func afterLogin()  {
      
        DataManager.sharedInstance().deviceToken = getDeviceToken()
        DataManager.sharedInstance().method = "authenticate"
        DataManager.sharedInstance().username = UserDefaults.standard.value(forKey: "userName_key") as? String ?? ""
        DataManager.sharedInstance().password = UserDefaults.standard.value(forKey: "password_key") as? String ?? ""
        //DataManager.sharedInstance().userDetails = UserDefaults.standard.object(forKey: "userDetails") as! User
        self.setUpView()
    }
    
    func getLiveVisters(startDate: String, endDate:String) {
      
        showLoadingIndictor()
        liveVistorsArray = []
        DataManager.sharedInstance().method = "live_visitor"
        print(self.userDetails.employeeId)
        DataManager.sharedInstance().LiveVistorsRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(),startDate:startDate ,endDate:endDate, apiName: liveVisitors,closure: { (result) in
            print(result)
           self.hideLoadingIndictor()
            
            switch result {
            case .success(let data):
                print(data)
                
                self.liveVistorsArray = data;
                self.gblLiveVistorsArray = self.liveVistorsArray
                if self.liveVistorsArray.count == 0 {
                self.noData()
                } else {
                    self.noDataFoundLabel.isHidden = true
                    self.liveVisitorsTableView.reloadData()
                    self.liveVisitorsTableView.scrollsToTop = true
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
    
    func ImpContactList() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "imp_visitor"
        DataManager.sharedInstance().ImpContactRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(), apiName: imp_contact_list,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.liveVistorsArray = data;
                self.gblLiveVistorsArray = self.liveVistorsArray
                if self.liveVistorsArray.count == 0 {
                    self.noData()
                } else {
                    self.noDataFoundLabel.isHidden = true
                    self.liveVisitorsTableView.reloadData()
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
   
    func DeliveryList(startDateDelivery: String, endDateDelivery:String) {
        print(startDateDelivery)
        print(endDateDelivery)
        showLoadingIndictor()
        DataManager.sharedInstance().method = "delivery"
        DataManager.sharedInstance().DeliveryRequest(empId:  self.getEmployeeID(), officeId: self.getOfficeID(),startDate:startDateDelivery ,endDate:endDateDelivery, apiName: delivery,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.liveVistorsArray = data;
                self.gblLiveVistorsArray = self.liveVistorsArray
                if self.liveVistorsArray.count == 0 {
                    self.noData()
                } else {
                    self.noDataFoundLabel.isHidden = true
                    self.liveVisitorsTableView.reloadData()
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchCommon(searchText: searchText)
        DispatchQueue.main.async { [weak self] in
            self?.liveVisitorsTableView?.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool  {
        return true
    }
    
    
    func searchCommon(searchText: String)  {
        if searchText.characters.count > 0 {
        gblLiveVistorsArray = liveVistorsArray.filter {
             let stringMatch = ($0.name).range(of: searchText.lowercased())
            return stringMatch != nil ? true : false
        }
        }else{
             self.gblLiveVistorsArray = liveVistorsArray
             print(gblLiveVistorsArray)
        }
        
    }
    
    
}

extension Date {
    
    func dateString(_ format: String = "yyyy-MM-dd") -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
    
    func dateByAddingYears(_ dYears: Int) -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.year = dYears
        
        return Calendar.current.date(byAdding: dateComponents, to: self)!
    }
}

