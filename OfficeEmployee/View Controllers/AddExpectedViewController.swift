//
//  AddExpectedViewController.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 11/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit
import ContactsUI

class AddExpectedViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    @IBOutlet weak var searchBarContact: UISearchBar!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var showInvitationButton: UIButton!
    @IBOutlet weak var nodataFoundLabel: UILabel!
    @IBOutlet weak var addInvitation: UIButton!
    @IBOutlet weak var sendInvitation: UIButton!
    @IBOutlet weak var addExpectedTableView: UITableView!
    let contactStore = CNContactStore()
    var contactList: [CNContact]!
    var contactNameArray = [String]()
    var filteredData = [CNContact]()
    var contactNumberArray = [String]()
    var selectedContactArray = [String]()
    var indexPath:IndexPath?
    var sortedContact = [String]()
    var arrIndexSection : NSArray = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    var contactCountArray = [[String: Any]]()
    var arrContact:[AnyHashable] = []
    
    var selectedDate = ""
    var rowsWhichAreChecked = [NSIndexPath]()
    var arrDict :NSMutableArray=[]
    var searchName = ""
    var inSearchMode = false
    var searchIndex = ""
    var showInvitation = false
    var dataSourceList = [ContactList]()
    var contactInfoList = [ContactList]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        setBackButtonForNavigation()
        contact()
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(AddExpectedViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        DispatchQueue.main.async
            {
                let popup : NotificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationViewController") as! NotificationViewController
                let navigationController = UINavigationController(rootViewController: popup)
                navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    func setUp() {
        self.navigationItem.title = "Add Expected"
        addExpectedTableView.delegate = self
        addExpectedTableView.dataSource = self
        self.addExpectedTableView.tableFooterView = UIView()
        self.navigationItem.backBarButtonItem?.title = ""
        
        addInvitation.titleLabel!.lineBreakMode = .byTruncatingTail
        addInvitation.titleLabel!.textAlignment = .center
        //        addInvitation.setTitle("Show Invitations", for: .normal)
        self.nodataFoundLabel.isHidden = true
    }
    
    func contact(){
        
        let store = CNContactStore()
        var index = 0
        store.requestAccess(for: .contacts, completionHandler: { (success, error) in
            if success {
                let keys = CNContactViewController.descriptorForRequiredKeys()
                let request = CNContactFetchRequest(keysToFetch: [keys])
                request.sortOrder = CNContactSortOrder.userDefault
                do {
                    self.contactList = []
                    try store.enumerateContacts(with: request, usingBlock: { (contact, status) in
                        self.contactList.append(contact)
                        index = index + 1
                        let phoneNumber = (contact.phoneNumbers.first?.value)?.stringValue
                        let contactInfo = ContactList(id: index, givenName: contact.givenName, familyName: contact.familyName, phoneNumber: phoneNumber, isChecked: false)
                        self.contactInfoList.append(contactInfo)
                    })
                } catch {
                    print("Error")
                }
                DispatchQueue.main.async {
                    self.dataSourceList = self.contactInfoList
                    self.addExpectedTableView.reloadData()
                }
            }
        })
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    
    public func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.arrIndexSection as? [String] //Side Section title
    }
    
    public func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int
    {
        return index
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if showInvitation == true {
            return "0"
        }else{
            return arrIndexSection.object(at: section) as? String
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = addExpectedTableView.dequeueReusableCell(withIdentifier: "addCell") as? AddExpectedCell
        if dataSourceList.count == 0 {
            self.nodataFoundLabel.isHidden = false
        }else{
            self.nodataFoundLabel.isHidden = true
            if showInvitation == true {
                self.searchBar.alpha = 0.5
                self.searchBar.isUserInteractionEnabled = false
                cell?.configureInvitationInfo(contactInfo: dataSourceList[indexPath.row])
            } else {
                self.searchBar.alpha = 1.0
                self.searchBar.isUserInteractionEnabled = true
                cell?.configureContactInfo(contactInfo: dataSourceList[indexPath.row])
            }
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: UITableViewCell = tableView.cellForRow(at: indexPath)!
        cell.accessoryView = UIImageView(image: UIImage(named: "Check"))
        cell.accessoryType = .checkmark
        searchBar.resignFirstResponder()
        let selectedContact  = dataSourceList[indexPath.row] as ContactList
        for contact in contactInfoList {
            if contact.id == selectedContact.id {
                contact.isChecked = true
                break
            }
        }
        
    }
    
    func AddExpectedVisters() {
        let userName: String? = UserDefaults.standard.string(forKey: "nameProfile_key")
        showLoadingIndictor()
        
        
        DataManager.sharedInstance().method = "add_expected"
        let myCount = String(contactCountArray.count)
        DataManager.sharedInstance().AddExpectedRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), expectDate: selectedDate, empName: userName!, visitor_count: myCount , data: contactCountArray, apiName: addExpected, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"].string
                let messageAert = theDictionary["message"].string
                
                if errorCode == "0"{
                    let alertController = UIAlertController(title: "Success!", message: "Added Successfully", preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
    @IBAction func showInvitationAction(_ sender: UIButton) {
        if showInvitationButton.titleLabel!.text == "Show Invitations"{
            showInvitation = true
            dataSourceList = contactInfoList.filter{$0.isChecked == true}
            self.addExpectedTableView.reloadData()
            showInvitationButton.setTitle("All Contact", for: UIControlState.normal)
            
        }else{
             addExpectedTableView.allowsSelection = true
            showInvitation = false
            dataSourceList = contactInfoList
            self.addExpectedTableView.reloadData()
            showInvitationButton.setTitle("Show Invitations", for: UIControlState.normal)
            
        }
        
    }
    
    @IBAction func sendInvitationAction(_ sender: UIButton) {
        if self.contactCountArray.count == 0 {
            self.showValidationAlert(title: "Alert!!", message: "Select Users")
        } else {
            RPicker.selectDate(title: "Select start date", hideCancel: true, minDate: Date(), maxDate: Date().dateByAddingYears(5), didSelectDate: { (selectedDate) in
                // TODO: Your implementation for date
                self.selectedDate = selectedDate.dateString("dd-MM-YYYY")
                self.AddExpectedVisters()
                
            })
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            view.endEditing(true)
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            dataSourceList = contactInfoList
            addExpectedTableView.reloadData()
        } else{
            dataSourceList = contactInfoList.filter {
                $0.givenName?.range(of: searchText, options: [.caseInsensitive, .diacriticInsensitive ]) != nil ||
                    $0.familyName?.range(of: searchText, options: [.caseInsensitive, .diacriticInsensitive ]) != nil
            }
            addExpectedTableView.reloadData()
        }
    }
}
