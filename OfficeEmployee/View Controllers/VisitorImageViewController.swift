//
//  VisitorImageViewController.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 22/03/19.
//  Copyright © 2019 Ashok Londhe. All rights reserved.
//

import UIKit
import SDWebImage

class VisitorImageViewController: UIViewController {

    var imageVisitor = ""
    
    @IBOutlet weak var visitorImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.visitorImage.sd_setImage(with: URL(string: imageVisitor), placeholderImage: UIImage(named: "user"))
        // Do any additional setup after loading the view.
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeAnimate()
        self.view.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
