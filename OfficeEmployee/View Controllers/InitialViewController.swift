//
//  InitialViewController.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 05/08/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var containerVIew: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.containerVIew.isHidden = true
        UIView.animate(withDuration: 4,
                                   delay: 0,
                                   usingSpringWithDamping: 0.1,
                                   initialSpringVelocity: 0.4,
                                   options: .transitionCrossDissolve,
                                   animations: {
                                    self.imageView.frame = self.containerVIew.frame
                                    
        }, completion: {
            //Code to run after animating
            (value: Bool) in
            
            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
            UIApplication.shared.keyWindow?.rootViewController = mainVC
        })

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
