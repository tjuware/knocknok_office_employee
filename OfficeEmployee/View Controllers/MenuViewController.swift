//
//  MenuViewController.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 08/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class MenuViewController: BaseViewController
//UITableViewDataSource, UITableViewDelegate
{
    
    /*
    @IBOutlet weak var profileImageButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mailIdLabel: UILabel!
    let imageURL = "http://www.digitalgorkha.com/officeupload/QR_images/"
    let menuArray = ["LIVE VISITORS", "PROFILE", "EXPECTED VISITORS", "DELIVERY LOGS", "LEAVE REQUEST", "BLOCK LIST", "IMP CONTACTS", "CHANGE PASSWORD", "LOG OUT"]
    let menuImgArray = ["liveVisitors", "profile", "expectedVisitors", "diliveryLogs", "leaveRequest", "watchList", "impContacts", "changePassword", "logout"]
    
    @IBOutlet weak var menuTableView: UITableView!
    var userDetails = DataManager.sharedInstance().userDetails

    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.width - 100
        profileDetail ()
        // Do any additional setup after loading the view.
    }
    
    func profileDetail (){
//        profileImageButton.layer.borderWidth = 1
//        profileImageButton.layer.masksToBounds = false
//        profileImageButton.layer.borderColor = UIColor.white.cgColor
//        profileImageButton.layer.cornerRadius = profileImageButton.frame.height/2
//        profileImageButton.clipsToBounds = true
        let userName: String? = UserDefaults.standard.string(forKey: "nameProfile_key")
        let userEmail: String? = UserDefaults.standard.string(forKey: "email_key")
        let profileImage: String? = UserDefaults.standard.string(forKey: "qrImage_key")
        let profileImg = imageURL + profileImage!
        self.nameLabel.text = userName
        self.mailIdLabel.text = userEmail
        profileImageButton.sd_setImage(with: URL(string:profileImg), for: .normal, placeholderImage:UIImage(named: "user")) 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func profileButtonAction(_ sender: Any) {
        let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "navViewController") as! UINavigationController
        let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "profileViewController") as! ProfileViewController
        // mainVC.vcIdentifier = "ChangePassword"
        self.revealViewController().setFrontViewPosition(FrontViewPosition.right, animated: false)
        navVC.setViewControllers([mainVC], animated: true)
        rvc.pushFrontViewController(navVC, animated: true)
    }
    @objc func statusBarChanged()  {
        
    }

    // MARK: Tableview delegate and Datasource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = self.menuTableView.dequeueReusableCell(withIdentifier: "Cell")
        if  cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "Cell")
        }
       // cell?.backgroundColor = UIColor.cellBackgroundColor
        cell?.textLabel?.text = menuArray[indexPath.row]
        cell?.textLabel?.adjustsFontSizeToFitWidth = true
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 12)
        cell?.imageView?.image = UIImage(named: menuImgArray[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.cellBackgroundColor
        
        let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "navViewController") as! UINavigationController
        
        if indexPath.row == 0  || indexPath.row == 3 || indexPath.row == 6 {
            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "homeViewController") as! HomeViewController
            
            if indexPath.row == 0 {
                DataManager.sharedInstance().method = "delivery"
                mainVC.viewControllerIdentifier = "LiveVisiterVC"
            } else if indexPath.row == 3 {
                mainVC.viewControllerIdentifier = "DeliveryLogsVC"
            } else if indexPath.row == 6 {
                mainVC.viewControllerIdentifier = "ImpContactVC"
            }
            self.revealViewController().setFrontViewPosition(FrontViewPosition.right, animated: false)
            navVC.setViewControllers([mainVC], animated: true)
            rvc.pushFrontViewController(navVC, animated: true)
        } else if indexPath.row == 1 {
            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "profileViewController") as! ProfileViewController
            // mainVC.vcIdentifier = "ChangePassword"
            self.revealViewController().setFrontViewPosition(FrontViewPosition.right, animated: false)
            navVC.setViewControllers([mainVC], animated: true)
            rvc.pushFrontViewController(navVC, animated: true)
        } else if indexPath.row == 2 || indexPath.row == 5 {
            
             let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "expectedVisitersViewController") as! ExpectedVisitersViewController
            if indexPath.row == 2{
                // mainVC.vcIdentifier = "ChangePassword"
                mainVC.viewControllerIdentifier = "expectedVisitersVC"
                DataManager.sharedInstance().method = "expected_visitor"
              
            }else if indexPath.row == 5{
                mainVC.viewControllerIdentifier = "WatchListVC"
                DataManager.sharedInstance().method = "spamlist"
             
            }
            self.revealViewController().setFrontViewPosition(FrontViewPosition.right, animated: false)
            navVC.setViewControllers([mainVC], animated: true)
            rvc.pushFrontViewController(navVC, animated: true)
            
//        }else if indexPath.row == 6  {
//            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "eventViewController") as! EventViewController
//            self.revealViewController().setFrontViewPosition(FrontViewPosition.right, animated: false)
//            navVC.setViewControllers([mainVC], animated: true)
//            rvc.pushFrontViewController(navVC, animated: true)
           
        }else if indexPath.row == 4  {
            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "leaveRequestViewController") as! LeaveRequestViewController
            self.revealViewController().setFrontViewPosition(FrontViewPosition.right, animated: false)
            navVC.setViewControllers([mainVC], animated: true)
            rvc.pushFrontViewController(navVC, animated: true)
            
        }else if indexPath.row == 7 {
            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "changePasswordViewController") as! ChangePasswordViewController
            mainVC.vcIdentifier = "ChangePassword"
            self.revealViewController().setFrontViewPosition(FrontViewPosition.right, animated: false)
            navVC.setViewControllers([mainVC], animated: true)
            rvc.pushFrontViewController(navVC, animated: true)
//        }else if indexPath.row == 11 {
//            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "viewReminderViewController") as! ViewReminderViewController
//            self.revealViewController().setFrontViewPosition(FrontViewPosition.right, animated: false)
//            navVC.setViewControllers([mainVC], animated: true)
//            rvc.pushFrontViewController(navVC, animated: true)

        }else if indexPath.row == menuImgArray.count - 1 {
            LogoutVistors()
        }
    
    }
    
    func LogoutVistors() {
        showLoadingIndictor()
        DataManager.sharedInstance().method = "logout"
        DataManager.sharedInstance().LogoutRequest(empId: self.getEmployeeID(), apiName: logout,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
//                UIApplication.shared.unregisterForRemoteNotifications()
                UserDefaults.standard.removeObject(forKey: "userName_key")
                UserDefaults.standard.removeObject(forKey: "password_key")
                let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
                UIApplication.shared.keyWindow?.rootViewController = mainVC
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
 */
}
