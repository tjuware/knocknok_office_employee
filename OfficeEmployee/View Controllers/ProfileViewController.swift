//
//  ProfileViewController.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 10/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var profileImageView: UIImageView!
 
    @IBOutlet weak var qrimage: UIImageView!
    @IBOutlet weak var empIDLable: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    let imageURL = "http://www.digitalgorkha.com/officeupload/"
    let qrImageURL = "http://www.digitalgorkha.com/officeupload/QR_images/"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Profile"
        
        //TJ Commented
//        slideMenuShow(menuButton, viewcontroller: self)
//        self.revealViewController().rearViewRevealWidth = self.view.frame.width - 100
        
        let point = CGPoint(x: scrollView.contentSize.width - scrollView.frame.size.width, y: 250)
        scrollView.setContentOffset(point, animated: true)
        // Do any additional setup after loading the view.
        profileDetail()
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        DispatchQueue.main.async
            {
                let popup : NotificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationViewController") as! NotificationViewController
                let navigationController = UINavigationController(rootViewController: popup)
                navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(navigationController, animated: true, completion: nil)
        }
    }
    func profileDetail(){
        profileImageView.layer.borderWidth = 1
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.clipsToBounds = true
        let userName: String? = UserDefaults.standard.string(forKey: "nameProfile_key")
        let usermobile: String? = UserDefaults.standard.string(forKey: "contactNo_key")
        let employeeID: String? = UserDefaults.standard.string(forKey: "empID_key")
        let userEmail: String? = UserDefaults.standard.string(forKey: "email_key")
        let qrImage: String? = UserDefaults.standard.string(forKey: "qrImage_key")
        let profileImage: String? = UserDefaults.standard.string(forKey: "profileImage_key")
        let profileImg = imageURL + profileImage!
        let qrImg = qrImageURL + qrImage!
        
        self.nameLabel.text = userName
        self.mobileLabel.text = usermobile
        self.empIDLable.text = employeeID
        self.emailLabel.text = userEmail
        self.profileImageView.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "user"))
        self.qrimage.sd_setImage(with: URL(string: qrImg), placeholderImage: UIImage(named: "user"))

    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
