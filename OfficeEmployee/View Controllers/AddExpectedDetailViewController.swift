//
//  AddExpectedDetailViewController.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 11/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class AddExpectedDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var expectedView: UIView!
    @IBOutlet weak var addExpectedTableView: UITableView!
    var entryCountArray = [[String: Any]]()
    var entryNumberArray = [String]()
    var userDetails = DataManager.sharedInstance().userDetails
    var selectedDateString = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        setBackButtonForNavigation()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(AddExpectedDetailViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        DispatchQueue.main.async
            {
                let popup : NotificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationViewController") as! NotificationViewController
                let navigationController = UINavigationController(rootViewController: popup)
                navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    func setUp() {
    
        self.navigationItem.title = "Add Expected"
        self.addExpectedTableView.tableFooterView = UIView()
        self.navigationItem.backBarButtonItem?.title = ""
     
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        switch section {
        case 0:
            return entryCountArray.count
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = addExpectedTableView.dequeueReusableCell(withIdentifier: "expectedListCell", for: indexPath) as! ExpectedListTableViewCell
            cell.nameLabel.text = entryCountArray[indexPath.row]["name"] as? String
            cell.numberLabel.text = entryCountArray[indexPath.row]["number"] as? String
            cell.deleteButton.addTarget(self, action: #selector(self.deleteButtonAction(sender:)), for: .touchUpInside)

            return cell
        case 1:
            
            let cell1 = addExpectedTableView.dequeueReusableCell(withIdentifier: "expectedEntryCell", for: indexPath) as! expectedEntryTableViewCell
            cell1.mobileNumberTextField.delegate = self
            cell1.addButton.addTarget(self, action: #selector(self.addButtonAction(sender:)), for: .touchUpInside)
            cell1.submitButton.addTarget(self, action: #selector(self.submitButtonAction(sender:)), for: .touchUpInside)
            
            return cell1
        default:
            return UITableViewCell()
        }

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var rowHeight:CGFloat = 0.0
        
        if(indexPath.row == 1){
            rowHeight = 0.0
        }else{
            rowHeight = 55.0    //or whatever you like
        }
        return rowHeight
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         let cell = textField.superview?.superview?.superview as! expectedEntryTableViewCell
        if textField == cell.mobileNumberTextField{
            guard let textFieldText = cell.mobileNumberTextField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            if count > 10{
                DispatchQueue.main.async {
                    self.presentWindow!.makeToast(message: "Mobile Number should be 10 digit")
                }
            }
            return count <= 10
        }
        return true
    }
    
    func dictionary(dict : [String : Any], containsValue value : String)->Bool{
        
        let contains = dict.contains { (arg) -> Bool in
            
            let (_, v) = arg
            return v as! Bool 
        }
        return contains
    }
    
    @objc func addButtonAction(sender: UIButton)  {
        let cell = sender.superview?.superview?.superview as! expectedEntryTableViewCell
        if cell.nameTextField.text == ""  || cell.mobileNumberTextField.text == ""{
            self.showValidationAlert(title: "Alert!!", message: "User Name or Mobile Number is Missing")
        }else{
            
            if entryCountArray.count > 0{
                if entryNumberArray.contains(cell.mobileNumberTextField.text!) {
                    self.showValidationAlert(title: "Alert!!", message: "Mobile number Already Exist")
                }else {
                   var myDictionary = [String: Any]()
                    myDictionary = ["name": cell.nameTextField.text as AnyObject,
                                    "number": cell.mobileNumberTextField.text as AnyObject]
                    entryCountArray.append(myDictionary)
                    entryNumberArray.append(cell.mobileNumberTextField.text!)
                    self.addExpectedTableView.reloadData()
                    cell.nameTextField.text = ""
                    cell.mobileNumberTextField.text = ""
                    print("output",entryCountArray)
                    
                }
//                for i in 1...entryCountArray.count {
//                   let numer = (entryCountArray[0]["number"] as? String)!
//                    if numer == cell.mobileNumberTextField.text {
//                        self.showValidationAlert(title: "Alert!!", message: "Mobile number Already Exist")
//                         break
//                    }
//                }
//                if numer != cell.mobileNumberTextField.text{
//                    var myDictionary = [String: Any]()
//                    myDictionary = ["name": cell.nameTextField.text as AnyObject,
//                                    "number": cell.mobileNumberTextField.text as AnyObject]
//                    entryCountArray.append(myDictionary)
//                    self.addExpectedTableView.reloadData()
//                    cell.nameTextField.text = ""
//                    cell.mobileNumberTextField.text = ""
//                    print("output",entryCountArray)
                
            
            }else{
                var myDictionary = [String: Any]()
                myDictionary = ["name": cell.nameTextField.text as AnyObject,
                                "number": cell.mobileNumberTextField.text as AnyObject]
                entryCountArray.append(myDictionary)
                entryNumberArray.append(cell.mobileNumberTextField.text!)
                self.addExpectedTableView.reloadData()
                cell.nameTextField.text = ""
                cell.mobileNumberTextField.text = ""
                print("output",entryCountArray)
            }
            
        }
        
        
    }
    
    @objc func deleteButtonAction(sender: UIButton)  {
        var indexPath: IndexPath? = nil
        if let aSuperview = sender.superview?.superview as? ExpectedListTableViewCell {
            indexPath = addExpectedTableView.indexPath(for: aSuperview)
            entryCountArray.remove(at: (indexPath?.row)!)
            entryNumberArray.remove(at: (indexPath?.row)!)
            self.addExpectedTableView.reloadData()
        }
        

    }
    
    func AddExpectedVisters() {
        let userName: String? = UserDefaults.standard.string(forKey: "nameProfile_key")
        showLoadingIndictor()
//        DataManager.sharedInstance().method = "add_expected"
        let myCount = String(entryCountArray.count)
        DataManager.sharedInstance().AddExpectedRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), expectDate: selectedDateString, empName: userName!, visitor_count: myCount , data: entryCountArray, apiName: addExpected, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"].string
                let messageAert = theDictionary["message"].string
                
                if errorCode == "0"{
                    let alertController = UIAlertController(title: "Alert!", message: messageAert, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
    
    
    @objc func submitButtonAction(sender: UIButton)  {
        let cell = sender.superview?.superview?.superview as! expectedEntryTableViewCell
        if entryCountArray.count > 0{
                var myDictionary = [String: Any]()
                myDictionary = ["name": cell.nameTextField.text as AnyObject,
                                "number": cell.mobileNumberTextField.text as AnyObject]
                entryCountArray.append(myDictionary)
                RPicker.selectDate(title: "Select start date", hideCancel: true, minDate: Date(), maxDate: Date().dateByAddingYears(5), didSelectDate: { (selectedDate) in
                    // TODO: Your implementation for date
                    self.selectedDateString = selectedDate.dateString("dd-MM-YYYY")
                    self.AddExpectedVisters()
                })
        } else if cell.nameTextField.text == "" || cell.mobileNumberTextField.text == "" {
            self.showValidationAlert(title: "Alert!!", message: "Please enter UserName & Mobile Number")
        }else{
            if cell.mobileNumberTextField.text!.count == 11 {
                var myDictionary = [String: Any]()
                myDictionary = ["name": cell.nameTextField.text as AnyObject,
                                "number": cell.mobileNumberTextField.text as AnyObject]
                entryCountArray.append(myDictionary)
                RPicker.selectDate(title: "Select start date", hideCancel: true, minDate: Date(), maxDate: Date().dateByAddingYears(5), didSelectDate: { (selectedDate) in
                    // TODO: Your implementation for date
                    self.selectedDateString = selectedDate.dateString("dd-MM-YYYY")
                    self.AddExpectedVisters()
                })
        }
        }
        
    }
}

