//
//  EventDetailViewController.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 21/12/18.
//  Copyright © 2018 Ashok Londhe. All rights reserved.
//

import UIKit

class EventDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var eventDetailTableView: UITableView!
     var userDetails = DataManager.sharedInstance().userDetails
     var eventIDNo = ""
     var guestListArray = [Guest]()

    @IBOutlet weak var noDataLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Guest List"
        self.eventDetailTableView.tableFooterView = UIView()
        EventDetailList()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return guestListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = eventDetailTableView.dequeueReusableCell(withIdentifier: "eventDetailCell", for: indexPath) as? EventDetailTableViewCell
        
        if cell == nil {
            cell = eventDetailTableView.dequeueReusableCell(withIdentifier: "eventDetailCell", for: indexPath) as? EventDetailTableViewCell
        }
        cell?.guestEvent = guestListArray[indexPath.row]
        cell?.selectionStyle = UITableViewCellSelectionStyle.none

        return cell!
    }
    
    func EventDetailList() {
        showLoadingIndictor()
        DataManager.sharedInstance().method = "events_members"
        DataManager.sharedInstance().GuestListRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), eventId: eventIDNo, apiName: event_member_list,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.guestListArray = data;
                if self.guestListArray.count == 0 {
                     self.noDataLabel.isHidden = false
                } else {
                    self.noDataLabel.isHidden = true
                    self.eventDetailTableView.reloadData()
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
