//
//  AddReminderViewController.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 08/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class AddReminderViewController: BaseViewController, SCPopDatePickerDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var reminderTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
//    let datePicker = UIDatePicker()
    var userDetails = DataManager.sharedInstance().userDetails
    var visitorname = ""
    var visitorNumber = ""
    var visitornameNumber = ""
    var visitorID = ""
    var selectedAmp = ""
    var dateSlected:Int = 0
    let datePicker = SCPopDatePicker()
    let date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        setUpView()
        createShadow(containerView: self.view)
        self.nameTextField.text = visitorname
        self.mobileTextField.text = visitorNumber
        // Do any additional setup after loading the view.
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        selectedAmp = formatter.string(from: now) + ".511"
        print(selectedAmp)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView()  {
        nameTextField.setTextFieldAllSqureBorder()
        mobileTextField.setTextFieldAllSqureBorder()
        reminderTextField.setTextFieldAllSqureBorder()
        closeButton.layer.cornerRadius = closeButton.bounds.height/2
        closeButton.clipsToBounds = true
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeAnimate()
        self.view.endEditing(true)
    }

    @IBAction func saveAction(_ sender: Any) {
        if reminderTextField.text == ""{
            self.presentWindow!.makeToast(message: "Data should not be Blank")
        }else{
            self.saveButton.isUserInteractionEnabled = false
            let blur = UIVisualEffectView(effect: UIBlurEffect(style:
                UIBlurEffect.Style.prominent))
            blur.frame = saveButton.bounds
            blur.isUserInteractionEnabled = false //This allows touches to forward to the button.
            saveButton.insertSubview(blur, at: 0)
            AddReminderVisters()
        }
       
    }
    @IBAction func selectTimeAction(_ sender: UIButton) {
        self.datePicker.tapToDismiss = true
        self.datePicker.datePickerType = SCDatePickerType.time
        self.datePicker.showBlur = true
        self.datePicker.datePickerStartDate = self.date
        self.datePicker.btnFontColour = UIColor.white
        self.datePicker.btnColour = UIColor.darkGray
        self.datePicker.showCornerRadius = false
        self.datePicker.delegate = self
        self.datePicker.show(attachToView: self.view)
        dateSlected = sender.tag
    }
    @IBAction func selectDateAction(_ sender: UIButton) {
        self.datePicker.tapToDismiss = true
        self.datePicker.datePickerType = SCDatePickerType.date
        self.datePicker.showBlur = true
        self.datePicker.datePickerStartDate = self.date
        self.datePicker.btnFontColour = UIColor.white
        self.datePicker.btnColour = UIColor.darkGray
        self.datePicker.showCornerRadius = false
        self.datePicker.delegate = self
        self.datePicker.show(attachToView: self.view)
        dateSlected = sender.tag

    }
    
    func scPopDatePickerDidSelectDate(_ date: Date) {
       
        if dateSlected == 1{
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd"
            let selectedDate: String = dateFormatter.string(from: date)
            print(selectedDate)
            dateButton.setTitle(selectedDate, for: UIControlState.normal)

        }else if dateSlected == 2 {
            let dateFormatter: DateFormatter = DateFormatter()
            let timeFormatter: DateFormatter = DateFormatter()

            dateFormatter.dateFormat = "hh:mm a"
            timeFormatter.dateFormat = "a"
            let selectedDate: String = dateFormatter.string(from: date)
            print(selectedDate)
            timeButton.setTitle(selectedDate, for: UIControlState.normal)
        }
      
    }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        removeAnimate()
        self.view.endEditing(true)
    }
    
    func AddReminderVisters() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_reminder"
        DataManager.sharedInstance().AddReminderRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), visitorId: visitorID, title: nameTextField!.text!, reminder: reminderTextField.text!, date: (dateButton.titleLabel?.text)!, timestamp: selectedAmp, time: (timeButton.titleLabel?.text)!, apiName: visitorsReminder, closure: { (result) in
            print(result)   
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                if errorCode == 0 {
                    self.removeAnimate()
                    self.view.endEditing(true)
                    self.presentWindow!.makeToast(message: "Reminder Added")
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                   
                }

                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
   
            }
        })
    }
    
}



