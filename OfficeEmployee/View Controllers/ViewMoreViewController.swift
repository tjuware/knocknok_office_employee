//
//  ViewMoreViewController.swift
//  DigitalGorkha
//
//  Created by Ankit  Bhardwaj on 15/03/19.
//  Copyright © 2019 Ashok Londhe. All rights reserved.
//

import UIKit

class ViewMoreViewController: UIViewController {

    @IBOutlet weak var checkInLabel: UILabel!
    @IBOutlet weak var checkOutaLabel: UILabel!
    @IBOutlet weak var purposeLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    
    var userPurpose = ""
    var companyName = ""
    var checkIn = ""
    var checkOut = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkInLabel.text = checkIn
        self.checkOutaLabel.text = checkOut
        self.purposeLabel.text = userPurpose
        self.companyLabel.text = companyName
        // Do any additional setup after loading the view.
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeAnimate()
        self.view.endEditing(true)
    }
    
    @IBAction func cancelActionButton(_ sender: Any) {
        removeAnimate()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
