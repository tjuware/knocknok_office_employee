//
//  SelectExpectedVisitorsViewController.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 10/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit


protocol SelectExpectedDelegate: class {
    func  closeButtonAction()
}

class SelectExpectedVisitorsViewController: BaseViewController {

     weak var delegate : SelectExpectedDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showAnimate()
        createShadow(containerView: self.view)
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(SelectExpectedVisitorsViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        DispatchQueue.main.async
            {
                let popup : NotificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationViewController") as! NotificationViewController
                let navigationController = UINavigationController(rootViewController: popup)
                navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(navigationController, animated: true, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func showAnimate()  
    {
        self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        removeAnimate()
        delegate?.closeButtonAction()
        self.view.endEditing(true)
        super.view.isUserInteractionEnabled = true
    }

    @IBAction func contactButtonAction(_ sender: Any) {
        removeAnimate()
        self.view.endEditing(true)
        super.view.isUserInteractionEnabled = true
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let addExpectedVC = storyBoard.instantiateViewController(withIdentifier: "addExpectedViewController") as! AddExpectedViewController
        self.navigationController?.pushViewController(addExpectedVC, animated: true)
    }
    
    @IBAction func otherButtonAction(_ sender: Any) {
        removeAnimate()
        self.view.endEditing(true)
        super.view.isUserInteractionEnabled = true
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let addExpectedVC = storyBoard.instantiateViewController(withIdentifier: "addExpectedDetailViewController") as! AddExpectedDetailViewController
        self.navigationController?.pushViewController(addExpectedVC, animated: true)
    }
  
    
}



