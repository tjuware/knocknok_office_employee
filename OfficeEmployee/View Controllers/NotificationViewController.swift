//
//  NotificationViewController.swift
//  DigitalGorkha
//
//  Created by Ankit  Bhardwaj on 15/01/19.
//  Copyright © 2019 Ankit Bhardwaj. All rights reserved.
//

import UIKit
import TNRadioButtonGroup
import SDWebImage
import AudioToolbox

class NotificationViewController: BaseViewController {

    @IBOutlet weak var notificationDetailView: UIView!
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var mobileNumberlabel: UILabel!
    @IBOutlet weak var visistorImage: UIImageView!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var visitorArrivedLabel: UILabel!
    @IBOutlet weak var reasonTextField: UITextField!
    @IBOutlet weak var submitbutton: UIButton!
    @IBOutlet weak var notificationImage: UIImageView!
    
    var visitorId = ""
    var employeName = ""
    var receptionId = ""
    /// notification DetailView
    var sexGroup: TNRadioButtonGroup?
    var dealyNotDelay = ""
    var visitorName = ""
    var titleVisitor = ""
    var visitorNumber = ""
    var typeReason = ""
    var visitorImageProfile = ""
    let imageURL = "http://www.digitalgorkha.com/officeupload/"

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        self.navigationController?.isNavigationBarHidden = true
        showAnimate()
        createShadow(containerView: self.view)
        employeName = UserDefaults.standard.string(forKey: "nameProfile_key")!
        // Do any additional setup after loading the view.
        visitorName = UserDefaults.standard.string(forKey: "name_key")!
        receptionId = UserDefaults.standard.string(forKey: "reception_key")!
        visitorId = UserDefaults.standard.string(forKey: "visitoriD_key")!
        titleVisitor = UserDefaults.standard.string(forKey: "title_key")!
        visitorNumber = UserDefaults.standard.string(forKey: "number_key")!
        typeReason = UserDefaults.standard.string(forKey: "type_key")!
        visitorImageProfile = UserDefaults.standard.string(forKey: "image_key")!
        /// Image rounded
        notificationImage.layer.borderWidth = 1
        notificationImage.layer.masksToBounds = false
        notificationImage.layer.borderColor = UIColor.white.cgColor
        notificationImage.layer.cornerRadius = notificationImage.frame.height/2
        notificationImage.clipsToBounds = true
        let profileImg = imageURL + visitorImageProfile
        self.notificationImage.sd_setImage(with: URL(string: profileImg), placeholderImage: UIImage(named: "user"))
        /// add data in labels
        self.visitorArrivedLabel.text = titleVisitor
        self.mobileNumberlabel.text = visitorNumber
        self.bodyLabel.text = visitorName
        self.reasonLabel.text = typeReason
        self.notificationDetailView.isHidden = true
        reasonTextField.setTextFieldRoundBorder()
//        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 300, repeats: true) { (t) in
                self.removeAnimate()
                self.view.endEditing(true)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func createHorizontalList() {
        let maleData = TNCircularRadioButtonData()
        maleData.labelText = "Delay"
        maleData.identifier = "delay"
        maleData.selected = true
        
        let femaleData = TNCircularRadioButtonData()
        femaleData.labelText = "Not Delay"
        femaleData.identifier = "not Delay"
        femaleData.selected = false
        femaleData.borderRadius = 12
        femaleData.circleRadius = 5
        
        self.sexGroup = TNRadioButtonGroup(radioButtonData: [maleData, femaleData], layout: TNRadioButtonGroupLayoutVertical)
        self.sexGroup?.identifier = "Sex group"
        self.sexGroup?.create()
        self.sexGroup!.position = CGPoint(x: 60, y: 335)
        self.view.addSubview( self.sexGroup!)
        NotificationCenter.default.addObserver(self, selector: #selector(NotificationViewController.statusGroupUpdated(_:)), name: NSNotification.Name(rawValue: SELECTED_RADIO_BUTTON_CHANGED), object: sexGroup)
        sexGroup!.update()
        sexGroup!.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: sexGroup!, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: -90).isActive = true
        NSLayoutConstraint(item: sexGroup!, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 20).isActive = true
        NSLayoutConstraint(item: sexGroup!, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 100).isActive = true
        NSLayoutConstraint(item: sexGroup!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 100).isActive = true
    
    }
    
    
    @IBAction func moreAction(_ sender: Any) {
        self.notificationDetailView.isHidden = false
        createHorizontalList()
    }
    @IBAction func closeButtonAction(_ sender: Any) {
        self.notificationDetailView.isHidden = true
    }
    
    @objc func statusGroupUpdated(_ notification: Notification?) {
        print("[MainView] Sex group updated to \(String(describing: sexGroup!.selectedRadioButton.data.identifier))")
        self.dealyNotDelay = sexGroup!.selectedRadioButton.data.identifier
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: SELECTED_RADIO_BUTTON_CHANGED), object: sexGroup)
    }
    
    
    @IBAction func closeAction(_ sender: UIButton) {
        DispatchQueue.main.async
            {
                self.dismiss(animated: true) {
                    self.removeAnimate()
                    self.view.endEditing(true)
                }
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func denayAction(_ sender: Any) {
        denyApi()
    }
    
    @IBAction func allowAction(_ sender: Any) {
        allowApi()
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        removeAnimate()
//        
//    }
    
    func denyApi() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_allow_deny"
        DataManager.sharedInstance().AllowDenayRequest(visitorName: self.visitorName, visitiorNo: self.visitorNumber, empId: self.getEmployeeID(), officeId: self.getOfficeID(), statusV: "2", employeeName: employeName, receptionId: receptionId, visitorId: visitorId,reason: "",userImage: visitorImageProfile, apiName: visitorAllowDeny, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                
                if errorCode == 0{
                    DispatchQueue.main.async
                        {
                            self.dismiss(animated: true) {
                                self.removeAnimate()
                                self.view.endEditing(true)
                            }
                    }
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    func allowApi() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_allow_deny"
        DataManager.sharedInstance().AllowDenayRequest(visitorName: self.visitorName, visitiorNo: self.visitorNumber, empId: self.getEmployeeID(), officeId: self.getOfficeID(), statusV: "1", employeeName: employeName, receptionId: receptionId, visitorId: visitorId,reason: "",userImage: visitorImageProfile, apiName: visitorAllowDeny, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                
                if errorCode == 0{
                    DispatchQueue.main.async
                        {
                            self.dismiss(animated: true) {
                                self.removeAnimate()
                                self.view.endEditing(true)
                            }
                    }
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
   
    func delayApi() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_allow_deny"
        DataManager.sharedInstance().AllowDenayRequest(visitorName: self.visitorName, visitiorNo: self.visitorNumber, empId: self.getEmployeeID(), officeId: self.getOfficeID(), statusV: "3", employeeName: employeName, receptionId: receptionId, visitorId: visitorId,reason: self.reasonTextField.text!,userImage: visitorImageProfile, apiName: visitorAllowDeny, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                
                if errorCode == 0{
                    DispatchQueue.main.async
                        {
                            self.dismiss(animated: true) {
                                self.notificationDetailView.isHidden = true
                                self.removeAnimate()
                                self.view.endEditing(true)
                            }
                    }
                }else{
                    self.showValidationAlert(title: "Alert!!", message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
    func notDelayApi() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_allow_deny"
        DataManager.sharedInstance().AllowDenayRequest(visitorName: self.visitorName, visitiorNo: self.visitorNumber, empId: self.getEmployeeID(), officeId: self.getOfficeID(), statusV: "4", employeeName: employeName, receptionId: receptionId, visitorId: visitorId,reason: reasonTextField.text!,userImage: visitorImageProfile, apiName: visitorAllowDeny, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"].int
                let messageAert = theDictionary["msg"].string
                
                if errorCode == 0{
                    DispatchQueue.main.async
                        {
                            self.dismiss(animated: true) {
                                self.notificationDetailView.isHidden = true
                                self.removeAnimate()
                                self.view.endEditing(true)
                            }
                    }
                    
                }else{
                    self.showValidationAlert(title: "Alert!!", message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }
    
    func buttonBlurEffect(){
        self.submitbutton.isUserInteractionEnabled = false
        let blur = UIVisualEffectView(effect: UIBlurEffect(style:
            UIBlurEffect.Style.prominent))
        blur.frame = submitbutton.bounds
        blur.isUserInteractionEnabled = false //This allows touches to forward to the button.
        submitbutton.insertSubview(blur, at: 0)
    }
    
    @IBAction func SubmitAction(_ sender: Any) {
       
        if dealyNotDelay == "not Delay"{
            buttonBlurEffect()
            notDelayApi()
        }else {
            buttonBlurEffect()
            delayApi()
        }
    }
    
}

