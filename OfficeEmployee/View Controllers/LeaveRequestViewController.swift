//
//  LeaveRequestViewController.swift
//  DigitalGorkha
//
//  Created by Ankit  Bhardwaj on 20/02/19.
//  Copyright © 2019 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class LeaveRequestViewController: BaseViewController, UITextViewDelegate {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    var startDate = ""
    var endDate = ""
    var leaveReason = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
                //TJ Commented
//        slideMenuShow(menuButton, viewcontroller: self)
        self.title = "Leave Update"
        textView.text = "Leave Request"
        textView.textColor = UIColor.lightGray
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(LeaveRequestViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        DispatchQueue.main.async
            {
                let popup : NotificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationViewController") as! NotificationViewController
                let navigationController = UINavigationController(rootViewController: popup)
                navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func startDateAction(_ sender: Any) {
        let currentDate = Date()
        var dateComponents = DateComponents()
        let calendar = Calendar.init(identifier: .gregorian)
        dateComponents.year = -100
        let minDates = calendar.date(byAdding: dateComponents, to: currentDate)
        RPicker.selectDate(title: "Select start date", hideCancel: true, minDate: minDates, maxDate: Date().dateByAddingYears(5), didSelectDate: { (selectedDate) in
            // TODO: Your implementation for date
            self.startDate = selectedDate.dateString("yyyy-MM-dd")
            self.startDateLabel.text = selectedDate.dateString("yyyy-MM-dd")
        })
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Leave Request"
            textView.textColor = UIColor.lightGray
            
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
           
        }
    }
    
    @IBAction func endDateAction(_ sender: Any) {
        let currentDate = Date()
        var dateComponents = DateComponents()
        let calendar = Calendar.init(identifier: .gregorian)
        dateComponents.year = -100
        let minDates = calendar.date(byAdding: dateComponents, to: currentDate)
        RPicker.selectDate(title: "Select start date", hideCancel: true, minDate: minDates, maxDate: Date().dateByAddingYears(5), didSelectDate: { (selectedDate) in
            // TODO: Your implementation for date
            self.endDate = selectedDate.dateString("yyyy-MM-dd")
            self.endDateLabel.text = selectedDate.dateString("yyyy-MM-dd")
        })
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if self.textView!.text == "" || self.startDateLabel.text == ""  || self.endDateLabel.text == "" {
            self.showValidationAlert(title: "Alert!!", message: "Please Enter Details")
        }else{
            LeaveRequestApi()
        }
    }
    
    func LeaveRequestApi() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "add_leave_request"
        DataManager.sharedInstance().LeaveRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), message: self.textView.text, leaveStartDate: self.startDate, leaveEndDate: self.endDate, apiName: leaveRequest, closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"].string
                let messageAert = theDictionary["message"].string
                
                if errorCode == "0"{
                    self.showValidationAlert(title: "Success!", message: messageAert!)
                    self.textView.text = "Leave Request"
                    self.textView.textColor = UIColor.lightGray
                    self.startDateLabel.text = "Start Date"
                    self.endDateLabel.text = "End Date"
                    self.textView.delegate = self
                    
                }else{
                    self.showValidationAlert(title: "Alert!!", message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }


}
