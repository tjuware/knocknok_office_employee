//
//  EventViewController.swift
//  DigitalGorkha
//
//  Created by Mayur Susare on 10/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class EventViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var eventTableView: UITableView!
    @IBOutlet weak var noDataFoundLabel: UILabel!
    
    var userDetails = DataManager.sharedInstance().userDetails
    var eventArray = [Event]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Events"
        eventTableView.delegate = self
        eventTableView.dataSource = self

        self.eventTableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
                //TJ Commented
//        slideMenuShow(menuButton, viewcontroller: self)
//        self.revealViewController().rearViewRevealWidth = self.view.frame.width - 100
        
        EventList()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = eventTableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as? EventTableViewCell
        
        if cell == nil {
            cell = eventTableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as? EventTableViewCell
        }
        cell?.visitorEvent = eventArray[indexPath.row]
        cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        cell?.selectionStyle = UITableViewCellSelectionStyle.none

        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(self.eventArray[indexPath.row])
        var event =  Event()
         event = eventArray[indexPath.row]
        let evntID = event.eventId
        let main = UIStoryboard(name: "Main", bundle: Bundle.main)
        let guestListVC = main.instantiateViewController(withIdentifier: "eventDetailViewController") as! EventDetailViewController
        guestListVC.eventIDNo = evntID
        self.navigationController?.pushViewController(guestListVC, animated: true)
    }
    
    func EventList() {
        showLoadingIndictor()
        DataManager.sharedInstance().method = "show_events"
        DataManager.sharedInstance().EventsRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), apiName: event_list,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.eventArray = data;
                if self.eventArray.count == 0 {
                    self.noDataFoundLabel.isHidden = false
                } else {
                    self.noDataFoundLabel.isHidden = true
                    self.eventTableView.reloadData()
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
