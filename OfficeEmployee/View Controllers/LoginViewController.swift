//
//  ViewController.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 07/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController, UITextFieldDelegate {
    
    // MARK: Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginWithOTPButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var containerViewTopConstraint: NSLayoutConstraint!
    
    let eyeButtonforPassword = UIButton(type: .custom)
    var userDetails = User()
    var loginPressed = Bool()
    
    // MARK: Variable declarations
    // MARK: View lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.userNameTextField.text = "abhi.dubey20@gmail.com"
//        self.passwordTextField.text = "Abhishek007"
        containerViewTopConstraint.constant = 150
        setUpView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Inital Setup ...
    
    func setUpView() {
        
        self.view.layoutIfNeeded()
        self.view.clipsToBounds = true
        
        logoImageView.layer.cornerRadius = logoImageView.bounds.width/2
        logoImageView.clipsToBounds = true
        
        containerView.layer.cornerRadius = 5
        containerView.clipsToBounds = true
        
        addShadowToView(containerView: containerView)
        
        loginButton.layer.cornerRadius = 20
        loginButton.clipsToBounds = true
        
        userNameTextField.setBottomBorder(color: UIColor.textFieldBorderColor)
        userNameTextField.setLeftIcon("username")
        
        passwordTextField.setBottomBorder(color: UIColor.textFieldBorderColor)
        passwordTextField.setLeftIcon("password")
        passwordTextField.setRightIcon("eye_close")
        
        addButton()
        passwordTextField.rightView = eyeButtonforPassword
        passwordTextField.rightViewMode = .always
        
        animation()
    }
    func animation() {
        
        let y = logoImageView.frame.origin.y + logoImageView.frame.size.height/2 - 20
        let rect = CGRect(x: containerView.frame.origin.x, y: y, width: containerView.frame.size.width, height: containerView.frame.size.height)
        UIView.animate(withDuration: 2,
                       delay: 0.1,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.2,
                       options: .transitionCrossDissolve,
                       animations: {
                        //Do all animations here
                        self.containerView.frame = rect
                        self.containerViewTopConstraint.constant = 54
                        
        }, completion: {
            //Code to run after animating
            (value: Bool) in
        })
    }
    
    func addButton(){
        let img = UIImage(named: "eye_close.png")!
        eyeButtonforPassword.setImage(img, for: .normal)
        eyeButtonforPassword.setImage(img, for: .normal)
        eyeButtonforPassword.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 10)
        eyeButtonforPassword.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        eyeButtonforPassword.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
    }
    
    // MARK: Textfield delegate method
    
    @objc func refresh()  {
        passwordTextField.isUserInteractionEnabled = true
        if passwordTextField.isSecureTextEntry {
            passwordTextField.isSecureTextEntry = false
            eyeButtonforPassword.setImage(UIImage(named:"eye_open.png"), for: .normal)
        } else {
            passwordTextField.isSecureTextEntry = true
            eyeButtonforPassword.setImage(UIImage(named:"eye_close.png"), for: .normal)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return resignFirstResponder()
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        self.loginPressed = true
        validateLogin()
        
    }
    
    @IBAction func loginWithOTPButtonPressed(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "changePasswordViewController") as! ChangePasswordViewController
        vc.vcIdentifier = "LoginOTP"
        self.present(vc, animated: true, completion: {
            
        })
    }
    
    func validateLogin()  {
        let tokenId: String? = UserDefaults.standard.string(forKey: "fcmToken_key")

            if userNameTextField.isEmpty() {
                showValidationAlert(title: "", message: errMsgUserNameEmpty)
            } else if !userNameTextField.isValidEmail() {
                showValidationAlert(title: "", message: errMsgUserNameInvalid)
            } else if passwordTextField.isEmpty() {
                showValidationAlert(title: "", message: errMsgPasswordEmpty)
            } else {
                DataManager.sharedInstance().username = userNameTextField.text!
                DataManager.sharedInstance().password = passwordTextField.text!
                DataManager.sharedInstance().deviceToken = getDeviceToken()
                DataManager.sharedInstance().method = "authenticate"
                showLoadingIndictor()
                DataManager.sharedInstance().loginRequest(username:userNameTextField.text!, password:passwordTextField.text! ,device: "2",gcmId: tokenId!, apiName: login,closure: { (result) in
                    switch (result) {
                    case .success(let user):
                        self.hideLoadingIndictor()
                        let message = user.message
                        print (user.is_error)
                        if user.is_error == "1" {
                            self.showValidationAlert(title: "Alert!!", message: message)
                        }else{
                            self.userDetails = user
                            UserDefaults.standard.set(self.userDetails.password, forKey: "password_key")
                            UserDefaults.standard.set(self.userDetails.username, forKey: "userName_key")
                            UserDefaults.standard.set(self.passwordTextField.text, forKey: "afterLoginPass_key")
                            //// Profile Detail
                            UserDefaults.standard.set(self.userDetails.name, forKey: "nameProfile_key")
                            UserDefaults.standard.set(self.userDetails.email, forKey: "email_key")
                            UserDefaults.standard.set(self.userDetails.contactNo, forKey: "contactNo_key")
                            UserDefaults.standard.set(self.userDetails.emp_id, forKey: "empID_key")
                            UserDefaults.standard.set(self.userDetails.employeeId, forKey: "employee_key")
                            UserDefaults.standard.set(self.userDetails.officId, forKey: "office_key")
                            UserDefaults.standard.set(self.userDetails.qrImage, forKey: "qrImage_key")
                            UserDefaults.standard.set(self.userDetails.profileImage, forKey: "profileImage_key")
                            UserDefaults.standard.synchronize()
                            
                            DataManager.sharedInstance().username = UserDefaults.standard.value(forKey: "userName_key") as? String ?? ""
                            DataManager.sharedInstance().password = UserDefaults.standard.value(forKey: "afterLoginPass_key") as? String ?? ""
//                            self.updateFcmAPI()
                            self.hideLoadingIndictor()
                            DispatchQueue.main.async {
                                //TJ Commented
//                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "swRevelVC") as! SWRevealViewController
//                                self.present(vc, animated: true, completion: {
//
//                                })
                                
                                
                            }
                        }
                        
                    case .failure(let error):
                        print(error)
                        self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                    }
                    
                })
                
            }
        
    }


}

