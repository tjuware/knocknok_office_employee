//
//  ViewReminderViewController.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 09/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class ViewReminderViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var reminderTableView: UITableView!
    
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    var viewReminderArray = [ViewReminder]()
    var userDetails = DataManager.sharedInstance().userDetails
    let vcIdentifier = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Reminders"
                //TJ Commented
//        slideMenuShow(menuButton, viewcontroller: self)
        reminderTableView.layer.borderWidth = 3
        reminderTableView.layer.borderColor = UIColor.lightGrayBorderColor.cgColor
        reminderTableView.clipsToBounds = true
        self.noDataLabel.isHidden = true
//        self.reminderTableView.tableFooterView = UIView()

        ViewReminderList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Tableview delegate and Datasource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewReminderArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 138
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = self.reminderTableView.dequeueReusableCell(withIdentifier: "Cell") as? ViewReminderTableViewCell
        if  cell == nil {
            cell = self.reminderTableView.dequeueReusableCell(withIdentifier: "viewReminderTableViewCell") as? ViewReminderTableViewCell
        }
        cell?.reminderV = viewReminderArray[indexPath.row]
        cell?.selectionStyle = UITableViewCellSelectionStyle.none

        return cell!
    }
    
    func ViewReminderList() {
        
        showLoadingIndictor()
        DataManager.sharedInstance().method = "visitor_rem_list"
        DataManager.sharedInstance().ViewReminderRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), apiName: visitor_rem_list,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.viewReminderArray = data;
                if self.viewReminderArray.count == 0 {
                    self.noDataLabel.isHidden = false
                } else {
                    self.noDataLabel.isHidden = true
                    self.reminderTableView.reloadData()
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
            }
        })
    }

    @IBAction func visitorReminderAction(_ sender: Any) {
        ViewReminderList()
    }
    
}
