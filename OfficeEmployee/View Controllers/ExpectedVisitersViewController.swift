//
//  ExpectedVisitersViewController.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 09/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

class ExpectedVisitersViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, SelectExpectedDelegate, addUserDelegate, UISearchBarDelegate, UISearchDisplayDelegate  {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var tableViews: UITableView!

    var userDetails = DataManager.sharedInstance().userDetails
    var expectedVistorsArray = [ExpectedVisitor]()
    var viewControllerIdentifier = "expectedVisitersVC"
    var serachExpectVisistorListArray = [ExpectedVisitor]()
    var addUserButton = UIButton()
    var addBlackUserButton = UIButton()
    var addUserBool = Bool()
    var expectVisit =  ExpectedVisitor()

    override func viewDidLoad() {
        super.viewDidLoad()
                //TJ Commented
//        slideMenuShow(menuButton, viewcontroller: self)
        addContactButtonToVC()
        addBlackUserButtonToVC()
        tableViews.layer.borderWidth = 3
        tableViews.layer.borderColor = UIColor.lightGrayBorderColor.cgColor
        tableViews.clipsToBounds = true
        self.view.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
        noDataLabel.isHidden = true
       
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ExpectedVisitersViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(self.functionName), name: NSNotification.Name(rawValue: "NotificationID"), object: nil)
        tableViews.tableFooterView = UIView()

        NotificationCenter.default.addObserver(self, selector: #selector(ExpectedVisitersViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        DispatchQueue.main.async
            {
                let popup : NotificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "notificationViewController") as! NotificationViewController
                let navigationController = UINavigationController(rootViewController: popup)
                navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @objc override func dismissKeyboard() {
        view.endEditing(true)
    
    }
    
    @objc func functionName() {
        apiCall()
    }
    
    func apiCall(){
        if viewControllerIdentifier == "expectedVisitersVC"{
            self.addBlackUserButton.isHidden = true
            self.addUserButton.isHidden = false
            self.navigationItem.title = "Expected Visitors"
            self.searchBar.placeholder = "Search ExpecteVisitor"
            getExpectedVistors()
        }else{
            self.addUserButton.isHidden = true
            self.addBlackUserButton.isHidden = false
            self.navigationItem.title = "Block List"
            self.searchBar.placeholder = "Search BlockList"
            WatchList()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        apiCall()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.isUserInteractionEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serachExpectVisistorListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewControllerIdentifier == "expectedVisitersVC"{
            var cell = tableView.dequeueReusableCell(withIdentifier: "expectedVisitersTableViewCell", for: indexPath) as? ExpectedVisitersTableViewCell
            
            if cell == nil {
                cell = tableView.dequeueReusableCell(withIdentifier: "expectedVisitersTableViewCell", for: indexPath) as? ExpectedVisitersTableViewCell
            }
            cell?.expectedVisiter = serachExpectVisistorListArray[indexPath.row]
            /// Tap Mobile number Label
            let tapVisistorNo = UITapGestureRecognizer(target: self, action: #selector(ExpectedVisitersViewController.tappedvisitorNumber))
            cell?.mobileNoLabel.addGestureRecognizer(tapVisistorNo)
            cell?.mobileNoLabel.isUserInteractionEnabled = true
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
             return cell!
        }else{
            var cell1 = tableView.dequeueReusableCell(withIdentifier: "watchListCell", for: indexPath) as? WatchListTableViewCell
            
            if cell1 == nil {
                cell1 = tableView.dequeueReusableCell(withIdentifier: "watchListCell", for: indexPath) as? WatchListTableViewCell
            }
            cell1?.expectedVisiter = serachExpectVisistorListArray[indexPath.row]
            cell1?.selectionStyle = UITableViewCellSelectionStyle.none
            cell1?.deleteButton.addTarget(self, action: #selector(self.deleteMethod(sender:)), for: .touchUpInside)
            return cell1!
        }
      
    }
    
    @objc func tappedvisitorNumber(sender: UIGestureRecognizer)
    {
        var indexPath: IndexPath? = nil
        if let aSuperview = sender.view?.superview?.superview?.superview as? ExpectedVisitersTableViewCell {
            indexPath = tableViews.indexPath(for: aSuperview)
        }
        expectVisit = expectedVistorsArray [indexPath!.row]
        let mobileNumber = expectVisit.contactNo
        var phoneNumberString = mobileNumber
        phoneNumberString = phoneNumberString.replacingOccurrences(of: " ", with: "")
        phoneNumberString = "tel:\(phoneNumberString)"
        let phoneNumberURL = URL(string: phoneNumberString)
        if let phoneNumberURL = phoneNumberURL {
            UIApplication.shared.openURL(phoneNumberURL)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
    }
    
    @objc func deleteMethod(sender: UIButton)  {
        var indexPath: IndexPath? = nil
        if let aSuperview = sender.superview?.superview?.superview as? WatchListTableViewCell {
           indexPath = tableViews.indexPath(for: aSuperview)
        }
        expectVisit = expectedVistorsArray[indexPath!.row]
        let visitorId = expectVisit.visitorId
        let visitorNumber = expectVisit.contactNo
        showLoadingIndictor()
        DataManager.sharedInstance().method = "delete_block_visitor"
        DataManager.sharedInstance().deleteBlackRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(),visitoriD: visitorId,visitorNo: visitorNumber, apiName: deleteBlackList,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                let theDictionary = data[0]
                print(theDictionary)
                
                let errorCode = theDictionary["error_code"].string
                let messageAert = theDictionary["message"].string
                
                if errorCode == "0"{
                    self.apiCall()
                }else{
                    self.presentWindow!.makeToast(message: messageAert!)
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")
                
            }
        })
    }
    
    func addContactButtonToVC() {
        addUserButton = UIButton(frame: CGRect(x: self.view.bounds.width - 60,y: self.view.bounds.height - 60 ,width: 40,height: 40))
        addUserButton.setBackgroundImage(UIImage(named: "add_user"), for: UIControlState())
        addUserButton.isExclusiveTouch = true
        addUserButton.layer.cornerRadius = self.view.bounds.size.height/2
        addUserButton.clipsToBounds = true
        addShadowToView1(containerView: addUserButton)
        addUserButton.addTarget(self, action: #selector(addContact(sender:)), for: .touchUpInside)
        self.view.addSubview(addUserButton)
        self.view.bringSubview(toFront: addUserButton)
        
    }
    
    func addBlackUserButtonToVC() {
        addBlackUserButton = UIButton(frame: CGRect(x: self.view.bounds.width - 60,y: self.view.bounds.height - 60 ,width: 40,height: 40))
        addBlackUserButton.setBackgroundImage(UIImage(named: "add_user"), for: UIControlState())
        addBlackUserButton.isExclusiveTouch = true
        addBlackUserButton.layer.cornerRadius = self.view.bounds.size.height/2
        addBlackUserButton.clipsToBounds = true
        addShadowToView1(containerView: addBlackUserButton)
        addBlackUserButton.addTarget(self, action: #selector(addblackList(sender:)), for: .touchUpInside)
        self.view.addSubview(addBlackUserButton)
        self.view.bringSubview(toFront: addBlackUserButton)
        
    }
    
    @objc func dismisViewController()  {
       // let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
       // let newViewController = storyBoard.instantiateViewController(withIdentifier: "newExpectedUserViewController") as! NewExpectedUserViewController
       // self.present(newViewController, animated: true, completion: nil)
    }
    
    func addShadowToView1(containerView: UIView)  {
        containerView.layer.masksToBounds = false
        containerView.layer.shadowColor = UIColor.grayBorderColor.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowRadius = 3
    }
    
    @objc func addContact(sender: UIButton)  {
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "selectExpectedVisitorsViewController") as! SelectExpectedVisitorsViewController
        self.addChildViewController(popOverVC)
        popOverVC.modalTransitionStyle = .crossDissolve
        popOverVC.delegate = self
        popOverVC.view.frame = self.view.bounds
        self.view.isUserInteractionEnabled = false
        self.view.addSubview(popOverVC.view)
        UIApplication.shared.windows.first?.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @objc func addblackList(sender: UIButton)  {
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "addBlackListViewController") as! AddBlackListViewController
        self.addChildViewController(popOverVC)
        popOverVC.modalTransitionStyle = .crossDissolve
        popOverVC.modalPresentationStyle = .overCurrentContext
        popOverVC.userDelegate = self
        popOverVC.view.frame = self.view.bounds
        self.view.isUserInteractionEnabled = false
        self.view.addSubview(popOverVC.view)
        UIApplication.shared.windows.first?.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    
    func closeButtonAction() {
        self.view.isUserInteractionEnabled = true
    }
    func userCloseButtonAction() {
        self.view.isUserInteractionEnabled = true
    }
    
    func getExpectedVistors() {
        showLoadingIndictor()
        DataManager.sharedInstance().method = "expected_visitor"
        DataManager.sharedInstance().expectVistorsRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), apiName: expectedVisitors,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.expectedVistorsArray = data;
                self.serachExpectVisistorListArray = self.expectedVistorsArray

                if self.expectedVistorsArray.count == 0 {
                    self.noDataLabel.isHidden = false
                    self.addUserBool = true
                } else {
                    self.noDataLabel.isHidden = true
                    self.tableViews.reloadData()
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
    
    func WatchList() {
        showLoadingIndictor()
        DataManager.sharedInstance().method = "spamlist"
        DataManager.sharedInstance().WatchListRequest(empId: self.getEmployeeID(), officeId: self.getOfficeID(), apiName: watchList,closure: { (result) in
            print(result)
            self.hideLoadingIndictor()
            switch result {
            case .success(let data):
                print(data)
                self.expectedVistorsArray = data;
                self.serachExpectVisistorListArray = self.expectedVistorsArray
                if self.expectedVistorsArray.count == 0 {
                    self.noDataLabel.isHidden = false
                    self.tableViews.reloadData()
                } else {
                    self.noDataLabel.isHidden = true
                    self.tableViews.reloadData()
                }
                break
            case .failure(let error):
                print(error)
                self.showValidationAlert(title: "Alert!!", message: "Server Respond Unexpectedly")

            }
        })
    }
    
    func DeleteBlackListAPI() {
        
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchCommon(searchText: searchText)
        DispatchQueue.main.async { [weak self] in
            self?.tableViews?.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool  {
        return true
    }
    
    func searchCommon(searchText: String)  {

            if searchText.characters.count > 0 {
                serachExpectVisistorListArray = expectedVistorsArray.filter {
                    let stringMatch = ($0.name).range(of: searchText.lowercased())
                    return stringMatch != nil ? true : false
                }
            }else{
                self.serachExpectVisistorListArray = expectedVistorsArray
                print(serachExpectVisistorListArray)
            }
            
        }
    
}
