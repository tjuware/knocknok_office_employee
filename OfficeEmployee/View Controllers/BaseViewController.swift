//
//  BaseViewController.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 07/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit
import EZLoadingActivity

class BaseViewController: UIViewController {
   
    var presentWindow : UIWindow?
    let ThemeColor   = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
    let toastFontColor   = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        EZLoadingActivity.Settings.ActivityColor = UIColor.cellBackgroundColor//UIColor.init(patternImage: UIImage(named: "multicolor1")!)
        EZLoadingActivity.Settings.ActivityHeight = 60
        EZLoadingActivity.Settings.BackgroundColor = UIColor.white
        EZLoadingActivity.Settings.TextColor = UIColor.cellBackgroundColor
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.checkNetwork(notification:)), name: Notification.Name("NetworkNotification"), object: nil)
        UIView.hr_setToastThemeColor(color: ThemeColor)
        UIView.hr_setToastFontColor(color: toastFontColor)
        UIView.hr_setToastFontName(fontName: "HelveticaNeueLTPro-UltLt")
        presentWindow = UIApplication.shared.keyWindow

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //dismissKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   @objc func dismissViewController()  {
//    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//    let newViewController = storyBoard.instantiateViewController(withIdentifier: "newExpectedUserViewController") as! NewExpectedUserViewController
//    self.present(newViewController, animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    func addBackButton() {
        self.popCurrentViewController()
    }
    
    func addShadowToView(containerView: UIView)  {
        containerView.layer.masksToBounds = false
        containerView.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowRadius = 5 
    }
    
    func setBottomBorder(toView : UIView) {
        toView.layoutIfNeeded()
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.grayBorderColor.cgColor
        border.frame = CGRect(x: 0, y: toView.frame.size.height - width, width:  toView.frame.size.width, height: toView.frame.size.height)
        
        border.borderWidth = width
        toView.layer.addSublayer(border)
        toView.layer.masksToBounds = true
    }
    
    func setTopBorder(toView : UIView) {
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.grayBorderColor.cgColor
        border.frame = CGRect(x: 0, y: toView.frame.size.height - width, width:  toView.frame.size.width, height: toView.frame.size.height)
        
        border.borderWidth = width
        toView.layer.addSublayer(border)
        toView.layer.masksToBounds = true
    }
    
    func setLeftBorder(toView : UIView) {
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.grayBorderColor.cgColor
        border.frame = CGRect(x: 0, y: toView.frame.size.height - width, width:  toView.frame.size.width, height: toView.frame.size.height)
        
        border.borderWidth = width
        toView.layer.addSublayer(border)
        toView.layer.masksToBounds = true
    }
    
    func setRightBorder(toView : UIView) {
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.grayBorderColor.cgColor
        border.frame = CGRect(x: 0, y: toView.frame.size.height - width, width:  toView.frame.size.width, height: toView.frame.size.height)
        border.borderWidth = width
        toView.layer.addSublayer(border)
        toView.layer.masksToBounds = true
    }
    
    func createShadow(containerView: UIView) {
        containerView.layer.shadowPath = createShadowPath(containerView: containerView).cgPath
        containerView.layer.masksToBounds = false
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        containerView.layer.shadowRadius = 5
        containerView.layer.shadowOpacity = 0.4
    }
    
    func createShadowPath(containerView: UIView) -> UIBezierPath {
        let myBezier = UIBezierPath()
        myBezier.move(to: CGPoint(x: -3, y: -3))
        myBezier.addLine(to: CGPoint(x: containerView.frame.width + 3, y: -3))
        myBezier.addLine(to: CGPoint(x: containerView.frame.width + 3, y: containerView.frame.height + 3))
        myBezier.addLine(to: CGPoint(x: -3, y: containerView.frame.height + 3))
        myBezier.close()
        return myBezier
    }
    
    func showValidationAlertWithButton(title: String, message: String,buttonArray: [String],completionHandler: @escaping (String) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for index in 0...buttonArray.count{
            let defaultAction = UIAlertAction(title: buttonArray[index], style: .default) { (action) in
                completionHandler(buttonArray[index])
            }
            alertController.addAction(defaultAction)
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func showValidationAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default) { (action) in
        }
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func getDeviceToken() -> String {
        return UIDevice.current.identifierForVendor!.uuidString
    }

    func dismissKeyboard() {
        self.resignFirstResponder()
    }
    
    func showLoadingIndictor() {
        DispatchQueue.main.async {
            EZLoadingActivity.show("Please wait...", disableUI: true)
        }
    }
    
    func hideLoadingIndictor() {
        DispatchQueue.main.async {
            EZLoadingActivity.hide()
        }
    }
    
    @objc func checkNetwork(notification: Notification)  {
        self.hideLoadingIndictor()
        showValidationAlert(title: "Internet Status", message: "Please check your internet connection.")
    }
    
    func getEmployeeID() -> String {
        return UserDefaults.standard.value(forKey: "employee_key") as? String  ?? ""
    }
    
    func getOfficeID()  -> String {
        return UserDefaults.standard.value(forKey: "office_key") as? String  ?? ""
    }

}
