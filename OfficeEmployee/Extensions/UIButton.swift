//
//  File.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 08/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func setButtonAllSqureBorder() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGrayBorderColor.cgColor
    }
    
    func setButtonAllRoundBorder() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGrayBorderColor.cgColor
        layer.cornerRadius = bounds.height/2
    }
}
