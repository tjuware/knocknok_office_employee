//
//  UITextField.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 07/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    public func setTextFieldRoundBorder() {
        self.layer.cornerRadius = self.frame.height/2
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.cellBackgroundColor.cgColor
        let padding = UIView(frame: CGRect(x: 0, y: 1, width: 15, height: self.frame.height - 2))
        self.leftView = padding
        self.leftViewMode = UITextFieldViewMode.always
        

    }
    
    public func setTextFieldAllSqureBorder() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGrayBorderColor.cgColor
        let padding = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.frame.height))
        self.leftView = padding
        self.leftViewMode = UITextFieldViewMode.always
        clipsToBounds = true
    }
    
    
   
    func setLeftIcon(_ imageName: String) {
        let padding = 5
        let size = 20
        let outerView = UIView(frame: CGRect(x: 0, y: 5, width: size+padding + 10, height: size) )
        let iconView  = UIImageView(frame: CGRect(x: padding, y: 0, width: size, height: size))
        iconView.image = UIImage(named: imageName)
        outerView.addSubview(iconView)
        self.leftView = outerView
        self.leftViewMode = .always
    }
    
    func setLeftIconFromUrl(_ urlString: String) {
        let padding = 20
        let size = 30
        let outerView = UIView(frame: CGRect(x: 0, y: 5, width: size+padding + 20, height: size) )
        let iconView  = UIImageView(frame: CGRect(x: padding, y: 0, width: size + 5, height: size))
        let url = URL(string:"http://" + urlString)
        let data = try? Data(contentsOf: url!)
        if let imageData = data {
            let image: UIImage = UIImage(data: imageData)!
            iconView.image = image
            
        } else {
            iconView.image = nil
        }
        outerView.addSubview(iconView)
        self.leftView = outerView
        self.leftViewMode = .always
        
    }
    
    /// set icon of 20x20 with left padding of 8px
    public func setRightIcon(_ imageName: String) {
        
        let padding = 10
        let size = 20
        let outerView = UIView(frame: CGRect(x: 0, y: 5, width: size+padding + 10, height: size) )
        let iconView  = UIImageView(frame: CGRect(x: padding, y: 0, width: size + 5, height: size))
        iconView.image = UIImage(named: imageName)
        outerView.addSubview(iconView)
        self.rightView = outerView
        self.rightViewMode = .always
    }
    
    func setBottomBorder(color: UIColor) {
        self.borderStyle = UITextBorderStyle.none;
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = color.cgColor
        //            UIColor(hexString: "FFFFFF")!.CGColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width,   width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
