//
//  Color.swift
//  DigitalGorkha
//
//  Created by Ankit Bhardwaj on 07/07/18.
//  Copyright © 2018 Ankit Bhardwaj. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var textFieldBorderColor: UIColor! {
        get {
            // return UIColor(red: 37.0/255.0, green: 170.0/255.0, blue: 145.0/255.0, alpha: 1.0)
            return UIColor(red: 241.0/255.0, green: 100.0/255.0, blue: 99.0/255.0, alpha: 1.0)
        }
    }
    
    static var grayBorderColor: UIColor! {
        get {
            return UIColor(red: 201.0/255.0, green: 201.0/255.0, blue: 201.0/255.0, alpha: 1.0)
        }
    }
    
    static var lightGrayBorderColor: UIColor! {
        get {
            return UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 1.0)
        }
    }
    
    static var lightGrayButtonBackgroundColor: UIColor! {
        get {
            return UIColor(red: 226.0/255.0, green: 226.0/255.0, blue: 226.0/255.0, alpha: 1.0)
        }
    }
    
    
    static var cellBackgroundColor: UIColor! {
        get {
//            return UIColor(red: 37.0/255.0, green: 170.0/255.0, blue: 145.0/255.0, alpha: 1.0)
            return UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 98.0/255.0, alpha: 1.0)
        }
    }
    
}
